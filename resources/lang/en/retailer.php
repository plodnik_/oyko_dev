<?php

return [

    'retailer'      =>  'Retailer',
    'new-retailer'  =>  'Add retailer',
    'edit-retailer' =>  'Edit retailer',

    'pos-icon'      =>  'POS icon',
    'region'        =>  'Region',
    'name-reg'      =>  'Name (regional language)',
    'name-en'       =>  'Name (english)',
    'address-reg'   =>  'Address (regional language)',
    'address-en'    =>  'Address (english)',
    'categories'    =>  'Category|Categories',
    'work-scheme'   =>  'Work scheme',
    'added-by'      =>  'Added by',

    'ln-reg'    =>  '(regional language)',
    'ln-en'     =>  '(eng)',
    'back-to'   =>  'To retailers list...>',


    'work-scheme-r' =>  'Work scheme with retailer',
    'name-reg-r'    =>  'Retailer name (regional language)',
    'name-en-r'     =>  'Retailer name (english)',
    'barcode-standard'  =>  'Barcode standard',
    'decs-company-reg'  =>  'Company description (regional language)',
    'decs-company-en'   =>  'Company description (english)',
    'keywords-separated'=>  'Keywords (separated by commas)',
    'keywords'      =>  'Keywords',
    'corp-color'    =>  'Corporate color',
    'max-count-categories'  =>  'No more than {n} category|No more than {n} categories',
    'loyalty-reg'    =>  'Loyalty program description (regional language)',
    'loyalty-en'    =>  'Loyalty program description (en)',
    'cont-number'   =>  'Agreement #',
    'cont-scan'     =>  'Agreement scan',
    'add-file'      =>  'Add file',


    'menu-back'    =>  'To list',
    'menu-analytics'    =>  'Analytics',
    'menu-information'  =>  'Information',
    'menu-users'  =>  'Users',
    'menu-logs' =>  'Logs',
    'to-list'   =>  'To retailers list…>',

    'gen-inf'   =>  'GENERAL INFORMATION',
    'loyalty-title' => 'LOYALTY PROGRAM DESCRIPTION',
    'tech-inf'  =>  'TECHNICAL INFORMATION',
    'map-title' =>  'PICTURES',

    't-large'   =>  'Large card',
    't-small'   =>  'Small card',
    't-pos-ico' =>  'Map POS ico',
    't-logo'    =>  'Logo',

    't-loyalty' =>  'Loyalty program description',
    't-tech'    =>  'Technical information',

    'add-large' =>  'Add picture for big card',
    'add-small' =>  'Add picture for small card',
    'add-pos'   =>  'Add picture for map POS ico',
    'add-logo'  =>  'Add picture for logo',

    'block-r'   =>  'Block retailer?',
    'unblock-r'     =>  'Unblock retailer?',
    'sure-block'    =>  'Are you sure you want to block retailer %s?',
    'sure-unblock'  =>  'Are you sure you want to unblock retailer %s?',
    'why-block'     =>  'Reason desciption',
    'why-unblock'   =>  'Reason for unblocking',
    'ok-block'  =>  'Retailer %s blocked successfully',
    'desc-block'    =>  'Retailer is blocked due to',

    'before-publish-t'  =>  'Unfinished mandatory actions',
    'before-publish'    =>  'For publishing you have to finish the following',
    'notic-publish' =>  'Publish new retailer',
    'notice-send'   =>  'Send push notifications',
    'ok-publush'    =>  'Retailer %s published',
    'ok-push'   =>  'Push notifications sent out to all OYKO users',

    'delete-r'  =>  'Delete retailer?',
    'sure-delete'   =>  'Are you sure you want to delete retailer %s?',
    'delete-success'    =>  'Retailer %s deleted successfully',

    'add-POS'   =>  'Add POS',
    'new-POS'   =>  'Add new POS',
    'edit-POS'  =>  'Editing POS',

    'file-is-invalid'   =>  'File format is incorrect',
    'ok-import'         =>  'Import successfully',

    'statistic-title'   =>  'GENERAL STATISTICS',
    'analytics-title'   =>  'ANALYTICS',
    'pos-count'     =>  'Number of POSs',
    'cards-count'   =>  'Number of added cards by the app users',
    'share-cards'   =>  'Share of cards added to Favorites',
    'made-by'       =>  'Made by',
    'pos_count_caption' => 'POS',




];