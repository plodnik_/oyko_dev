<?php

return [

    'login'     => 'Login',
    'password'  => 'Password',
    'enter'     => 'Log in',
    'profile'   => 'Profile',
    'create'    => 'Create',
    'save'      => 'Save',
    'delete'    => 'Delete',
    'cancel'    => 'Cancel',
    'close'     => 'Close',
    'next'      => 'Next',
    'block'     => 'Block',
    'unblock'   => 'Unblock ',
    'publish'   => 'Publish',
    'upload'    => 'Upload',
    'added'     => 'Added',
    'edited'    => 'Edited',
    'status'    => 'Status',
    'last-changes'  =>  'Last changes',
    'picture'   =>  'Picture',

    'phone'     =>  'Phone',
    'free-phone'=>  'Free phone',
    'all'       =>  'All',
    'latitude'  =>  'Latitude',
    'longitude' =>  'Longitude',

    'name'  =>  'Name',
    'address'   =>  'Address',
    'action'    =>  'Action',
    'reason'    =>  'Reason',
    'date-time' =>  'Date & Time',





];