<?php
/**
 * Created by PhpStorm.
 * User: mirage
 * Date: 13.10.16
 * Time: 21:36
 */

return [

    'error500' => 'Something wrong!',
    'email_exist' => 'Email/login exist',
    'account-blocked'   => 'Access denied! Your account is blocked',
    'set_category'      => 'Select at least one category',

    'files-limit'   =>  'Only {{fi-limit}} file is allowed to be uploaded.|Only {{fi-limit}} files are allowed to be uploaded.',
    'file-type'     =>  'File type is not valid',
    'file-size'     =>  '{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.',

    'not-required'  =>  'Not all mandatory fields are filled in!',

    'csrf'  => 'Token mismatch! Try reload page and repeat your actions again',

];