<?php

return [


    'retailers'     =>  'Retailers',
    'total-retailers'   =>  'Total connected retailers',
    'with-status'   =>  'with status',

    'cards' =>  'Cards',
    'total-cards'   =>  'Total cards added',
    'average-cards' =>  'On average per on retailer'


];