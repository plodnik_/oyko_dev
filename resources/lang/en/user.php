<?php
/**
 * Created by PhpStorm.
 * User: mirage
 * Date: 13.10.16
 * Time: 17:26
 */

return [

    'new_user'      =>  'Create user',
    'edit_user'     =>  'Edit profile',
    'login_email'   =>  'Login/E-mail',
    'first_name'    =>  'First name',
    'last_name'     =>  'Last name',
    'user_role'     =>  'Role',
    'user_status'   =>  'Status',
    'date_register' =>  'Registration date&time',
    'status'        =>  'Status',

];