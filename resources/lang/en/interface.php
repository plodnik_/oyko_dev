<?php

return [

    'max-length-n-char'     =>  'Maximum {n} characters',
    'char-left'             =>  'Remaining',
    'file-or-link'          =>  'File or link',
    'add-link'              =>  'Add link',
    'upload-pdf'            =>  'Upload PDF',
    'upload-file'           =>  'Upload file',
    'choose-file'           =>  'Choose file',
    'file-selected'         =>  'File selected',
    'success-manipulation'  =>  'Actions completed',
    'delete-img'            =>  'Delete image?',
    'sure-delete-img'       =>  'Are you sure you want to delete this image?',
];