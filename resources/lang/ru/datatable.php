<?php
/**
 * Created by PhpStorm.
 * User: mirage
 * Date: 13.10.16
 * Time: 19:40
 */

return [

    'all' => [
        'processing'=>     '<img src="/assets/admin/img/buffering.gif">',
        'lengthMenu'=>     'Показывать _MENU_ записей на одной странице',
        'zeroRecords'=>    'Ничего не найдено',
        'emptyTable'=>     'Данные отсутствуют',
        'info'=>           'Показано с _START_ по _END_ из _TOTAL_ записей',
        'infoEmpty'=>      'Показано с 0 по 0 из 0 записей',
        'infoFiltered'=>   '(отфильтровано из _MAX_ записей)',
        'infoPostFix'=>    '',
        'search'=>         'Поиск',
        'url'=>            '',
        'infoThousands'=>  ',',
        'loadingRecords'=> '<img src="/assets/admin/img/buffering.gif">',

        'paginate'=> array(
            'first'=>    '<<',
            'last'=>     '>>',
            'next'=>     '>',
            'previous'=> '<'
        ),

        'aria'=> array(
            'sortAscending' =>  'Сортировака по возрастанию',
            'sortDescending' => 'Сортировака по убыванию'
        ),
    ]

];