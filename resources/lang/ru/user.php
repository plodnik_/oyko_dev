<?php
/**
 * Created by PhpStorm.
 * User: mirage
 * Date: 13.10.16
 * Time: 17:26
 */

return [

    'new_user'      =>  'Новый пользователь',
    'edit_user'     =>  'Редактирование профиля',
    'login_email'   =>  'Login/E-mail',
    'first_name'    =>  'Имя',
    'last_name'     =>  'Фамилия',
    'user_role'     =>  'Роль',
    'user_status'   =>  'Статус',
    'date_register' =>  'Дата и время регистрации',
    'status'        =>  'Статус'

];