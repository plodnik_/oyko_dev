<?php

return [


    'retailers'     =>  'РИТЕЙЛЫ',
    'total-retailers'   =>  'Всего подключенных Ритейлов',
    'with-status'   =>  'со статусом',

    'cards' =>  'КАРТЫ',
    'total-cards'   =>  'Всего добавлено карт',
    'average-cards' =>  'В среднем на одного Ритейла'


];