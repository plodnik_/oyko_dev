<?php

return [

    'max-length-n-char'     =>  'Не более {n} символов ',
    'char-left'             =>  'Осталось',
    'file-or-link'          =>  'Файл или ссылка',
    'add-link'              =>  'Добавить ссылку',
    'upload-pdf'            =>  'Загрузить PDF',
    'upload-file'           =>  'Загрузить файл',
    'choose-file'           =>  'Укажите файл',
    'file-selected'         =>  'Файл указан',
    'success-manipulation'  =>  'Действия выполнено',
    'delete-img'            =>  'Удалить изображение',
    'sure-delete-img'       =>  'Вы действидельно хотите удалить изображение?',

];