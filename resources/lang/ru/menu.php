<?php
/**
 * Created by PhpStorm.
 * User: mirage
 * Date: 13.10.16
 * Time: 17:05
 */


return [

    'statistics'    =>  'Статистика',
    'retailers'     =>  'Ритейлы',
    'users'         =>  'Пользователи',
    'marketing'     =>  'Маркетинг'

];