<ul class="navbar-locales lang-variants">
    @foreach($supportedLocales as $key => $locale)
        <li class="lang-variants__lng {{ localization()->getCurrentLocale() == $key ? 'active' : '' }}">
            <a href="{{ localization()->getLocalizedURL($key) }}" rel="alternate" hreflang="{{ $key  }}" class="lang-variants__link">
                {{ strtoupper($locale->key()) }}
            </a>
        </li>
    @endforeach
</ul>
