<script type="text/javascript">
    jQuery(document).ready(function(){
        // dynamic table

        var tableId = '#{!! $id !!}';

        if (tableId == '#pos-dataTable') {

            oTablePos = jQuery(tableId).dataTable(
                    {!! $options !!}
            );

        } else if(tableId == '#logs-dataTable'){
            logsTable = jQuery(tableId).DataTable(
                    {!! $options !!}
            );
        }else {

            oTable = jQuery(tableId).dataTable(
                    {!! $options !!}
            );

        }

    });

    setInterval(update_retailer_status,5000);

    //функция обновления статусов ритейлов на поточной странице
    function update_retailer_status()
    {
        if (window.page_retailter_oyko_ids) {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/"+locale+'/admin/retailer/page_status_update',
                data: {
                    oyko_ids: window.page_retailter_oyko_ids
                },
                type: 'POST',
                dataType: 'json',
                cache: false,
                success: function(response){
                    if (!response)
                        return false;
                    var cur_status = '';
                    for (var i=0; i<response.length; i++)
                    {
                        cur_status = $("#retailer_status_"+response[i]['oyko_id']).attr('status');
                        if (cur_status == response[i]['retailer_status_id'])
                            continue;
                        $("#retailer_status_"+response[i]['oyko_id'])
                                .attr('status',response[i]['retailer_status_id'])
                                .css('color',response[i]['status_color'])
                                .html(response[i]['name_'+locale]);
                    }
                }
            })
        }
    }

</script>
