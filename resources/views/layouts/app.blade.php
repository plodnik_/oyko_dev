<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="icon" type="image/png" href="{{asset('assets/admin/img/LogoRed.png')}}" />
    <!-- Styles -->
    <link href="{{ asset('assets/admin/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/packages/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/custom.css') }}" rel="stylesheet">
    @yield('css')


    <script type="text/javascript" src="{{ asset('assets/packages/jquery/jquery.min.js') }}"></script>
    <!-- Scripts -->
    <script src="{{ asset('assets/packages/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/main.js') }}"></script>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <div class="fixed-nav">
            <nav class="navbar navbar-default navbar-static-top navbar-blue">
                <div class="info-block">
                    <div class="container clearfix">
                        <div class="row">
                            <div class="clock-block col-md-6">
                                <span class="clock-block__date-text">
                                    {{ date( 'd.m.Y', time()) }}
                                </span>
                                <span class="clock-block__time-text"></span>
                            </div>
                            <div class="lang-block col-md-2">
                                {!! Localization::localesNavbar() !!}
                            </div>

                            @if(auth()->check())
                                @include('sections.user-menu')
                            @endif
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="navbar-header">
                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ route('admin.index') }}">
                            <img src="{{asset('assets/admin/img/logo.png')}}">
                        </a>
                    </div>

                    @if(auth()->check())
                        @include('sections.admin-menu')
                    @endif
                </div>
            </nav>
        </div>


        @if(auth()->check())
        <!-- Modal from edit myself -->
        @include('pages.users.modals.myself')
        @endif

        <div class="page-wrapper">
            @yield('content')
        </div>
    </div>

    <div class="modal fade" id="specialAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 400px">
            <div class="modal-content grey">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="message">
                                {{ session('error') ? session('error') : '' }}
                            </div>
                            <button type="button" data-dismiss="modal" class="btn btn-custom pull-right">Ok</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(session('error'))
        <script type="text/javascript">
            $(function () {
                $('#specialAlert').modal('show');
            })
        </script>
    @endif

    @if ($errors->has('email'))
    @include('sections.auth-error')
    @endif

    <script type="text/javascript">

        var locale = '{{ $locale }}';
        var error500 = '{{ trans('error.error500') }}';

    </script>

    <script type="text/javascript" src="{{ asset('assets/admin/js/Cleaner.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/KeyConfirm.js') }}"></script>

    @yield('js')
</body>
</html>
