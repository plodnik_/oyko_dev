@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default" style="margin-top: 40px;">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/'.$locale.'/admin/login') }}">
                    {{ csrf_field() }}
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ trans('auth.title') }}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label oyko-label">{{ trans('words.login') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label oyko-label">{{ trans('words.password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>
                        </div>
                        <input type="hidden" name="remember" value="on">
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-custom pull-right">
                                    {{ trans('words.enter') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
