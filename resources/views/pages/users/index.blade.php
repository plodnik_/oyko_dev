@extends('layouts.app')

@section('active.users')class="active"@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/packages/datatable/css/dataTables.bootstrap.min.css') }}">
@stop

@section('content')
<div class="container custom-wrapper">
    <div class="row">
        @if($current_user->hasRole('admin'))
        <div class="col-md-12">
            <button class="btn btn-custom pull-right" data-toggle="modal" data-target="#createUser">{{ trans('user.new_user')}}</button>
        </div>
        <!-- Create user modal -->
        @include('pages.users.modals.create')
        <!-- Update user modal -->
        @include('pages.users.modals.update')

        @endif
    </div>
    <div class="row" style="margin-top: 30px;">
        <div class="col-md-12">
        {!! $table !!}
        </div>
    </div>
</div>

@stop


@section('js')
    <script type="text/javascript" src="{{ asset('assets/packages/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/packages/datatable/js/dataTables.bootstrap.min.js') }}"></script>
@stop


