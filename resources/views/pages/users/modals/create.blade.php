<div class="modal fade" id="createUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-clear="clear">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" id="create-user">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ trans('user.new_user')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="new_role" class="col-md-4 control-label oyko-label required">{{ trans('user.user_role') }}</label>

                                <div class="col-md-8">
                                    <select id="new_role" class="form-control" name="bo_user_role_id" required data-class="role">
                                        @foreach($roles->where('id', '<>', 1) as $role)
                                            <?php $role_key = 'role_name_'.$locale;?>
                                            <option value="{{ $role->id }}">{{ $role->$role_key }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block" style="display: none">
                                                <strong></strong>
                                            </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="new_email" class="col-md-4 control-label oyko-label required">{{ trans('user.login_email') }}</label>
                                <div class="col-md-8">
                                    <input id="new_email" type="text" class="form-control" name="email" data-class="email" required>
                                    <span class="help-block" style="display: none">
                                                <strong></strong>
                                            </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="new_password" class="col-md-4 control-label oyko-label required">{{ trans('words.password') }}</label>

                                <div class="col-md-8">
                                    <input id="new_password" type="password" class="form-control" name="password" data-class="password" required>
                                    <span class="help-block" style="display: none">
                                                <strong></strong>
                                            </span>
                                </div>
                            </div>
                            <div class="col-md-8 col-md-offset-4 success-status">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-custom submit" data-type="create" role="confirm">{{ trans('words.create') }}</button>
                    <a href="javascript:;" data-dismiss="modal" class="a_close">{{ trans('words.cancel') }}</a>
                </div>
            </form>
        </div>
    </div>
</div>