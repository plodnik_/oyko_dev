<div class="modal fade" id="updateMy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-clear="save">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ trans('user.edit_user')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-4 control-label oyko-label required">{{ trans('user.user_role') }}</label>

                                <div class="col-md-8">
                                    <span class="value">{{ $current_user_role }}</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label oyko-label">{{ trans('user.login_email') }}</label>
                                <div class="col-md-8">
                                    <span class="value">{{ $current_user->email }}</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="my_password" class="col-md-4 control-label oyko-label">{{ trans('words.password') }}</label>

                                <div class="col-md-8">
                                    <input id="my_password" type="password" class="form-control" name="password" data-class="password" required>
                                    <span class="help-block" style="display: none">
                                                <strong></strong>
                                            </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="my_name" class="col-md-4 control-label oyko-label">{{ trans('user.first_name') }}</label>

                                <div class="col-md-8">
                                    <input id="my_name" type="text" class="form-control" name="name" data-class="name" value="{{ $current_user->name }}">
                                    <span class="help-block" style="display: none">
                                                <strong></strong>
                                            </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="my_surname" class="col-md-4 control-label oyko-label">{{ trans('user.last_name') }}</label>

                                <div class="col-md-8">
                                    <input id="my_surname" type="text" class="form-control" name="surename" data-class="surname" value="{{ $current_user->surename }}">
                                    <span class="help-block" style="display: none">
                                                <strong></strong>
                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" value="{{ $current_user->id }}" id="my_id">
                    <input type="hidden" name="myself" value="true">
                    <button type="button" class="btn btn-custom submit" data-type="update" role="confirm">{{ trans('words.save') }}</button>
                    <a href="javascript:;" data-dismiss="modal" class="a_close">{{ trans('words.cancel') }}</a>
                </div>
            </form>
        </div>
    </div>
</div>