<div class="modal fade" id="updateUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-clear="clear">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ trans('user.edit_user')}} <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-4 control-label oyko-label">{{ trans('user.login_email') }}</label>
                                <div class="col-md-8">
                                    <span class="value" id="edit_email"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit_role" class="col-md-4 control-label oyko-label required">{{ trans('user.user_role') }}</label>

                                <div class="col-md-8">
                                    <select id="edit_role" class="form-control" name="bo_user_role_id" data-class="role" required>
                                        @foreach($roles->where('id', '<>', 1) as $role)
                                            <option value="{{ $role->id }}">{{ $role->{'role_name_'.$locale} }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block" style="display: none">
                                            <strong></strong>
                                        </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit_password" class="col-md-4 control-label oyko-label">{{ trans('words.password') }}</label>

                                <div class="col-md-8">
                                    <input id="edit_password" type="password" class="form-control" name="password" data-class="password" required>
                                    <span class="help-block" style="display: none">
                                            <strong></strong>
                                        </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit_status" class="col-md-4 control-label oyko-label required">{{ trans('user.user_status') }}</label>

                                <div class="col-md-8">
                                    <select id="edit_status" class="form-control" name="bo_user_status_id" data-class="status" required>
                                        @foreach($statuses as $status)
                                            <option value="{{ $status->id }}">{{ $status->{'status_'.$locale} }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block" style="display: none">
                                            <strong></strong>
                                        </span>
                                </div>
                                <div class="col-md-8 col-md-offset-4 success-status">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" value="" id="edit_id">
                    <button type="button" class="btn btn-custom submit" data-type="update" role="confirm">{{ trans('words.save') }}</button>
                    <a href="javascript:;" data-dismiss="modal" class="a_close">{{ trans('words.cancel') }}</a>
                </div>
            </form>
        </div>
    </div>
</div>