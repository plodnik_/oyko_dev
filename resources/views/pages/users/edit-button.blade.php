<a href="javascript:;"
   class="edit-user"
   data-toggle="modal"
   data-target="#updateUser"
   data-id="{{ $user->id  }}"
   data-email="{{ $user->email }}"
   data-role="{{ $user->bo_user_role_id }}"
   data-status="{{ $user->bo_user_status_id }}"
>
    <span class="icon-pencil"></span>
</a>