@extends('layouts.app')

@section('active.retailers')class="active"@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/packages/datatable/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/packages/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/packages/jquery/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/packages/aehlke-tag-it/css/jquery.tagit.css') }}">

@stop

@section('content')
    <div class="container custom-wrapper">
        @include('pages.retailers.sections.head')
        <div class="row">
            <div class="col-md-12">
                {!! $table !!}
                <script type="text/javascript">
                    var table_url = '{{ $table_url }}';
                </script>
            </div>
        </div>
    </div>

    @include('pages.retailers.modals.create')


@stop


@section('js')
    <script type="text/javascript" src="{{ asset('assets/packages/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/packages/datatable/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/packages/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/packages/jquery/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/packages/aehlke-tag-it/js/tag-it.js') }}"></script>
@stop


