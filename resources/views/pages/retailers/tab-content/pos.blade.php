<div class="row">
    <div class="col-md-12 filters clearfix">
        <form action="" id="pos-filters">

            <div class="one">
                <button class="btn btn-custom actions-btn disable"
                        type="button"
                        id="delete-pod-btn"
                        data-action="delete"
                >{{ trans('words.delete') }}</button>
                <button class="btn btn-custom actions-btn disable"
                        type="button"
                        id="block-pod-btn"
                        data-action="block"
                >{{ trans('words.block') }}</button>
            </div>

            <div class="one" style="position: relative">
                <input name="status" class="form-control"
                       id="pos-added"
                       placeholder="{{ trans('words.added') . ': ' .trans('words.all') }}"
                       style="width: 200px;"
                >
                <button class="btn btn-custom enabled-grey mini"
                        id="drop-filter-added"
                        style="display: none"
                        type="button">&times;</button>
            </div>
            <div class="one">
                <select name="status" class="select2-filter-pos"
                        id="pos-status"
                        data-title="{{ trans('words.status') }}"
                        data-empty="{{ trans('words.all') }}"
                        style="width: 350px;"
                >
                    <option value="">{{ trans('words.all') }}</option>
                    @foreach($pos_statuses as $p_status)
                        <option value="{{ $p_status->id }}">{{ $p_status->{'name_'.$locale} }}</option>
                    @endforeach
                </select>
            </div>
            <div class="pull-right">
                <button type="button" class="btn btn-custom" data-toggle="modal" data-target="#posCreate">{{ trans('retailer.add-POS') }}</button>
                <button type="button" class="btn btn-custom" data-toggle="modal" data-target="#posUpload">{{ trans('interface.upload-file') }}</button>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        {!! $posDT !!}
    </div>
    <script type="text/javascript">
        var pos_table_url = '{{ url('/'.$locale.'/admin/api/table/retailer/'.$retailer->id.'/pos') }}';
    </script>
</div>

@include('pages.retailers.modals.pos-create')
@include('pages.retailers.modals.pos-upload')
@include('pages.retailers.modals.pos-edit')