<div class="row">
    <div class="col-md-12">
        <p class="decs-blocked" {!! $reason ? '' : 'style="display:none"' !!}>
        {{ trans('retailer.desc-block') }}: <span>{{!$reason ? '' : $reason->reason }}</span>
        </p>
    </div>
</div>
<div class="row">
    <div class="col-md-4 images-box">
        <h4 class="upper">{{ trans('retailer.map-title') }}</h4>
        <div class="large">
            <label for="" class="title required">{{ trans('retailer.t-large') }}</label>
            <div class="img-box" {!! $retailer->big_img ? 'style="background: url('.$retailer->big_img .') center left / contain no-repeat"' : ''  !!}>

                <input type="hidden" id="retailer_id" value="{{ $retailer->id }}">
                <div>
                    @if($current_user->hasRole('admin') || $current_user->hasRole('moderator'))
                    <button class="btn btn-circle delete-img"
                            data-img="big_img"
                            data-toggle="modal"
                            data-target="#confirm-delete-img"
                            @if(!$retailer->big_img || $retailer->retailer_status_id==1)
                            style="display: none"
                            @endif
                    ><i class="ico-times"></i></button>
                    @endif
                    <button data-toggle="modal"
                            data-target="#imageModal"
                            class="btn btn-circle edit-img"
                            data-field="big_img"
                            data-img="{{ $retailer->big_img ? ($retailer->big_img):'' }}"
                            data-title="{{ trans('retailer.t-large') }}">
                        <span class="pencil-before"></span>
                    </button>
                </div>
            </div>
            <span class="desc-size">630x400</span>
        </div>
        <div class="small">
            <label for="" class="title required">{{ trans('retailer.t-small') }}</label>
            <div class="img-box" {!! $retailer->small_img ? 'style="background: url('.$retailer->small_img.') center left / contain no-repeat"' : ''  !!}>
                <div>
                    @if($current_user->hasRole('admin') || $current_user->hasRole('moderator'))
                    <button class="btn btn-circle delete-img"
                            data-img="small_img"
                            data-toggle="modal"
                            data-target="#confirm-delete-img"
                            @if(!$retailer->small_img || $retailer->retailer_status_id==1)
                            style="display: none"
                            @endif
                    ><i class="ico-times"></i></button>
                    @endif
                    <button data-toggle="modal"
                            data-target="#imageModal"
                            class="btn btn-circle edit-img"
                            data-field="small_img"
                            data-img="{{ $retailer->small_img ? ($retailer->small_img):'' }}"
                            data-title="{{ trans('retailer.t-small') }}">
                        <span class="pencil-before"></span>
                    </button>
                </div>
            </div>
            <span class="desc-size">190x122</span>
        </div>
        <div class="pos">
            <label for="" class="title required">{{ trans('retailer.t-pos-ico') }}</label>
            <div class="img-box" {!! $retailer->pos_img ? 'style="background: url('.$retailer->pos_img.') center left / contain no-repeat"' : ''  !!}>
                @if($current_user->hasRole('admin') || $current_user->hasRole('moderator'))
                    <button class="btn btn-circle delete-img"
                            data-img="pos_img"
                            data-toggle="modal"
                            data-target="#confirm-delete-img"
                            @if(!$retailer->pos_img || $retailer->retailer_status_id==1)
                            style="display: none"
                            @endif
                    ><i class="ico-times"></i></button>
                @endif
                    <button data-toggle="modal"
                            data-target="#imageModal"
                            class="btn btn-circle edit-img"
                            data-field="pos_img"
                            data-img="{{ $retailer->pos_img ? ($retailer->pos_img):'' }}"
                            data-title="{{ trans('retailer.t-pos-ico') }}">
                        <span class="pencil-before"></span>
                    </button>
            </div>
            <span class="desc-size">55x55</span>
        </div>
        <div class="logo">
            <label for="" class="title required">{{ trans('retailer.t-logo') }}</label>
            <div class="img-box" {!! $retailer->logo_img ? 'style="background: url('.$retailer->logo_img.') center left / contain no-repeat"' : ''  !!}>
                <div>
                    @if($current_user->hasRole('admin') || $current_user->hasRole('moderator'))
                    <button class="btn btn-circle delete-img"
                            data-img="logo_img"
                            data-toggle="modal"
                            data-target="#confirm-delete-img"
                            @if(!$retailer->logo_img || $retailer->retailer_status_id==1)
                            style="display: none"
                            @endif
                    ><i class="ico-times"></i></button>
                    @endif
                    <button data-toggle="modal"
                            data-target="#imageModal"
                            class="btn btn-circle edit-img"
                            data-field="logo_img"
                            data-img="{{ $retailer->logo_img ? ($retailer->logo_img):'' }}"
                            data-title="{{ trans('retailer.t-logo') }}">
                        <span class="pencil-before"></span>
                    </button>
                </div>
            </div>
            <span class="desc-size">200x200</span>
        </div>
    </div>
    <div class="col-md-8">
        <h4 class="upper">{{ trans('retailer.gen-inf') }}</h4>
        <button class="btn btn-circle edit-block-info"
                data-target="#editRetailer"
                data-toggle="modal"
                data-title="{{ trans('retailer.map-pos-ico') }}">
            <span class="pencil-before"></span>
        </button>
        <div class="form-horizontal saved">
            <div class="form-group">
                <label class="col-md-6 control-label oyko-label required">
                    {{ trans('retailer.name-reg') }}:
                </label>
                <span class="value col-md-5">{{ $retailer->name_rl }}</span>
            </div>
            <div class="form-group">
                <label class="col-md-6 control-label oyko-label required">
                    {{ trans('retailer.name-en') }}:
                </label>
                <span class="value col-md-5">{{ $retailer->name_en }}</span>
            </div>
            <div class="form-group">
                <label class="col-md-6 control-label oyko-label required">
                    {{ trans('retailer.region') }}:
                </label>
                <span class="value col-md-5">{{ $retailer->region->{'name_'.$locale} }}</span>
            </div>
            <div class="form-group">
                <label class="col-md-6 control-label oyko-label required">
                    {{ trans('retailer.work-scheme') }}:
                </label>
                <span class="value col-md-5 text-grey">{{ $retailer->workScheme->{'name_'.$locale} }}</span>
            </div>
            <div class="form-group">
                <label class="col-md-6 control-label oyko-label">OYKO ID: </label>
                <span class="value col-md-5 text-grey">{{ $retailer->oyko_id }}</span>
            </div>
            <div class="form-group">
                <label class="col-md-6 control-label oyko-label">
                    {{ trans('retailer.added-by') }}:
                </label>
                <span class="value col-md-5 text-grey">{{ $retailer->user->email }}</span>
            </div>
            @foreach($retailer->categories as $i=>$selected)
                <div class="form-group">
                    <label class="col-md-6 control-label oyko-label{{ $i==0 ? ' required' : ''}}">
                        {{ trans_choice('retailer.categories', 1) }} {{ $i+1 }}:
                    </label>
                    <span class="value col-md-5">{{ $selected->category->{'name_'.$locale} }}</span>
                </div>
            @endforeach
            <div class="form-group">
                <label class="col-md-6 control-label oyko-label required">
                    {{ trans('retailer.barcode-standard') }}:
                </label>
                <span class="value col-md-5">{{ $retailer->barcode->{'bar_code_standard_name'} }}</span>
            </div>
            <div class="form-group">
                <label class="col-md-6 control-label oyko-label required">
                    {{ trans('words.phone') }}{{ $retailer->phone_second ? ' 1' : ''}}:
                </label>
                <span class="value col-md-5">{{ $retailer->phone_first }}</span>
            </div>
            @if($retailer->phone_second)
                <div class="form-group">
                    <label class="col-md-6 control-label oyko-label ">
                        {{ trans('words.phone') }} 2:
                    </label>
                    <span class="value col-md-5">{{ $retailer->phone_second }}</span>
                </div>
            @endif
            @if($retailer->free_phone)
                <div class="form-group">
                    <label class="col-md-6 control-label oyko-label ">
                        {{ trans('words.free-phone') }}:
                    </label>
                    <span class="col-md-5 value">{{ $retailer->free_phone }}</span>
                </div>
            @endif
            @if($retailer->email)
                <div class="form-group">
                    <label class="col-md-6 control-label oyko-label ">E-mail: </label>
                    <span class="col-md-5 value">{{ $retailer->email }}</span>
                </div>
            @endif
            @if($retailer->url)
                <div class="form-group">
                    <label class="col-md-6 control-label oyko-label ">URL: </label>
                    <span class="col-md-5 value">{{ $retailer->email }}</span>
                </div>
            @endif
            <div class="form-group">
                <label class="col-md-6 control-label oyko-label required">
                    {{ trans('retailer.decs-company-reg') }}:
                </label>
                <span class="col-md-5 value">
                    {{ mb_strlen($retailer->company_description_rl) > 50
                        ? substr($retailer->company_description_rl, 0, 47).'...'
                        : $retailer->company_description_rl
                        }}
                </span>
            </div>
            <div class="form-group">
                <label class="col-md-6 control-label oyko-label required">
                    {{ trans('retailer.decs-company-en') }}:
                </label>
                <span class="col-md-5 value">
                    {{mb_strlen($retailer->company_description_en) > 50
                        ? substr($retailer->company_description_en, 0, 47).'...'
                        : $retailer->company_description_en
                        }}
                </span>
            </div>
            @if($retailer->keyword)
                <div class="form-group">
                    <label class="col-md-6 control-label oyko-label">
                        {{ trans('retailer.keywords') }}:
                    </label>
                    <span class="col-md-5 value">{{ $retailer->keyword }}</span>
                </div>
            @endif
            <div class="form-group">
                <label class="col-md-6 control-label oyko-label required">
                    {{ trans('retailer.corp-color') }}:
                </label>
                <span class="col-md-5 value">{{ $retailer->color }}</span>
            </div>
            <hr>
            <h4 class="upper">{{ trans('retailer.loyalty-title') }}</h4>
            <button class="btn btn-circle edit-block-info" data-target="#editLoyalty" data-toggle="modal">
                <span class="pencil-before"></span>
            </button>
            <div class="form-group program_description_url_rl">
                <label class="col-md-6 control-label oyko-label">
                    {{ trans('retailer.loyalty-reg') }}:
                </label>
                @if($retailer->program_description_url_rl)
                    <a href="{{ $retailer->program_description_url_rl }}" class="col-md-5 value">{{ $retailer->program_description_url_rl }}</a>
                @endif
            </div>
            <div class="form-group program_description_url_en">
                <label class="col-md-6 control-label oyko-label">
                    {{ trans('retailer.loyalty-en') }}:
                </label>
                @if($retailer->program_description_url_en)
                    <a href="{{ $retailer->program_description_url_en }}" class="col-md-5 value">{{ $retailer->program_description_url_en }}</a>
                @endif
            </div>
            <hr>
            <h4 class="upper">{{ trans('retailer.tech-inf') }}</h4>
            <button class="btn btn-circle edit-block-info" data-target="#editContract" data-toggle="modal">
                <span class="pencil-before"></span>
            </button>
            <div class="form-group contract_number">
                <label class="col-md-6 control-label oyko-label">
                    {{ trans('retailer.cont-number') }}:
                </label>
                <span class="col-md-5 value cont-number">{{ $retailer->contract ? $retailer->contract->number : ''}}</span>
            </div>
            <div class="form-group contract_scan">
                <label class="col-md-6 control-label oyko-label">
                    {{ trans('retailer.cont-scan') }}:
                </label>
                @if($retailer->contract)
                    <a href="{{ $retailer->contract->url ? ($retailer->contract->url) : '' }}" class="col-md-5 value cont-scan">
                        {{ $retailer->contract->url ? ($retailer->contract->url) : '' }}
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>