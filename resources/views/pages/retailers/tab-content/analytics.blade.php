<div class="row">
    <div class="col-md-12">
        <h1 class="page-title">{{ trans('retailer.statistic-title') }}</h1>
    </div>
    <div class="col-md-8 text-right">
        <h3 class="label-o">{{ trans('retailer.pos-count') }}:</h3>
    </div>
    <div class="col-md-4">
        <span class="text-value" id="pos-count"></span>
    </div>
    <div class="col-md-8 text-right">
        <h3 class="label-o">{{ trans('retailer.cards-count') }}:</h3>
    </div>
    <div class="col-md-4">
        <span class="text-value">242</span>
    </div>
    <hr>
    <div class="col-md-12">
        <h1 class="page-title">{{ trans('retailer.analytics-title') }}</h1>
    </div>
    <div class="col-md-12 text-center">
        <h3 class="label-o">{{ trans('retailer.share-cards') }}</h3>
    </div>
</div>