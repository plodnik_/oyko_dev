<div class="row">
    <div class="col-md-12 filters clearfix">
        <form action="" id="filters">
            <div class="one">
                <select name="region" id="filter-region" class="select2-filter"
                        data-title="{{ trans('retailer.region') }}"
                        data-empty="{{ trans('words.all') }}"
                        style="width: 150px"
                >
                    <option value="">{{ trans('words.all') }}</option>
                    @foreach($regions as $region)
                        <option value="{{ $region->id }}" >{{ $region->{'name_'.$locale} }}</option>
                    @endforeach
                </select>
            </div>
            <div class="one">
                <select name="category" id="filter-category" class="select2-filter"
                        data-title="{{ trans_choice('retailer.categories', 1) }}"
                        data-empty="{{ trans('words.all') }}"
                        style="width: 300px"
                >
                    <option value="">{{ trans('words.all') }}</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->{'name_'.$locale} }}</option>
                    @endforeach
                </select>
            </div>
            <div class="one">
                <select name="scheme" id="filter-schemes" class="select2-filter"
                        data-title="{{ trans('retailer.work-scheme') }}"
                        data-empty="{{ trans('words.all') }}"
                        style="width: 200px;"
                >
                    <option value="">{{ trans('words.all') }}</option>
                    @foreach($schemes as $scheme)
                        <option value="{{ $scheme->id }}">{{ $scheme->{'name_'.$locale} }}</option>
                    @endforeach
                </select>
            </div>
            <div class="one">
                <select name="status" id="filter-statuses" class="select2-filter"
                        data-title="{{ trans('words.status') }}"
                        data-empty="{{ trans('words.all') }}"
                        style="width: 150px;"
                >
                    <option value="">{{ trans('words.all') }}</option>
                    @foreach($statuses as $status)
                        <option value="{{ $status->id }}">{{ $status->{'name_'.$locale} }}</option>
                    @endforeach
                </select>
            </div>
            <div class="pull-right">
                <button type="button" class="btn btn-custom" data-toggle="modal" data-target="#createRetailer">{{ trans('retailer.new-retailer') }}</button>
            </div>
        </form>
    </div>
</div>