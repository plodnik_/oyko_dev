@extends('layouts.app')

@section('active.retailers')class="active"@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/packages/datatable/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/packages/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.8.17/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('assets/packages/aehlke-tag-it/css/jquery.tagit.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/packages/filer/css/jquery.filer.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/packages/filer/css/themes/jquery.filer-dragdropbox-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/packages/icheck/skins/square/blue.css') }}">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@stop

@section('content')

    <div class="container">
        <div class="row row-retailer-content">
            <div class="col-md-7">
                <ul class="nav nav-tabs">
                    <li><a style="{{ $locale != 'ru' ? 'padding-left: 23px !important;' : '' }}" class="retailer_tab_back" href="{{ url('/'.$locale.'/admin/retailers') }}">{{ trans('retailer.menu-back') }}</a></li>
                    <li><a data-toggle="tab" href="#analytics">{{ trans('retailer.menu-analytics') }}</a></li>
                    <li class="active"><a data-toggle="tab" href="#info">{{ trans('retailer.menu-information') }}</a></li>
                    <li><a data-toggle="tab" href="#users">{{ trans('retailer.menu-users') }}</a></li>
                    <li><a data-toggle="tab" href="#pos">POS</a></li>
                    <li><a data-toggle="tab" href="#logs">{{ trans('retailer.menu-logs') }}</a></li>
                </ul>
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-md-6 retailer-box">
                        <button class="color-box"
                                style="background-color: {{ $retailer->color }};
                                {{ $retailer->pos_img ? 'background: url("'.$retailer->pos_img.'") no-repeat; background-size: contain; background-position: center center' : '' }}">
                        </button>
                        <div class="text-box">
                            <span class="title col-md-3">{{ trans('retailer.retailer') }}:</span>
                            <span class="retailer-name col-md-7">{{ mb_strlen($retailer->name_en)>12 ? substr($retailer->name_en, 0,13).'...' : $retailer->name_en }}</span>
                        </div>
                    </div>
                    <div class="col-md-6 retailer-status text-right">
                        @include('pages.retailers.elements.status-select')
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div class="tab-pane fade" id="analytics">
                @include('pages.retailers.tab-content.analytics')
            </div>
            <div class="tab-pane fade in active" id="info">
                @include('pages.retailers.tab-content.info')
            </div>
            <div class="tab-pane fade" id="users"></div>
            <div class="tab-pane fade" id="pos">
                @include('pages.retailers.tab-content.pos')
            </div>
            <div class="tab-pane fade" id="logs">
                @include('pages.retailers.tab-content.log')
            </div>
        </div>
    </div>

    @include('pages.retailers.modals.edit')
    @include('pages.retailers.modals.image')
    @include('pages.retailers.modals.loyalty')
    @include('pages.retailers.modals.contract')
    @include('pages.retailers.modals.unfinished')
    @include('pages.retailers.modals.block')
    @include('pages.retailers.modals.delete')
    @include('pages.retailers.modals.delete-img')

    @if($retailer->notPublish())
    @include('pages.retailers.modals.notice')
    @endif

    @include('pages.retailers.modals.unblock')

@stop


@section('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.2/moment.min.js"></script>

    @if($locale !== 'en')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.2/locale/{{ $locale }}.js"></script>
    @endif

    <script type="text/javascript" src="{{ asset('assets/packages/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/packages/datatable/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/packages/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="{{ asset('assets/packages/aehlke-tag-it/js/tag-it.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/packages/filer/js/jquery.filer.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/packages/icheck/icheck.min.js') }}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <script type="text/javascript">
        window.retailer_id = '{{ $retailer->id }}';
        window.retailer_status  = '{{ $retailer->status->id }}';
        window.isPublish = {{ $retailer->notPublish() ? 'false' : 'true' }};
        $(function () {
            window.setTimeout(function () {
                var $select = $('.retailer-status');
                $select.css({ visibility: 'visible'});
            }, 50);

            $('body').css('overflow-y', 'scroll');
        })
    </script>
@stop


