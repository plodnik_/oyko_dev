<div class="modal fade" id="editPos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-clear="clear" data-cancel="esc">
    <div class="modal-dialog" role="document">
        <form id="edit-pos-form" class="form-horizontal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="img-modal-label">{{ trans('retailer.edit-POS') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-bottom: 15px;">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="edit_pos_name_rl" class="col-md-4 control-label oyko-label required">
                                    {{ trans('retailer.name-reg') }}
                                </label>
                                <div class="col-md-8">
                                        <textarea id="edit_pos_name_rl"
                                                  class="form-control"
                                                  name="name_rl"
                                                  rows="1"
                                                  maxlength="40"></textarea>
                                    <div class="max-length">
                                        <span class="desc">{{ str_replace('{n}', 40, trans('interface.max-length-n-char'))}}</span>
                                        <span class="left-desc">{{ trans('interface.char-left') }}:
                                                <span class="left">40</span>
                                            </span>
                                    </div>
                                    <span class="help-block" style="display: none">
                                        </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit_pos_name_en" class="col-md-4 control-label oyko-label required">
                                    {{ trans('retailer.name-en') }}
                                </label>
                                <div class="col-md-8">
                                        <textarea id="edit_pos_name_en"
                                                  class="form-control"
                                                  name="name_en"
                                                  rows="1"
                                                  maxlength="40"></textarea>
                                    <div class="max-length">
                                        <span class="desc">{{ str_replace('{n}', 40, trans('interface.max-length-n-char'))}}</span>
                                        <span class="left-desc">{{ trans('interface.char-left') }}:
                                                <span class="left">40</span>
                                            </span>
                                    </div>
                                    <span class="help-block" style="display: none">
                                        </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit_pos_address_rl" class="col-md-4 control-label oyko-label required">
                                    {{ trans('retailer.address-reg') }}
                                </label>
                                <div class="col-md-8">
                                        <textarea id="edit_pos_address_rl"
                                                  class="form-control"
                                                  name="address_rl"
                                                  rows="1"
                                                  maxlength="80"></textarea>
                                    <div class="max-length">
                                        <span class="desc">{{ str_replace('{n}', 80, trans('interface.max-length-n-char'))}}</span>
                                        <span class="left-desc">{{ trans('interface.char-left') }}:
                                                <span class="left">80</span>
                                            </span>
                                    </div>
                                    <span class="help-block" style="display: none">
                                        </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit_pos_address_en" class="col-md-4 control-label oyko-label required">
                                    {{ trans('retailer.address-en') }}
                                </label>
                                <div class="col-md-8">
                                        <textarea id="new_pos_address_en"
                                                  class="form-control"
                                                  name="address_en"
                                                  rows="1"
                                                  maxlength="80"></textarea>
                                    <div class="max-length">
                                        <span class="desc">{{ str_replace('{n}', 80, trans('interface.max-length-n-char'))}}</span>
                                        <span class="left-desc">{{ trans('interface.char-left') }}:
                                                <span class="left">80</span>
                                            </span>
                                    </div>
                                    <span class="help-block" style="display: none">
                                        </span>
                                </div>
                            </div>
                            <hr>

                            <div class="form-group">
                                <label for="edit_latitude" class="col-md-4 control-label oyko-label required">
                                    {{ trans('words.latitude') }}
                                </label>
                                <div class="col-md-8">
                                    <input id="edit_latitude" type="text" class="form-control" name="latitude">
                                    <span class="help-block" style="display: none">
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit_longitude" class="col-md-4 control-label oyko-label required">
                                    {{ trans('words.longitude') }}
                                </label>
                                <div class="col-md-8">
                                    <input id="edit_longitude" type="text" class="form-control" name="longitude">
                                    <span class="help-block" style="display: none">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="pos_id">
                    <button type="submit" class="btn btn-custom" id="save-pos">{{ trans('words.save') }}</button>
                    <a href="javascript:;" data-dismiss="modal" class="a_close">{{ trans('words.cancel') }}</a>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    var button_pdf_title = '{{ trans('retailer.add-file') }} .pdf';
    var file_selected = '{{ trans('interface.file-selected') }}';
</script>