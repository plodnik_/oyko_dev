<div class="modal fade" id="editContract" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-clear="clear" data-confirm="enter" data-cancel="esc">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="img-modal-label">{{ trans('retailer.t-tech') }}</h4>
            </div>
            <div class="modal-body">

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-md-5 text-right" style="padding-top: 5px;">{{ trans('retailer.cont-number') }}</div>
                    <div class="col-md-7">
                        <input type="text" class="form-control" name="number" value="{{ $retailer->contract->number }}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 text-right" style="padding-top: 5px;">{{ trans('retailer.cont-scan') }}</div>
                    <div class="col-md-7">
                        <input type="file" name="file" class="up-pdf">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-custom" id="save-contract" role="confirm">{{ trans('words.save') }}</button>
                <a href="javascript:;" data-dismiss="modal" class="a_close">{{ trans('words.cancel') }}</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var button_pdf_title = '{{ trans('retailer.add-file') }} .pdf';
    var file_selected = '{{ trans('interface.file-selected') }}';
</script>