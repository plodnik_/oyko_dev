<div class="modal fade" id="editRetailer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-clear="save" data-cancel="esc">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" id="edit-retailer" action="{{ url('/'.$locale.'/admin/retailer/save/'.$retailer->id) }}" method="post" data-error="{{ trans('error.not-required') }}">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">{{ trans('retailer.edit-retailer')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('retailer_work_schemes_id') ? ' has-error' : '' }}">
                                <label for="new_scheme" class="col-md-4 control-label oyko-label required">
                                    {{ trans('retailer.work-scheme-r') }}
                                </label>
                                <div class="col-md-8">
                                    <select id="new_role" class="form-control select2i" name="retailer_work_schemes_id" required>
                                        @foreach($schemes as $scheme)
                                            <option value="{{ $scheme->id }}"
                                                    @if(old('retailer_work_schemes_id'))
                                                        {{ old('retailer_work_schemes_id')==$scheme->id ? 'selected' : '' }}
                                                    @else
                                                        {{ $retailer->retailer_work_schemes_id==$scheme->id ? 'selected' : '' }}
                                                    @endif
                                            >
                                                {{ $scheme->{'name_'.$locale} }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <span class="help-block" {{ $errors->has('retailer_work_schemes_id') ? '':'style="display: none"'}}>
                                        {{ $errors->has('retailer_work_schemes_id') ? $errors->first('retailer_work_schemes_id') : '' }}
                                    </span>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('retailer_regional_id') ? ' has-error' : '' }}">
                                <label for="new_region" class="col-md-4 control-label oyko-label required">
                                    {{ trans('retailer.region') }}
                                </label>
                                <div class="col-md-8">
                                    <select id="new_region" class="form-control select2i" name="retailer_regional_id" required>
                                        @foreach($regions as $region)
                                            <option value="{{ $region->id }}"
                                                @if(old('retailer_regional_id'))
                                                    {{ old('retailer_regional_id')==$region->id ? 'selected' : '' }}
                                                @else
                                                    {{ $retailer->retailer_regional_id==$region->id ? 'selected' : '' }}
                                                @endif
                                            >
                                                {{ $region->{'name_'.$locale} }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <span class="help-block" {{ $errors->has('retailer_regional_id') ? '':'style="display: none"'}}>
                                        {{ $errors->has('retailer_regional_id') ? $errors->first('retailer_regional_id') : '' }}
                                    </span>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group{{ $errors->has('name_rl') ? ' has-error' : '' }}">
                                <label for="new_name_rl" class="col-md-4 control-label oyko-label required">
                                    {{ trans('retailer.name-reg-r') }}
                                </label>

                                <div class="col-md-8">

                                    <input id="new_name_rl"
                                           type="text"
                                           class="form-control"
                                           name="name_rl"
                                           maxlength="40"
                                           required
                                           value="{{ old('name_rl') ? old('name_rl') : $retailer->name_rl }}">

                                    <div class="max-length">
                                        <span class="desc">{{ str_replace('{n}', 40, trans('interface.max-length-n-char'))}}</span>
                                        <span class="left-desc">{{ trans('interface.char-left') }}:
                                            <span class="left">
                                                @if(old('name_rl'))
                                                {{ (40-mb_strlen(old('name_rl'))) }}
                                                @else
                                                {{ (40-mb_strlen($retailer->name_rl))}}
                                                @endif
                                            </span>
                                        </span>
                                    </div>
                                    <span class="help-block" {{ $errors->has('name_rl') ? '':'style="display: none"'}}>
                                        {{ $errors->has('name_rl') ? $errors->first('name_rl') : '' }}
                                    </span>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('name_en') ? ' has-error' : '' }}">
                                <label for="new_name_en" class="col-md-4 control-label oyko-label required">
                                    {{ trans('retailer.name-en-r') }}
                                </label>

                                <div class="col-md-8">
                                    <input id="new_name_en"
                                           type="text"
                                           class="form-control"
                                           name="name_en"
                                           maxlength="40"
                                           required
                                           value="{{ old('name_en') ? old('name_en') : $retailer->name_en }}">

                                    <div class="max-length">
                                        <span class="desc">{{ str_replace('{n}', 40, trans('interface.max-length-n-char'))}}</span>
                                        <span class="left-desc">{{ trans('interface.char-left') }}:
                                            <span class="left">
                                                @if(old('name_en'))
                                                    {{ (40-mb_strlen(old('name_en'))) }}
                                                @else
                                                    {{ (40-mb_strlen($retailer->name_en))}}
                                                @endif
                                            </span>
                                        </span>
                                    </div>
                                    <span class="help-block" {{ $errors->has('name_en') ? '':'style="display: none"'}}>
                                        {{ $errors->has('name_en') ? $errors->first('name_en') : '' }}
                                    </span>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                <label for="new_category" class="col-md-4 control-label oyko-label required">
                                    {{ trans_choice('retailer.categories', 3) }}
                                </label>

                                <div class="col-md-8">
                                    <div class="row categories">
                                        @foreach($categories as $category)
                                        <div class="col-md-6">
                                            <input id="cat_{{ $category->id }}"
                                                   type="checkbox" value="{{ $category->id }}"
                                                   name="category[]"
                                                   @if(is_array(old('category')))
                                                        {{ in_array($category->id,old('category')) ? ' checked' : '' }}
                                                       @if(count(old('category')) >=3)
                                                        data-hidden="{{ !in_array($category->id,old('category')) ? 'true' : 'false' }}"
                                                       @endif
                                                    @else

                                                    <?php $checked = $retailer->categories->where('retailer_category_id',$category->id)->where('retailer_id', $retailer->id)->first() ? true : false?>

                                                       {{ $checked ? ' checked' : '' }}
                                                       @if($retailer->categories->count()>=3)
                                                        data-hidden="{{ $checked ? 'false' : 'true' }}"
                                                       @endif
                                                   @endif
                                            >
                                            <label for="cat_{{ $category->id }}" class="c-label">
                                                {{ $category->{'name_'.$locale} }}
                                            </label>
                                        </div>
                                        @endforeach
                                        @if($retailer->categories->count()>=3)
                                        <script type="text/javascript">
                                            window.maxCategories = true;
                                        </script>
                                        @endif
                                    </div>
                                    <div class="max-length">
                                        <span>{{ str_replace('{n}', 3, trans_choice('retailer.max-count-categories',3)) }}</span>
                                    </div>
                                    <span class="help-block" {{ $errors->has('category') ? '':'style="display: none"'}}>
                                        {{ $errors->has('category') ? $errors->first('category') : '' }}
                                    </span>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('retailer_barcode_id') ? ' has-error' : '' }}">
                                <label for="new_barcode" class="col-md-4 control-label oyko-label required">
                                    {{ trans('retailer.barcode-standard') }}
                                </label>
                                <div class="col-md-8">
                                    <select id="new_barcode" class="form-control select2i" name="retailer_barcode_id" required">
                                        @foreach($barcode_standards as $barcode)
                                            <option value="{{ $barcode->id }}"
                                            @if(old('retailer_barcode_id'))
                                                {{ old('retailer_barcode_id')==$barcode->id ? 'selected' : '' }}
                                            @else
                                                {{ $retailer->retailer_barcode_id==$barcode->id ? 'selected' : '' }}
                                            @endif
                                            >
                                                {{ $barcode->bar_code_standard_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <span class="help-block" {{ $errors->has('retailer_barcode_id') ? '':'style="display: none"'}}>
                                        {{ $errors->has('retailer_barcode_id') ? $errors->first('retailer_barcode_id') : '' }}
                                    </span>
                                </div>
                            </div>
                            <hr>

                            <div class="form-group{{ $errors->has('phone_first') ? ' has-error' : '' }}">
                                <label for="new_phone_1" class="col-md-4 control-label oyko-label required">{{ trans('words.phone') }} 1</label>
                                <div class="col-md-8">

                                    <input id="new_phone_1"
                                           type="text"
                                           class="form-control"
                                           name="phone_first"
                                           required
                                           value="{{ old('phone_first') ? old('phone_first') : $retailer->phone_first }}">

                                    <span class="help-block" {{ $errors->has('phone_first') ? '':'style="display: none"'}}>
                                        {{ $errors->has('phone_first') ? $errors->first('phone_first') : '' }}
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="new_phone_2" class="col-md-4 control-label oyko-label">{{ trans('words.phone') }} 2</label>
                                <div class="col-md-8">

                                    <input id="new_phone_2"
                                           type="text"
                                           class="form-control"
                                           name="phone_second"
                                           value="{{ old('phone_second') ? old('phone_second') : $retailer->phone_second }}">

                                    <span class="help-block" style="display: none">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="new_phone_free" class="col-md-4 control-label oyko-label">{{ trans('words.free-phone') }}</label>
                                <div class="col-md-8">

                                    <input id="new_phone_free"
                                           type="text"
                                           class="form-control"
                                           name="free_phone"
                                           value="{{ old('free_phone') ? old('free_phone') : $retailer->free_phone }}">

                                    <span class="help-block" style="display: none">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="new_email" class="col-md-4 control-label oyko-label">E-mail</label>
                                <div class="col-md-8">
                                    <input id="new_email"
                                           type="text"
                                           class="form-control"
                                           name="email"
                                           value="{{ old('email') ? old('email') : $retailer->email }}">

                                    <span class="help-block" style="display: none">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="new_url" class="col-md-4 control-label oyko-label">URL</label>
                                <div class="col-md-8">

                                    <input id="new_url"
                                           type="text"
                                           class="form-control"
                                           name="url"
                                           value="{{ old('url') ? old('url') : $retailer->url }}">

                                    <span class="help-block" style="display: none">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group{{ $errors->has('company_description_rl') ? ' has-error' : '' }}">
                                <label for="new_desc_rl" class="col-md-4 control-label oyko-label required">{{ trans('retailer.decs-company-reg') }}</label>
                                <div class="col-md-8">

                                    <textarea id="new_desc_rl"
                                              class="form-control"
                                              name="company_description_rl"
                                              maxlength="140"
                                              rows="1"
                                              required>{{ old('company_description_rl') ? old('company_description_rl') : $retailer->company_description_rl }}</textarea>

                                    <div class="max-length">
                                        <span class="desc">{{ str_replace('{n}', 140, trans('interface.max-length-n-char'))}}</span>
                                        <span class="left-desc">{{ trans('interface.char-left') }}:
                                            <span class="left">
                                                @if(old('company_description_rl'))
                                                    {{ (140-mb_strlen(old('company_description_rl'))) }}
                                                @else
                                                    {{ (140-mb_strlen($retailer->company_description_rl))}}
                                                @endif
                                            </span>
                                        </span>
                                    </div>
                                    <span class="help-block" {{ $errors->has('company_description_rl') ? '':'style="display: none"'}}>
                                        {{ $errors->has('company_description_rl') ? $errors->first('company_description_rl') : '' }}
                                    </span>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('company_description_en') ? ' has-error' : '' }}">
                                <label for="new_desc_en" class="col-md-4 control-label oyko-label required">{{ trans('retailer.decs-company-en') }}</label>
                                <div class="col-md-8">

                                    <textarea id="new_desc_en"
                                              class="form-control"
                                              name="company_description_en"
                                              maxlength="140"
                                              rows="1"
                                              required>{{ old('company_description_en') ? old('company_description_en') : $retailer->company_description_en }}</textarea>

                                    <div class="max-length">
                                        <span class="desc">{{ str_replace('{n}', 140, trans('interface.max-length-n-char'))}}</span>
                                        <span class="left-desc">{{ trans('interface.char-left') }}:
                                            <span class="left">
                                                @if(old('company_description_en'))
                                                    {{ (140-mb_strlen(old('company_description_en'))) }}
                                                @else
                                                    {{ (140-mb_strlen($retailer->company_description_en))}}
                                                @endif
                                            </span>
                                        </span>
                                    </div>
                                    <span class="help-block" {{ $errors->has('company_description_en') ? '':'style="display: none"'}}>
                                        {{ $errors->has('company_description_en') ? $errors->first('company_description_en') : '' }}
                                    </span>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label for="new_keywords" class="col-md-4 control-label oyko-label required">{{ trans('retailer.keywords-separated') }}</label>
                                <div class="col-md-8">
                                    <ul id="tag-it-keywords">
                                        @if(old('keyword'))
                                            @if(strpos(old('keyword'), ','))
                                                @foreach(explode(',', old('keyword')) as $key)
                                                    <li>{{ $key }}</li>
                                                @endforeach
                                            @else
                                                <li>{{ old('keyword') }}</li>
                                            @endif
                                        @else
                                            @if(strpos($retailer->keyword, ','))
                                                @foreach(explode(',', $retailer->keyword) as $key)
                                                    <li>{{ $key }}</li>
                                                @endforeach
                                            @else
                                                <li>{{ $retailer->keyword }}</li>
                                            @endif
                                        @endif
                                    </ul>
                                    <span class="help-block" style="display: none">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('color') ? ' has-error' : '' }}">
                                <label for="new_corp_color" class="col-md-4 control-label oyko-label required">{{ trans('retailer.corp-color') }}</label>
                                <div class="col-md-8" style="position: relative;">
                                    <input id="new_corp_color"
                                           type="text"
                                           class="form-control"
                                           name="color"
                                           value="{{ old('color') ? old('color') : $retailer->color }}"
                                           required>
                                    <span class="help-block" {{ $errors->has('color') ? '':'style="display: none"'}}>
                                        {{ $errors->has('color') ? $errors->first('color') : '' }}
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-8 col-md-offset-4 success-status">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-custom submit" data-type="create">{{ trans('words.save') }}</button>
                    <a href="javascript:;" data-dismiss="modal" class="a_close">{{ trans('words.cancel') }}</a>
                </div>
            </form>
        </div>
    </div>
</div>
@if(session('error_create'))
    <script type="text/javascript">
        $(function(){
            $('#createRetailer').modal('show');
        })
    </script>
@endif