<div class="modal fade" id="notice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-clear="save" data-confirm="enter" data-cancel="esc">
    <div class="modal-dialog mini-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="img-modal-label">{{ trans('retailer.notic-publish') }}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center" style="padding: 30px 0;">
                        <input type="checkbox" id="send_notice" class="icheck" value="1" checked>
                        <span>{{ trans('retailer.notice-send') }}</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-custom" id="publish-retailer" role="confirm">{{ trans('words.publish') }}</button>
                <a href="javascript:;" data-dismiss="modal" class="a_close">{{ trans('words.cancel') }}</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var button_pdf_title = '{{ trans('retailer.add-file') }} .pdf';
    var file_selected = '{{ trans('interface.file-selected') }}';
</script>