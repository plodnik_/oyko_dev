<div class="modal fade" id="posUpload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-clear="clear" data-confirm="enter" data-cancel="esc">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="img-modal-label">{{ trans('interface.upload-file') }}</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-5 text-right" style="padding-top: 5px;">{{ trans('interface.choose-file') }}</div>
                    <div class="col-md-7">
                        <input type="file" name="file" class="up-csv">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-custom" id="upload-pos" role="confirm">{{ trans('words.upload') }}</button>
                <a href="javascript:;" data-dismiss="modal" class="a_close">{{ trans('words.cancel') }}</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var button_csv_title = '{{ trans('retailer.add-file') }} .csv';
    var file_selected = '{{ trans('interface.file-selected') }}';
</script>