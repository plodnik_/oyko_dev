<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-clear="empty" data-confirm="enter" data-cancel="esc">
    <div class="modal-dialog mini-modal" role="document" style="width: 45%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="img-modal-label" style="color: #761c19">{{ trans('retailer.delete-r') }}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p class="second">{{ sprintf(trans('retailer.sure-delete'), $retailer->name_en) }}</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-custom" id="retailer-del" role="confirm">{{ trans('words.delete') }}</button>
                <a href="javascript:;" data-dismiss="modal" class="a_close">{{ trans('words.cancel') }}</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var button_pdf_title = '{{ trans('retailer.add-file') }} .pdf';
    var file_selected = '{{ trans('interface.file-selected') }}';
</script>