<div class="modal fade" id="block" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-clear="clear" data-confirm="enter" data-cancel="esc">
    <div class="modal-dialog mini-modal" role="document"{!!  mb_strlen($retailer->name_en)>20 ? ' style="width: 45%"' : ''  !!}>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="img-modal-label">{{ trans('retailer.block-r') }}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p class="second">{{ sprintf(trans('retailer.sure-block'), $retailer->name_en) }}</p>
                        <div class="row form-group">
                            <div class="col-md-4">
                                <p class="second" style="text-align: right">{{ trans('retailer.why-block') }}:</p>
                            </div>
                            <div class="col-md-8">
                                <textarea name="reason" id="reason-block" rows="5" class="form-control" maxlength="255"></textarea>
                                <div class="max-length">
                                    <span class="desc">{{ str_replace('{n}', 255, trans('interface.max-length-n-char'))}}</span>
                                    <span class="left-desc">{{ trans('interface.char-left') }}:
                                        <span class="left">255</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-custom disable" id="retailer-bl" role="confirm">{{ trans('words.block') }}</button>
                <a href="javascript:;" data-dismiss="modal" class="a_close">{{ trans('words.cancel') }}</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var button_pdf_title = '{{ trans('retailer.add-file') }} .pdf';
    var file_selected = '{{ trans('interface.file-selected') }}';
</script>