<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-clear="empty"  data-confirm="enter" data-cancel="esc">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="img-modal-label"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5">
                        <div id="preview">
                            <div class="jFiler-items jFiler-row">
                                <ul class="jFiler-items-list jFiler-items-grid">
                                    <li class="jFiler-item" data-jfiler-index="0" style="">
                                        <div class="jFiler-item-container">
                                            <div class="jFiler-item-inner">
                                                <div class="jFiler-item-thumb">
                                                    <div class="jFiler-item-thumb-image">
                                                        <img src="" draggable="false">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <form id="form-img" enctype="multipart/form-data">
                            <input type="file" name="image" id="filer_input" multiple="multiple">
                        </form>
                        <script>
                            var field_title = '{{ trans('words.picture') }}';
                            var button_title = '{{ trans('retailer.add-file').'.png' }}';
                            var limit_error = '{{ trans_choice('error.files-limit',1) }}';
                            var type_error  = '{{ trans('error.file-type') }}';
                            var size_error  = '{{ trans('error.file-size') }}';
                        </script>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-custom" id="save-img" role="confirm">{{ trans('words.save') }}</button>
                <a href="javascript:;" data-dismiss="modal" class="a_close">{{ trans('words.cancel') }}</a>
            </div>
        </div>
    </div>
</div>