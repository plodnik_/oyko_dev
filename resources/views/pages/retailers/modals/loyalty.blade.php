<div class="modal fade" id="editLoyalty" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-clear="clear" data-confirm="enter" data-cancel="esc">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="img-modal-label">{{ trans('retailer.t-loyalty') }}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5 text-right">{{ trans('interface.file-or-link') }}</div>
                    <div class="col-md-4 text-center">
                        <input type="radio" name="type"  id="link-l" class="icheck" value="link" checked>
                        <span style="margin: 0 10px;">{{ trans('interface.add-link') }}</span>
                    </div>
                    <div class="col-md-3 text-center">
                        <input type="radio" name="type" id="pdf-l" class="icheck" value="pdf">
                        <span style="margin: 0 10px;">{{ trans('interface.upload-pdf') }}</span>
                    </div>
                </div>
                <hr>
                <div class="row fields-lr" style="margin-bottom: 15px;">
                    <div class="col-md-5 text-right" style="padding-top: 5px;">{{ trans('retailer.loyalty-reg') }}</div>
                    <div class="col-md-7 link-b">
                        <input type="text" class="form-control" name="program_description_url_rl" value="{{ $retailer->program_description_url_rl }}">
                    </div>
                    <div class="col-md-7 pdf-b" style="display: none">
                        <input type="file" name="file_rl" class="up-pdf" data-jfiler-name="657624">
                    </div>
                </div>
                <div class="row fields-en">
                    <div class="col-md-5 text-right" style="padding-top: 5px;">{{ trans('retailer.loyalty-en') }}</div>
                    <div class="col-md-7 link-b">
                        <input type="text" class="form-control" name="program_description_url_en" value="{{ $retailer->program_description_url_en }}">
                    </div>
                    <div class="col-md-7 pdf-b" style="display: none">
                        <input type="file" name="file_en" class="up-pdf">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-custom" id="save-loyalty" role="confirm">{{ trans('words.save') }}</button>
                <a href="javascript:;" data-dismiss="modal" class="a_close">{{ trans('words.cancel') }}</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var button_pdf_title = '{{ trans('retailer.add-file') }} .pdf';
    var file_selected = '{{ trans('interface.file-selected') }}';
</script>