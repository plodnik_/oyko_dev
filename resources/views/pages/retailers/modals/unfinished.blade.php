<div class="modal fade" id="unfinished" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-clear="empty"  data-confirm="enter" data-cancel="esc">
    <div class="modal-dialog mini-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="img-modal-label before-publish-t" style="color: #761c19">{{ trans('retailer.before-publish-t') }}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <p class="col-md-9 col-md-offset-1 intro-text" style="color: #777">{{ trans('retailer.before-publish') }}:</p>
                    <p class="col-md-8 col-md-offset-2 errors"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-custom" data-dismiss="modal" role="confirm">OK</button>
            </div>
        </div>
    </div>
</div>