<label for="select-status" class="title-status">{{ trans('words.status') }}:</label>
@if($current_user->hasRole('admin') || $current_user->hasRole('moderator'))
    <select name="status" id="select-status" class="select-status" style="width: 130px; float: right;">
        @foreach($statuses as $status)
            <option data-color="{{ $status_settings['colors'][$status->id] }}"
                    value="{{ $status->id }}" {{ $status->id == $retailer->retailer_status_id ? 'selected' : '' }}
                    data-show="{{ $status_settings['show'][$status->id] }}"
                    data-title="{{ $status->{'name_'.$locale} }}"
                    data-add="{{ $status->{'add_name_'.$locale} }}"
            >{{ $status->{'do_name_'.$locale} }}</option>
        @endforeach
        <option value="delete" data-show="{{ $status_settings['show']['delete'] }}">{{ trans('words.delete') }}</option>
    </select>
    @if($retailer->retailer_status_id !== 2)
        <script>
            window.isPublish = true;
        </script>
    @endif
@else
    <span class="status" style="color: {{ $status_settings['colors'][$retailer->retailer_status_id] }}">
        {{ $retailer->status->{'name_'.$locale} }}
    </span>
@endif