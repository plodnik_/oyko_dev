<button
   class="edit-pos"
   data-toggle="modal"
   data-target="#editPos"
   data-id="{{ $pos->id }}"
   data-name_rl="{{ $pos->name_rl }}"
   data-name_en="{{ $pos->name_en }}"
   data-address_rl="{{ $pos->address_rl }}"
   data-address_en="{{ $pos->address_en }}"
   data-latitude="{{ $pos->latitude }}"
   data-longitude="{{ $pos->longitude }}">
    <span class="icon-pencil"></span>
</button>