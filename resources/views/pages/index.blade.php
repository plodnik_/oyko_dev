@extends('layouts.app')

@section('active.statistics')class="active"@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">{{ trans('home.retailers') }}</h1>
        </div>
        <div class="row">
            <div class="col-md-5 text-right">
                <h3 class="label-o">{{ trans('home.total-retailers') }}:</h3>
            </div>
            <div class="col-md-7">
                <span class="text-value">{{ $retailers->count() }}</span>
            </div>
        </div>
        @foreach($statuses as $status)
        <div class="row">
            <div class="col-md-5 text-right">
                <h3 class="label-o">{{ trans('home.with-status') }} "{{ $status->{'name_'.$locale} }}":</h3>
            </div>
            <div class="col-md-7">
                <span class="text-value">{{ $retailers->where('retailer_status_id', $status->id)->count() }}</span>
            </div>
        </div>
        @endforeach
        <hr>
        <div class="col-md-12">
            <h1 class="page-title">{{ trans('home.cards') }}</h1>
        </div>
        <div class="row">
            <div class="col-md-5 text-right">
                <h3 class="label-o">{{ trans('home.total-cards') }}:</h3>
            </div>
            <div class="col-md-7">
                <span class="text-value">{{ $pos->count() }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 text-right">
                <h3 class="label-o">{{ trans('home.average-cards') }}:</h3>
            </div>
            <div class="col-md-4">
                <span class="text-value">{{ $average }}</span>
            </div>
        </div>
    </div>
</div>
@stop
