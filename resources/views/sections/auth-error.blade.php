<div class="modal fade" id="alertLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content grey">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="message">
                            {{ $errors->first('email') }}
                        </div>
                        <button type="button" data-dismiss="modal" class="btn btn-custom pull-right">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        $('#alertLogin').modal('show');

    })
</script>