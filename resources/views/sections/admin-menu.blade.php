<div class="collapse navbar-collapse" id="app-navbar-collapse">
    <!-- Left Side Of Navbar -->
    <ul class="nav navbar-nav navbar-right main-menu">
        <li @yield('active.statistics')><a href="/{{ $locale }}/admin">{{ trans('menu.statistics') }}</a></li>
        <li @yield('active.retailers')><a href="{{ url('/'.$locale.'/admin/retailers') }}">{{ trans('menu.retailers') }}</a></li>
        @if($current_user->bo_user_role_id == 1)
            <li @yield('active.users')><a href="{{ url('/'.$locale.'/admin/users') }}">{{ trans('menu.users') }}</a></li>
        @endif
        <li @yield('active.marketing')><a href="#">{{ trans('menu.marketing') }}</a></li>
    </ul>
</div>