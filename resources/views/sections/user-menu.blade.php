<div class="user-block col-md-4">
    <ul class="nav navbar-nav navbar-right user">
        <li class="dropdown">
            <span class="a_bro"> {{ $current_user_role }}</span>|<a href="#" class="dropdown-toggle a_bro_user_link" data-toggle="dropdown"><span class="a_bro_user_name">{{ $current_user->email }}</span> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a href="#" data-toggle="modal" data-target="#updateMy">{{ trans('words.profile') }}</a></li>
                <li><a href="{{ url('/'.$locale.'/admin/logout') }}" style="font-weight: 600">{{ trans('auth.logout') }}</a></li>
            </ul>
        </li>
    </ul>
</div>