<?php

namespace App\Http\Middleware;

use Closure;

class CustomAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->check()){

            return redirect('/admin/login');
            
        }

        if (!auth()->user()->isActive()) {

            auth()->logout();

            return redirect('/admin/login')->with('error', trans('error.account-blocked'));

        }
        
        return $next($request);
    }
}
