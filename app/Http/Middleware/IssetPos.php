<?php

namespace App\Http\Middleware;

use App\Models\Pos;
use Closure;

class IssetPos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->route('id')) {

            if (!Pos::find($request->route('id'))) return abort(404);

        }

        return $next($request);
    }
}
