<?php

namespace App\Http\Middleware;

use App\Models\Retailer;
use Closure;

class IssetRetailer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $retailer = Retailer::find((int)$request->route('retailer_id'));

        if (!$retailer) return abort(404);

        return $next($request);
    }
}
