<?php

namespace App\Http\ViewComposers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AdminComposer
{

    protected $user;

    public function __construct(){

        $this->user = Auth::user();

    }

    /**
     *  Добавление к представлениям глобальньІх данньІх
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('locale', App::getLocale()); // Текущая локализация
        $view->with('current_user', $this->user);// АвторизированньІй пользователь

        if (Auth::check()) {
            $key_role = 'role_name_' . app()->getLocale();
            $view->with('current_user_role', $this->user->role->$key_role); // Роль авторизированого пользователя
        }

    }

}