<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pos;
use App\Models\Retailer;
use App\Models\RetailerStatus;
use App\Http\Controllers\Controller;

use App\Http\Requests;

class AdminController extends Controller
{

    public function index(){

        $retailer = Retailer::get(['id', 'name_en', 'retailer_status_id']);
        $pos = Pos::all();

        return view('pages.index',[

            'retailers' =>  $retailer,
            'statuses'  =>  RetailerStatus::all(['id', 'name_'.app()->getLocale()]),
            'pos'   =>  $pos,
            'average'   => ($retailer->count() > 0 && $pos->count() >0) ? ceil($pos->count()/$retailer->count()) : 0

        ]);

    }

}
