<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pos;
use App\Models\Retailer;
use App\Models\RetailerLog;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Chumper\Datatable\Facades\DatatableFacade as Datatable;

use App\Http\Requests;

class APIController extends Controller
{

    /**
     *  Запрос данньІх для таблицьІ
     *
     * @param $type string Тип запрашиваемьІх данньІх
     * @return null
     */
    public function table(Request $request, $type){

        $dataTable = null;

        if ($type == 'user') {

            $dataTable = Datatable::collection(User::where('bo_user_role_id', '<>', 1)->get([
                    'id',
                    'bo_user_role_id',
                    'bo_user_status_id',
                    'email',
                    'name',
                    'surename',
                    'registration_date'
                ]))
                ->showColumns('email','name','surename')
                ->addColumn('role', function ($row){

                    return $row->role->{'role_name_'.app()->getLocale()};

                })
                ->addColumn('registration_date', function ($row){

                    return $row->registration_date;

                })
                ->addColumn('status', function ($row){

                    $class = $row->status->id == 1 ? 'green' : 'red';

                    return '<span class="'.$class.'">'.$row->status->{'status_'.app()->getLocale()}.'</span>';
                })
                ->addColumn('edit', function ($row){
                    return view('pages.users.edit-button', ['user' => $row])->render();
                })
                ->searchColumns('email')
                ->make();

        }

        if ($type === 'retailer') {

            $query = Retailer::query();

            # ----- Применение фильтров ----- #
            if ((int)$request->region !== 0) {
                $query->whereIn('retailer_regional_id', explode(',',$request->region));
            }
            if ((int)$request->category !== 0) {

                if (strpos($request->category,',') !== false) {

                    foreach (explode(',', $request->category) as $category) {

                        $query->whereHas('categories', function ($sql) use ($category) {
                            $sql->where('retailer_category_id', $category);
                        });
                    }

                } else{
                    $query->whereHas('categories', function ($sql) use ($request) {
                        $sql->where('retailer_category_id', $request->category);
                    });
                }
            }
            if ((int)$request->scheme !== 0) {
                $query->whereIn('retailer_work_schemes_id', explode(',',$request->scheme));
            }
            if ((int)$request->status !== 0) {
                $query->whereIn('retailer_status_id', explode(',',$request->status));
            }

            $collection = $query->get([
                'id',
                'pos_img',
                'oyko_id',
                'retailer_contract_id',
                'retailer_regional_id',
                'name_rl',
                'name_en',
                'retailer_work_schemes_id',
                'date_add',
                'date_update',
                'retailer_status_id'
            ]);

            $dataTable = Datatable::collection($collection)
                ->addColumn('pos_icon', function ($row){
                    $html = '<div class="corp-color" style="background-color: '.$row->color.'"></div>';

                    if ($row->pos_img) {
                        $html = '<div class="pos-img" style="background: url('.asset($row->pos_img).') center center / contain no-repeat">';
                    }

                    return $html;
                })
                ->addColumn('contract', function ($row){

                    $link = '<span style="display:none" class="retailer-link">'.url('/'.app()->getLocale().'/admin/retailer/'.$row->id.'/edit').'</span>';
                    return $row->oyko_id.$link;

                })
                ->addColumn('region', function ($row){

                    return $row->region->{'name_'.app()->getLocale()};

                })
                ->addColumn('name_rl', function ($row){

                    return mb_strlen($row->name_rl) > 22 ? mb_substr($row->name_rl, 0, 21).'...' : $row->name_rl;
                })
                ->addColumn('name_en', function ($row){
                    return mb_strlen($row->name_en) > 22 ? mb_substr($row->name_en, 0, 21).'...' : $row->name_en;
                })
                ->addColumn('categories', function ($row){

                    $categories = '';
                    $first = true;

                    foreach ($row->categories as $category) {

                        if (!$first) $categories .= ', ';

                        $categories .= $category->category->{'name_'.app()->getLocale()};

                        $first = false;

                    }

                    return $categories;
                })
                ->addColumn('work_scheme', function ($row){

                    return $row->workScheme->{'name_'.app()->getLocale()};

                })
                ->addColumn('pos_count', function ($row){

                    return $row->pos->count();

                })
                ->addColumn('added', function ($row){

                    return $row->date_add->format('d.m.Y');

                })
                ->addColumn('last_update', function ($row){

                    if (!$row->date_update) return 'n/a';

                    return $row->date_update->format('d.m.Y');

                })
                ->addColumn('status', function ($row){

                    // Цвета для статусов
                    $statuses_colors = [
                        1 => '#2ecc71',
                        2 => '#FFDF2B',
                        3 => '#e74c3c'
                    ];

                    return '<span status="'.$row->status->id.'" oyko_id="'.$row->oyko_id.'" id="retailer_status_'.$row->oyko_id.'" style="color:'.$statuses_colors[$row->status->id].'">'.$row->status->{'name_'.app()->getLocale()}.'</span>';

                })
                ->searchColumns('contract', 'name_en', 'name_rl')
                ->make();

        }



        return $dataTable;

    }

    public function fromRetailer(Request $request, $id, $type)
    {
        $retailer = Retailer::find($id);

        # Проверка существования ритейла
        if (!$retailer) return about(404);

        if ($type == 'pos') {

            $query = Pos::where('retailer_id', $id);

            // ФильтрьІ

            // По статусу
            if ($request->status) {

                if (strpos($request->status, ',')) {

                    $query->whereIn('pos_status_id', explode(',', $request->status));
                } else {

                    $query->where('pos_status_id', $request->status);
                }

            }

            // По дате
            if ($request->from && $request->to) {

                if ($from = Carbon::createFromFormat('d.m.Y', $request->from) !== false) {
                    if ($to = Carbon::createFromFormat('d.m.Y', $request->to) !== false) {

                        $query->where('date_add', '>', date('Y-m-d h:i:s', strtotime($request->from)));
                        $query-> where('date_add', '<', date('Y-m-d h:i:s', strtotime($request->to)));

                    }
                }

            }


            $collection = $query->get();

            $dataTable = Datatable::collection($collection)
                ->addColumn('inc', function ($row){
                    $result  = '';
                    if ($row->id < 100) $result .= '0';
                    if ($row->id < 10)  $result .= '0';

                    return $result . $row->id;
                })
                ->addColumn('check', function ($row){
                    return '<input type="checkbox" class="icheck-pos check-one" value="'.$row->id.'">';
                })
                ->addColumn('name_rl', function ($row){

                    return mb_strlen($row->name_rl) > 14 ? mb_substr($row->name_rl,0,12).'...' : $row->name_rl;

                })
                ->addColumn('name_en', function ($row){

                    return mb_strlen($row->name_en) > 14 ? mb_substr($row->name_en,0,12).'...' : $row->name_en;

                })
                ->addColumn('address_rl', function ($row){

                    return mb_strlen($row->address_rl) > 14 ? mb_substr($row->address_rl,0,12).'...' : $row->address_rl;

                })
                ->addColumn('address_en', function ($row){

                    return mb_strlen($row->address_en) > 14 ? mb_substr($row->address_en,0,12).'...' : $row->address_en;

                })
                ->addColumn('latitude', function ($row){

                    return $row->latitude;

                })
                ->addColumn('longitude', function ($row){

                    return $row->longitude;

                })
                ->addColumn('date_add', function ($row){

                    return $row->date_add->format('d.m.Y');
                })
                ->addColumn('date_update', function ($row){

                    return $row->date_update ? $row->date_update->format('d.m.Y') : '';
                })
                ->addColumn('status', function ($row){

                    $colors = [
                        1   =>  '#2ecc71',
                        2   =>  '#e74c3c'
                    ];

                    return '<span style="color:'.$colors[$row->pos_status_id].'">'.$row->status->{'name_'.app()->getLocale()}.'</span>';
                })
                ->addColumn('edit-pos', function ($row){
                    return view('pages.retailers.elements.edit-button', ['pos' => $row])->render();
                })
                ->searchColumns('name_en', 'name_rl')
                ->make();

            return $dataTable;

        }

        if ($type == 'log') {

            // ФильтрьІ
            $query = RetailerLog::where('retailer_id', $id);

            // ФильтрьІ
            $collection = $query->get();

            $dataTable = Datatable::collection($collection)
                ->addColumn('inc', function ($row){

                    return '';
                })
                ->addColumn('action', function ($row){

                    return $row->action->{'description_'.app()->getLocale()};

                })
                ->addColumn('reason', function ($row){

                    return $row->reason;

                })
                ->addColumn('user', function ($row){

                    return $row->user->email;

                })
                ->addColumn('date', function ($row){

                    return $row->updated_at->format('d.m.Y H:i');

                })
                ->addColumn('empty', function (){

                    return '';

                })
                ->searchColumns('action', 'reason')
                ->make();

            return $dataTable;

        }

    }
}
