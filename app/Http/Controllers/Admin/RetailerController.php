<?php

namespace App\Http\Controllers\Admin;

use App\Models\PosStatus;
use App\Models\Retailer;
use App\Models\RetailerBarcode;
use App\Models\RetailerCategory;
use App\Models\RetailerContract;
use App\Models\RetailerLog;
use App\Models\RetailerRegion;
use App\Models\RetailerStatus;
use App\Models\RetailerWorkScheme;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Chumper\Datatable\Facades\DatatableFacade as Datatable;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class RetailerController extends Controller
{

    protected $errors;

    /**
     *  Получение информации о статусах по параметрам oyko_id(массив)
     */
    public function page_status_update(Request $request)
    {
        $retailer = new Retailer();
        echo json_encode($retailer->get_statuses_by_ids($request->oyko_ids));
        die;
    }

    /**
     *  Таблица ритейлеров
     */
    public function index()
    {

        # Заголовки таблицьІ
        $dt = Datatable::table()
            ->addColumn(
                trans('retailer.pos-icon'),
                'OYKO ID',
                trans('retailer.region'),
                '<b>'.trans('words.name').'</b> <small>'.trans('retailer.ln-reg').'</small>',
                '<b>'.trans('words.name').'</b> <small>'.trans('retailer.ln-en').'</small>',
                Lang::choice('retailer.categories', 1),
                trans('retailer.work-scheme'),
                trans('retailer.pos_count_caption'),
                trans('words.added'),
                trans('words.edited'),
                trans('words.status')
            )
            ->setUrl(url('/'.app()->getLocale().'/admin/api/table/retailer'))
            ->setOptions([
                'language' => config('datatable.'.app()->getLocale()),
                'aoColumns' => [
                    ['bSortable' => false],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                ],
                'order'    => [[4,'desc']],
                'rowCallback' => "function (row, data, index) {
                            if (!window.page_retailter_oyko_ids)
                                window.page_retailter_oyko_ids = [];
                                
                            var arr     =  data[10].split('oyko_id=\"');
                            var arr2    = arr[1].split('\"');
                            window.page_retailter_oyko_ids.push(arr2[0]);
                        }"

            ])
            ->setClass('table dataTable no-footer retailer-table')
            ->render();

        return view('pages.retailers.index', [
            'table' =>  $dt,
            'regions'   =>  RetailerRegion::all(['id', 'name_'.app()->getLocale()]),
            'categories'=>  RetailerCategory::all(['id', 'name_'.app()->getLocale()]),
            'schemes'   =>  RetailerWorkScheme::all(['id', 'name_'.app()->getLocale()]),
            'statuses'  =>  RetailerStatus::all(['id', 'name_'.app()->getLocale()]),
            'barcode_standards'  => RetailerBarcode::all(['id', 'bar_code_standard_name']),
            'table_url' => url('/'.app()->getLocale().'/admin/api/table/retailer')
        ]);

    }


    /**
     *  Сохранение нового ритейла
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request, $id = false)
    {
        # Если есть id - значит идет редактирование
        if ($id) {

            $retailer = Retailer::find($id);

            # Проверка существования ритейла
            if (!$retailer) return about(404);

        } else $retailer = new Retailer();

        // Валидация
        $validate = $this->checkData($request->all(), $id ? 'update' : 'create');

        # Если данньІе не прошли валидацию
        if (!$validate['check']) {

            return redirect()
                ->back()
                ->withErrors($this->errors)
                ->withInput()
                ->with('error_create', true);

        }


        if (!$id) { // Сохранение нового

            $retailer = $retailer->saveNew($request->all());

            $retailer->saveCategories($request->category);

            $retailer->logStatus(10006);

        } else {    // Обновление существующего

            $retailer->update($request->all());

            $retailer->updateCategories($request->category);

        }

        $retailer->update(['date_update' => Carbon::now()]);


        return redirect('/'.app()->getLocale().'/admin/retailer/'.$retailer->id.'/edit');

    }


    /**
     *  Страница редактирования ритейла
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {

        $retailer = Retailer::find($id);

        # Проверка существования ритейла
        if (!$retailer) return about(404);

        # Кастомизация селекта статуса
        $status_settings = [

            // Цвета для статусов
            'colors'    =>  [
                1 => '#2ecc71',
                2 => '#FFDF2B',
                3 => '#e74c3c'
            ],
            // Правила отображения статусов
            'show'  =>  [
                1   =>  '2,3',
                2   =>  '',
                3   =>  '1',
                'delete'    =>  '2,3'
            ],
        ];


        # Причина блокировки (если она существует)
        $reason = '';
        if ($retailer->retailer_status_id == 3) {

            $reason = RetailerLog::where('retailer_id', $retailer->id)
                ->where('log_actions_id',10001)
                ->orderBy('id', 'desc')
                ->first();

            if (!$reason) $reason = '';
        }

        # Таблица POS
        $posDT = Datatable::table()
            ->addColumn(
                trans('#'),
                '<input type="checkbox" id="pos-all" class="icheck-pos" value="1">',
                '<b>'.trans('words.name').'</b> <small>'.trans('retailer.ln-reg').'</small>',
                '<b>'.trans('words.name').'</b> <small>'.trans('retailer.ln-en').'</small>',
                '<b>'.trans('words.address').'</b> <small>'.trans('retailer.ln-reg').'</small>',
                '<b>'.trans('words.address').'</b> <small>'.trans('retailer.ln-en').'</small>',
                trans('words.latitude'),
                trans('words.longitude'),
                trans('words.added'),
                trans('words.edited'),
                trans('words.status'),
                ''
            )
            ->setUrl(url('/'.app()->getLocale().'/admin/api/table/retailer/'.$id.'/pos'))
            ->setOptions([
                'language' => config('datatable.'.app()->getLocale()),
                'order'    => [[0,'desc']],
                'aoColumns' => [
                    ['bSortable' => false],
                    ['bSortable' => false],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => false]
                ],
                'drawCallback' =>   "function () { redrawPosTableCallback(); }",
            ])
            ->setClass('table dataTable no-footer pos-table')
            ->setId('pos-dataTable')
            ->render();

        $logDT = Datatable::table()
            ->addColumn(
                '#',
                trans('words.action'),
                trans('words.reason'),
                trans('retailer.made-by'),
                trans('words.date-time'),
                ''
            )
            ->setUrl(url('/'.app()->getLocale().'/admin/api/table/retailer/'.$id.'/log'))
            ->setId('logs-dataTable')
            ->setOptions([
                'language' => config('datatable.'.app()->getLocale()),
                'order'    => [[0,'desc']],
                'aoColumns' => [
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => false],
                ],
                'fnRowCallback' => "function( row, data, iDisplayIndex ) {
                     var info = logsTable.page.info();
                     var page = info.page;
                     var length = info.length;
                     var index = (page * length + (iDisplayIndex +1));
                     $('td:eq(0)', row).html(index);
                  }"
            ])
            ->setClass('table dataTable no-footer log-table')
            ->render();


        return view('pages.retailers.edit', [
            'retailer'  =>  $retailer,
            'statuses'  =>  RetailerStatus::forEditSelect($id)->get(),
            'regions'   =>  RetailerRegion::all(['id', 'name_'.app()->getLocale()]),
            'categories'=>  RetailerCategory::all(['id', 'name_'.app()->getLocale()]),
            'schemes'   =>  RetailerWorkScheme::all(['id', 'name_'.app()->getLocale()]),
            'pos_statuses'  => PosStatus::all(['id', 'name_'.app()->getLocale()]),
            'status_settings'   =>  $status_settings,
            'barcode_standards' => RetailerBarcode::all(['id', 'bar_code_standard_name']),
            'reason'    =>  $reason,

            'posDT'     =>  $posDT,
            'logDT'     =>  $logDT,
        ]);

    }


    /**
     *  Сохранение картинок ритейла
     *
     * @param Request $request
     * @return array
     */
    public function image(Request $request)
    {

        # Правила проверки
        $rules = [
            'retailer_id'   =>  'required|numeric|exists:retailer,id',
            'img_name'      =>  'required|in:big_img,small_img,pos_img,logo_img',
            'file'  =>  'required|max:10000|mimes:png,jpeg,bmp,gif,svg'
        ];

        # Проверка
        $validator = Validator::make($request->all(), $rules);

        # если есть ошибки
        if ($validator->fails()) return ['check' => false, 'message' => trans('error.error500')];


        # Сохранение
        $retailer = Retailer::find($request->retailer_id)
            ->saveImage($request->img_name, $request->file);

        return ['check' => true, 'file_path' => asset($retailer->{$request->img_name})];

    }


    /**
     *  Удаление изображения ритейла
     *
     * @param $retailer_id
     * @param $img_type
     * @return array
     */
    public function delete_image($retailer_id, $img_type)
    {
        $retailer = Retailer::find($retailer_id);

        $retailer->update([$img_type => '', 'date_update' => Carbon::now()]);

        return ['check' => true];
    }

    /**
     *  Сохранение программьІ лояльности
     *
     * @param Request $request
     * @return array
     */
    public function loyalty(Request $request)
    {
        $rules = [
            'retailer_id'   =>  'required|numeric|exists:retailer,id',
            'type'  =>  'required|in:link,pdf',
            'file_rl'  =>  'sometimes|max:10024',
            'file_en'  =>  'sometimes|max:10024',
        ];

        # Проверка
        $validator = Validator::make($request->all(), $rules);

        # если есть ошибки
        if ($validator->fails()) return ['check' => false, 'message' => trans('error.error500')];

        # Сохранение
        $retailer = Retailer::find($request->retailer_id)
            ->saveLoyalty($request)
            ->logStatus(10007);

        return [
            'check' =>  true,
            'link'  =>  [

                'rl'    =>  $retailer->program_description_url_rl,
                'en'    =>  $retailer->program_description_url_en

            ]];


    }
    /**
     *  Сохранение контракта
     *
     * @param Request $request
     * @return array
     */
    public function contract(Request $request)
    {
        $rules = [
            'retailer_id'   =>  'required|numeric|exists:retailer,id',
            'number'  =>  'sometimes',
            'scan'  =>  'sometimes|max:4024',
        ];

        # Проверка
        $validator = Validator::make($request->all(), $rules);

        # если есть ошибки
        if ($validator->fails()) return ['check' => false, 'message' => trans('error.error500')];

        # Сохранение
        Retailer::find($request->retailer_id)
            ->logStatus(10008)
            ->saveContract($request);

        $contract = Retailer::find($request->retailer_id)->contract;


        return [
            'check' =>  true,
            'contract'  =>  [

                'number'    =>  $contract->number,
                'scan'    =>  $contract->url ? asset($contract->url) : ''

            ]];


    }


    /**
     *  Проверка возможности установить новьІй статус
     *
     * @param $id
     * @param $status
     * @return array
     */
    public function check($id, $status)
    {
        $retailer = Retailer::find($id);

        # Проверка существования ритейла
        if (!$retailer) return ['check' => false, 'messages' => trans('error.error500')];

        # Перед публикацией надо проверть присудствие всех обьязательньІх данньІх
        if ($status == 1) {

            $errors = ''; // Ошибки проверки

            if (!$retailer->big_img)    $errors .= trans('retailer.add-large').'<br>';
            if (!$retailer->small_img)  $errors .= trans('retailer.add-small').'<br>';
            if (!$retailer->pos_img)    $errors .= trans('retailer.add-pos').'<br>';
            if (!$retailer->logo_img)   $errors .= trans('retailer.add-logo').'<br>';

            if ($errors) {  // Сообщаем пользователю об ошибках

                return ['check' => false, 'messages' => $errors];
            }

            return ['check' => true];
        }

        return ['check' => false, 'messages' => trans('error.error500')];
    }


    /**
     * @param Request $request
     * @param $id
     * @param $status
     * @return array
     */
    public function status(Request $request, $id, $status)
    {
        $retailer = Retailer::find($id);

        # Проверка существования ритейла
        if (!$retailer) return about(404);

        $response = ['check' => true];

        $validate = Validator::make($request->all(), ['reason' => 'sometimes|required|max:255']);

        if ($validate->fails()) {
            return ['check' => false, 'message' => $validate->errors()->first('reason')];
        }

        # Опубликовано
        if ($status == 1) {

            $response['message'] = '<b>'.sprintf(trans('retailer.ok-publush'), $retailer->name_en).'</b>';

            if ($retailer->notPublish()) {

                if ($request->send == 'true') $response['message'] .= '<p class="second">'.trans('retailer.ok-push').'</p>';
                $retailer->logStatus(10001);

            } else {

                $retailer->logStatus(10001, $request->reason);
            }
        }

        # Заблокировано
        if ($status == 3) {

            $retailer->logStatus(10001, $request->reason);

            $response['message'] = sprintf(trans('retailer.ok-block'), $retailer->name_en);
        }

        $retailer->update(['retailer_status_id' => $status]);

        return $response;

    }

    /**
     * Запрос о данньІх аналитики ритейла
     *
     * @param $retailer_id
     * @return array
     */
    public function analytics($retailer_id)
    {

        # Ответ
        $response   =   ['check' => true, 'data' => []];

        $retailer   =   Retailer::find($retailer_id);

        $response['data']['pos_count']  =   $retailer->pos->count();

        return $response;

    }


    /**
     *  Валидация данньІх пользователя (AJAX проверка)
     *
     * @param Request $request
     * @param bool $id          ID пользователя (если валидируются данньІе для обновленния)
     * @return \Illuminate\Http\JsonResponse
     */
    public function valid(Request $request, $id=false)
    {
        // НачальньІй ответ обработки запроса
        $response = ['check' => true, 'message' => 'ok'];

        # Если $id не равно false - тогда запрашивается валидация данньІх для уже существующего ритейла
        if ($id) {

            $retailer = Retailer::find((int)$id);

            # Если обновляемого ритейла не существует
            if (!$retailer) {
                return response()->json(['check' => false, 'message' => trans('error.error500')]); // ответ с ошибкой
            }
        }

        // Валидация
        $validate = $this->checkData($request->all(), $id ? 'update' : 'create');

        # Если данньІе не прошли валидацьІю
        if (!$validate['check']) {
            $response['check']   = false;                   // ОшибочньІй статус
            $response['message'] = $validate['message'];    // Сообщения ошибок
        }

        return response()->json($response);

    }

    /**
     *  Удаление ритейла
     *
     * @param $id
     * @return array
     */
    public function delete($id)
    {
        $retailer = Retailer::find($id);

        # Проверка существования ритейла
        if (!$retailer) return ['check' => false, 'messages' => trans('error.error500')];

        $retailer->delete();

        return ['check' => true, 'message' => sprintf(trans('retailer.delete-success'), $retailer->name_en)];

    }

    /**
     *  Функция валидации
     *
     * @param $data Request ДанньІе для валидации
     * @param $type string  тип - update или create
     * @return array Результат валидации - ключ check сообщаем о статусе проверки, messages - содержит ошибки (если они есть)
     */
    private function checkData($data, $type){

        $result = ['check' => true];

        # Правила проверки
        $rules = [

            'create' => [
                'retailer_work_schemes_id'  =>  'required|exists:retailer_work_schemes,id',
                'category.*'   =>  'sometimes|exists:retailer_category,id',
                'company_description_rl'    =>  'required|max:140',
                'company_description_en'    =>  'required|max:140',
                'retailer_regional_id'  =>  'required|exists:retailer_regional,id',
                'retailer_barcode_id'   =>  'required|exists:retailer_barcode,id',
                'phone_first'   =>  'required',
                'category'  =>  'required',
                'name_rl'   =>  'required|max:40',
                'name_en'   =>  'required|max:40',
                'color' =>  ['required','regex:/^#([0-9a-f]{6}|[0-9a-f]{3})$/i'],
                'keyword'   =>  'required'

            ],

            'update' => [
                'bo_user_role_id'   =>  'sometimes|required|exists:bo_user_role,id|not_in:1',
                'password'          =>  'sometimes|min:6',
                'bo_user_status_id' =>  'sometimes|required|exists:bo_user_status,id',
                'name'              =>  'sometimes|max:25',
                'surename'          =>  'sometimes|max:25',
                'keyword'   =>  'required'

            ]
        ];

        $messages = ['category.required' => trans('error.set_category')];

        # Если типа проверки существует
        if (array_key_exists($type,$rules)) {

            # Проверка
            $validator = Validator::make($data, $rules[$type],$messages);

            # ДанньІе не прошли проверку
            if ($validator->fails()) {

                # Сохраняем для передачи через редирект в представление (при необходимости)
                $this->errors = $validator->errors();

                $result['check'] = false; // ОшибочниьІй статус проверки

                $errors = $validator->errors();

                # Добавление ошьІбок к результату
                foreach ($errors->messages() as $field=>$e) {

                    $result['message'][$field] = ucfirst($errors->first($field));

                    // Обработка ошибок категорий
                    if (strpos($field, 'category.') !== false && !isset($result['message']['category'])) {

                        $result['message']['category'] = $errors->first($field);
                    }

                }
            }

        } else {

            $result['check'] = false;

        }


        return $result;

    }

}
