<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use App\Models\UserStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Chumper\Datatable\Facades\DatatableFacade as Datatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    /**
     *  ВьІвод таблицьІ пользователей
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        # Заголовки таблицьІ
        $dt = Datatable::table()
            ->addColumn(
                trans('user.login_email'),
                trans('user.first_name'),
                trans('user.last_name'),
                trans('user.user_role'),
                trans('user.date_register'),
                trans('user.status'),
                ''
            )
            ->setUrl(url('/'.app()->getLocale().'/admin/api/table/user'))
            ->setOptions([
                'language' => config('datatable.'.app()->getLocale()),
                'order'    => [[4,'desc']],
                'aoColumns' => [
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => true],
                    ['bSortable' => false]
                ]
            ])
            ->setClass('table dataTable no-footer user-table black')
            ->render();

        return view('pages.users.index', [

            'table' => $dt,
            'roles' => Role::all('id', 'role_name_'.app()->getLocale()), // роли для вьІпадающего списка ролей
            'statuses' => UserStatus::all()                              // статусьІ для вьІпадающего списка

        ]);

    }

    /**
     *  Сохранение нового пользователя
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request){

        // НачальньІй ответ обработки запроса
        $response = ['check' => false, 'message' => trans('error.error500')];

        // Валидация
        $validate = $this->checkData($request->all(), 'create');

        # Если данньІе не прошли валидацьІю
        if (!$validate['check']) {

            $response['message'] = $validate['message'];    // Сообщения ошибок
            $response['trouble'] = 'valid';                 // Уведомление об ошибках валидации

            return response()->json($response);

        }

        User::create($request->all());

        $response['check'] =  true; // запрос вьІполене успешно
        unset($response['message']);// ошибок нет

        return response()->json($response);

    }

    /**
     *  Обновление пользователя
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){

        $user_session = Auth::user();

        // НачальньІй ответ обработки запроса
        $response = ['check' => false, 'message' => trans('error.error500')];

        $user = User::find((int)$request->get('id'));

        # Если обновляемого пользователя не существует
        if (!$user) return response()->json($response);

        # Проверка права на редактирование данньІх пользователя
        # (Если пользователь не администратор он может редактировать только свои данньІе)
        if (!$user_session->hasRole('admin') && $user_session->id !== $user->id) return response()->json($response);

        # Валидация
        $validate = $this->checkData($request->all(), 'update');

        # Если данньІе не прошли валидацьІю
        if (!$validate['check']) {

            $response['message'] = $validate['message'];    // Сообщения ошибок
            $response['trouble'] = 'valid';                 // Уведомление об ошибках валидации

            return response()->json($response);
        }

        $update = $request->all();

        # Если пароль бьІл незаполнен - удаляем его из данньІх для обновления
        if (!$update['password']) {
            unset($update['password']);
        }


        # Обновление
        $user->update($update);

        # Ответ об успешном вьІполнении запроса
        $response = ['check' => true, 'message' => trans('message.success-update')];

        return response()->json($response);

    }

    /**
     *  Валидация данньІх пользователя (AJAX проверка)
     *
     * @param Request $request
     * @param bool $id          ID пользователя (если валидируются данньІе для обновленния)
     * @return \Illuminate\Http\JsonResponse
     */
    public function valid(Request $request, $id=false)
    {
        $user_session = Auth::user();

        // НачальньІй ответ обработки запроса
        $response = ['check' => true, 'message' => 'ok'];

        # Если $id не равно false - тогда запрашивается валидация данньІх для уже существующего пользователя
        if ($id) {

            $user = User::find((int)$id);

            # Если обновляемого пользователя не существует
            if (!$user) {
                return response()->json(['check' => false, 'message' => trans('error.error500')]); // ответ с ошибкой
            }

            # Если обновляемого пользователя не существует
            if (!$user_session->hasRole('admin') && $user_session->id !== $user->id) {
                return response()->json(['check' => false, 'message' => trans('error.error500')]); // ответ с ошибкой
            }
        }

        // Валидация
        $validate = $this->checkData($request->all(), $id ? 'update' : 'create');

        # Если данньІе не прошли валидацьІю
        if (!$validate['check']) {
            $response['check']   = false;                   // ОшибочньІй статус
            $response['message'] = $validate['message'];    // Сообщения ошибок
        }

        return response()->json($response);

    }

    /**
     *  Функция валидации
     *
     * @param $data Request ДанньІе для валидации
     * @param $type string  тип - update или create
     * @return array Результат валидации - ключ check сообщаем о статусе проверки, messages - содержит ошибки (если они есть)
     */
    private function checkData($data, $type){

        $result = ['check' => true];

        # Правила проверки
        $rules = [

            'create' => [
                'bo_user_role_id' =>    'required|exists:bo_user_role,id|not_in:1',
                'email'           =>    'required|email|unique:bo_user,email',
                'password'        =>    'required|min:6'
            ],

            'update' => [
                'bo_user_role_id'   =>  'sometimes|required|exists:bo_user_role,id|not_in:1',
                'password'          =>  'sometimes|min:6',
                'bo_user_status_id' =>  'sometimes|required|exists:bo_user_status,id',
                'name'              =>  'sometimes|max:25',
                'surename'          => 'sometimes|max:25'

            ]
        ];

        $messages = ['email.exists' => trans('error.email_exist')];

        # Если типа проверки существует
        if (array_key_exists($type,$rules)) {

            # Проверка
            $validator = Validator::make($data, $rules[$type],$messages);

            # ДанньІе не прошли проверку
            if ($validator->fails()) {

                $result['check'] = false; // ОшибочниьІй статус проверки

                $errors = $validator->errors();

                # Добавление ошьІбок к результату
                foreach ($errors->messages() as $field=>$e) {

                    $result['message'][$field] = ucfirst($errors->first($field));
                }

            }

        } else {

            $result['check'] = false;

        }


        return $result;

    }

}
