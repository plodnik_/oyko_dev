<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pos;
use App\Models\Retailer;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PosController extends Controller
{

    /**
     *  Ajax сохранение POS
     *
     * @param Request $request
     * @param bool $id
     * @return array
     */
    public function save(Request $request, $retailer_id, $id = false)
    {

        // Если есть $id тогда надо обновить существующую POS
        if ($id) {
            $pos = Pos::find($id);
            // проверка существования POS
            if (!$pos) return about(404);

        } else {
            $pos = Pos::create(['retailer_id' => $retailer_id, 'bo_user_id' => Auth::user()->id]);

            $pos->retailer->logStatus(10009);
        }

        $pos->savePos($request->all());

        return ['check' => true];
    }

    /**
     *  Импрот POS из csv файла
     *
     * @param Request $request
     * @param $retailer_id
     * @return array
     */
    public function upload(Request $request, $retailer_id)
    {

        # Проверка существования файла
        if ($request->hasFile('csv_file')) {

            $new = hash('md5', time()).'.csv';

            $request->csv_file->move(public_path(), $new); // сохранение файла на сервер

            $create_arr = $this->csvToArray(public_path($new)); // Получаем массив из csv файла


            # Если парсинг csv прошел успешно
            if ($create_arr) {

                # Сначало надо проверить все данньІе
                foreach ($create_arr as $pos) {
                    $validate = $this->checkData($pos);
                    if (!$validate['check']){
                        return ['check' => false, 'message' => trans('retailer.file-is-invalid')];
                        break;
                    }
                }

                # Сохранение
                foreach ($create_arr as $pos) {

                    $pos['retailer_id'] = $retailer_id;
                    $pos['bo_user_id']  = Auth::user()->id;

                    $new = Pos::create($pos);

                    $new->savePos($pos);
                }

                Retailer::find($retailer_id)->logStatus(10010);

                # Успех
                return ['check' => true, 'message' => trans('retailer.ok-import')];


            } else { // Парсинг прошел с ошибками

                return ['check' => false, 'message' => trans('retailer.file-is-invalid')];
            }

        } else {  //  Файл не бьІл загружен
            return ['check' => false, 'message' => trans('error.error500')];
        }

    }

    public function manipulate(Request $request, $action)
    {
        $ids = strpos($request->ids, ',') ? explode(',', $request->ids) : $request->ids;

        $pos_query = is_array($ids) ? Pos::whereIn('id',$ids) : Pos::where('id', $ids);

        $pos_items = $pos_query->get();


        // Если не найдено записей
        if (empty($pos_items)) return ['check' => false, 'message' => trans('error.error500')];

        foreach ($pos_items as $pos) {

            if ($action == 'delete') {
                $pos->retailer->logStatus(10012);
                $pos->delete();
            }

            if ($action == 'block') {

                $pos->update(['pos_status_id' => 2, 'date_update' => Carbon::now()]);
                $pos->retailer->logStatus(10011);
            }
        }

        return ['check' => true, 'message' => trans('interface.success-manipulation')];

    }

    /**
     * Обработка запроса валидации
     *
     * @param Request $request
     * @return array
     */
    public function valid(Request $request)
    {
        // НачальньІй ответ обработки запроса
        $response = ['check' => true, 'message' => 'ok'];

        // Валидация
        $validate = $this->checkData($request->all());

        # Если данньІе не прошли валидацьІю
        if (!$validate['check']) {
            $response['check']   = false;                   // ОшибочньІй статус
            $response['message'] = $validate['message'];    // Сообщения ошибок
        }

        return $response;

    }

    /**
     * Валидация
     *
     * @param $data
     * @return array
     */
    protected function checkData($data)
    {
        // ИзначальньІй результат
        $result = ['check' => true];

        // Правила проверки
        $rules = [
            'name_rl'   =>  'required|max:40',
            'name_en'   =>  'required|max:40',
            'address_rl'    =>  'required|max:80',
            'address_en'    =>  'required|max:80',
            'latitude'  =>  'required|regex:/^[0-9]{1}[0-9]{1}\.[0-9]{0,8}+$/',
            'longitude' =>  'required|regex:/^[0-9]{1}[0-9]{1}\.[0-9]{0,8}+$/'
        ];

        # Проверка
        $validator = Validator::make($data, $rules);

        # ДанньІе не прошли проверку
        if ($validator->fails()) {

            # Сохраняем для передачи через редирект в представление (при необходимости)
            $this->errors = $validator->errors();

            $result['check'] = false; // ОшибочниьІй статус проверки

            $errors = $validator->errors();

            # Добавление ошьІбок к результату
            foreach ($errors->messages() as $field=>$e) {

                $result['message'][$field] = ucfirst($errors->first($field));

                // Обработка ошибок категорий
                if (strpos($field, 'category.') !== false && !isset($result['message']['category'])) {

                    $result['message']['category'] = $errors->first($field);
                }
            }
        }

        return $result;
    }

    /**
     *  Парсинг csv файла в масив
     *
     * @param string $filename
     * @param string $delimiter
     * @return array|bool
     */
    protected function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
            unlink($filename);
        }

        return $data;
    }

}
