<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RetailerRegion extends Model
{
    protected $table = 'retailer_regional';
}
