<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'bo_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surename', 'email', 'password', 'bo_user_role_id', 'bo_user_status_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'bo_user_role_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\UserStatus', 'bo_user_status_id');
    }


    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public static function dropAdmin(){

        return DB::table('bo_user')
            ->where('bo_user_role_id', 1)
            ->update(['bo_user_role_id' => 2]);

    }

    public function hasRole($role = 'admin')
    {
        $role = strtolower($role);

        return $this->role->role === $role ? true : false;
    }

    public function isActive()
    {
        return $this->status->id === 1 ? true : false;
    }
}
