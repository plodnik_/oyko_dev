<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Pos extends Model
{

    protected $table = 'pos';

    protected $dates = ['date_add', 'date_update'];

    protected $fillable = [
        'retailer_id', 'pos_status_id', 'bo_user_id', 'name_rl', 'name_en', 'address_rl', 'address_en',
        'latitude', 'longitude', 'date_add', 'date_update'
    ];

    public $timestamps = false;

    public function retailer()
    {
        return $this->belongsTo('App\Models\Retailer', 'retailer_id');

    }

    public function status()
    {
        return $this->belongsTo('App\Models\PosStatus', 'pos_status_id');
    }

    public function scopeByRetailer($query, $retailer_id)
    {
        return $query->where('retailer_id', $retailer_id);
    }

    /**
     *  Сохранение POS (используется как при создании так и при обновлении POS)
     *
     * @param $pos
     * @return bool
     */
    public function savePos($pos)
    {
        if (!$this->date_add) {
            $pos['date_add']    =   Carbon::now();
        } else {
            $pos['date_update'] = Carbon::now();
        }

        return $this->update($pos);
    }

    public function setLatitudeAttribute($value)
    {
        $this->attributes['latitude'] = (float)$value;
    }
    public function setLongitudeAttribute($value)
    {
        $this->attributes['longitude'] = (float)$value;
    }
}
