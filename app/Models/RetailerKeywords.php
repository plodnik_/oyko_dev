<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RetailerKeywords extends Model
{
    protected $table = 'retailer_key_words';

    protected $fillable = ['retailer_id', 'key_word_1', 'key_word_2', 'key_word_3', 'key_word_4', 'key_word_5'];
}
