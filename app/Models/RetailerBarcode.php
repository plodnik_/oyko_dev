<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RetailerBarcode extends Model
{
    protected $table = 'retailer_barcode';
}
