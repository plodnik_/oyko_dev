<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class RetailerStatus extends Model
{
    protected $table = 'retailer_status';

    /**
     *  ДоступньІе статусьІ при редактировании с учетом прав пользователя и состояния ритейла
     *
     * @param $query
     * @param $retailer_id
     * @return mixed
     */
    public function scopeForEditSelect($query, $retailer_id)
    {
        $user   =   Auth::user();
        $retailer   =   Retailer::find($retailer_id);

        if (!$user->hasRole('admin') && !$user->hasRole('moderator')) {
            return $query->where('id', $retailer->retailer_status_id);
        }
        if ($retailer->isPublish())
        {
            return $query->where('id','<>', 2);
        }

        return $query;

    }
}
