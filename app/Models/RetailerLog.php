<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RetailerLog extends Model
{

    protected $table = 'retailer_logs';

    protected $fillable = ['retailer_id', 'log_actions_id', 'bo_user_id', 'reason'];


    public function notPublish($retailer_id)
    {
        return $this->where('retailer_id', $retailer_id)->where('log_actions_id', 10001)->count() ? false : true;
    }

    public function action()
    {
        return $this->belongsTo('App\Models\LogAction', 'log_actions_id');
    }

    public function user()
    {
        return$this->belongsTo('App\Models\User', 'bo_user_id');
    }

}
