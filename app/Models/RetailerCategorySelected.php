<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RetailerCategorySelected extends Model
{

    protected $table = 'retailer_category_selected';

    protected $fillable = ['retailer_category_id', 'retailer_id'];

    public $timestamps = false;

    protected $primaryKey = 'retailer_category_id';

    public function category()
    {
        return $this->belongsTo('App\Models\RetailerCategory', 'retailer_category_id');
    }

    public function dropByRetailer($retailer_id)
    {
        DB::table($this->table)
            ->where('retailer_id', $retailer_id)
            ->delete();

    }
}
