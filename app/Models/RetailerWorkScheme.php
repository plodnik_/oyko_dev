<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RetailerWorkScheme extends Model
{
    protected $table = 'retailer_work_schemes';
}
