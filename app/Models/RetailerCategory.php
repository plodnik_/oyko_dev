<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RetailerCategory extends Model
{

    protected $table = 'retailer_category';

}
