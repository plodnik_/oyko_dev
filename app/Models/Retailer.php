<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Retailer extends Model
{
    protected $table = 'retailer';

    protected $fillable = [

        'retailer_status_id', 'retailer_barcode_id', 'retailer_regional_id', 'retailer_regional_id',
        'retailer_work_scheme_id', 'bo_user_id', 'qyko_id', 'name_rl', 'name_en', 'company_description_rl',
        'company_description_en', 'phone_first', 'phone_second', 'free_phone', 'email', 'url', 'color', 'keyword',
        'big_img', 'small_img', 'pos_img', 'logo_img', 'program_description_url_rl', 'program_description_url_en',
        'date_add', 'date_update', 'retailer_contract_id', 'retailer_work_schemes_id', 'bo_user_id', 'oyko_id'
    ];

    protected $dates = ['date_add', 'date_update'];

    public $timestamps = false;

    public function barcode()
    {
        return $this->belongsTo('App\Models\RetailerBarcode', 'retailer_barcode_id');
    }

    public function contract()
    {
        return $this->belongsTo('App\Models\RetailerContract', 'retailer_contract_id');
    }

    public function categories()
    {
        return $this->hasMany('App\Models\RetailerCategorySelected', 'retailer_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\RetailerStatus', 'retailer_status_id');
    }

    public function region()
    {
        return $this->belongsTo('App\Models\RetailerRegion', 'retailer_regional_id');
    }

    public function workScheme()
    {
        return $this->belongsTo('App\Models\RetailerWorkScheme', 'retailer_work_schemes_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'bo_user_id');
    }

    public function pos()
    {
        return $this->hasMany('App\Models\Pos', 'retailer_id');
    }

    /**
     *   Получение информации о статусах по параметрам oyko_id(массив)
     */
    public function get_statuses_by_ids(array $oyko_ids)
    {
        $statuses_colors = [
            1 => '#2ecc71',
            2 => '#FFDF2B',
            3 => '#e74c3c'
        ];

        $res = DB::table($this->table)
            ->leftJoin('retailer_status', $this->table.'.retailer_status_id', '=', 'retailer_status.id')
            ->select($this->table.'.oyko_id',$this->table.'.retailer_status_id','retailer_status.name_en','retailer_status.name_ru')
            ->whereIn('oyko_id',$oyko_ids)
            ->get();

        if (!$res)
            return null;

        foreach($res as $r)
            $r->status_color = $statuses_colors[$r->retailer_status_id];

        return $res;
    }

    /**
     *   Сознание нового ритейла
     *
     * @param $retailer Request Ритейл
     * @return static
     */
    public function saveNew($retailer)
    {
        $oyko_id = $this->orderBy('oyko_id', 'desc')
            ->first(['oyko_id']);

        $oyko_id = empty($oyko_id) ? 100000 : ($oyko_id->oyko_id+1);

        return $this->create(array_merge(
            [
                'retailer_status_id' => 2,  // статус неактивного
                'retailer_contract_id' => 1,// пустой контракт
                'bo_user_id'    =>  Auth::user()->id,   // пользователь которьІй создает
                'oyko_id'   => $oyko_id,
                'date_add'  =>  Carbon::now()
            ],
            $retailer
        ));
    }

    /**
     *  Сохранение данньІх об категориях
     *
     * @param $categories array Категории
     */
    public function saveCategories($categories)
    {
        foreach ($categories as $category) {

            RetailerCategorySelected::create([
                'retailer_id' => $this->id,
                'retailer_category_id' => $category
            ]);

        }
    }

    /**
     *  Обновление данньІх об категориях
     *
     * @param $categories array Категории
     */
    public function updateCategories($categories)
    {
        // Удаляем старьІе связи
        $selected = new RetailerCategorySelected();
        $selected->dropByRetailer($this->id);


        // Добавляем нужньІе связи
        foreach ($categories as $category) {
            RetailerCategorySelected::create([
                'retailer_id' => $this->id,
                'retailer_category_id' => $category
            ]);
        }
    }

    /**
     *  Сохранение изображения
     *
     * @param $img_name string Название поля куда надо сохранить изображение
     * @param $file UploadedFile Файл
     * @return $this Retailer
     */
    public function saveImage($img_name, $file)
    {

        $path = 'retailerimg/'.$this->id.'/'.$img_name.'/';   // Папка для хранения картинок ритейла
        $filename = hash('md5', time().microtime());      // название картинки

        $on_server  = public_path('uploads/'.$path); // Путь сохранения на сервере

        # Создание папки если ее не существует
        if (!Storage::exists($path)) Storage::makeDirectory($path);

        # Перемещение файла
        $file->move($on_server, $filename);

        # Сохранение данньІх в БД
        $this->update([$img_name => $filename, 'date_update' => Carbon::now()]);

        // Сохранение логов
        if ($img_name == 'big_img')     $this->logStatus(10002);
        if ($img_name == 'small_img')   $this->logStatus(10003);
        if ($img_name == 'pos_img')     $this->logStatus(10004);
        if ($img_name == 'logo_img')    $this->logStatus(10005);

        return $this;

    }

    /**
     *  сохранение лояльности
     *
     * @param $request
     * @return $this
     */
    public function saveLoyalty($request)
    {

        // Ключи файлов и полей
        $keys = [
            'file_rl'    =>  'program_description_url_rl',
            'file_en'    =>  'program_description_url_en'
        ];

        if ($request->type == 'link') {

            # Удаление старьІх файлов при необходимости
            foreach ($keys as $key) {
                if ($request->{$key} === '' && $this->{$key}) {

                    // Путь к файлу для использования с Laravel Storage
                    $path_from_storage = str_replace('uploads/', '', $this->{$key});

                    # Удаление старого файла
                    if (Storage::exists($path_from_storage)) Storage::delete($path_from_storage);
                }
            }

            // Сохранение ссьІлок
            $this->update(array_merge($request->all(),['date_update' => Carbon::now()]));

        } else {

            # Удаление старьІх файлов при необходимости
            foreach ($keys as $file=>$key) {
                if ($request->hasFile($file)) {

                    // Путь к файлу для использования с Laravel Storage
                    $path_from_storage = str_replace('uploads/', '', $this->{$key});

                    # Удаление старого файла
                    if (Storage::exists($path_from_storage)) Storage::delete($path_from_storage);

                    $path = 'retailer/'.$this->id.'/loyalty/';   // Папка для хранения файла ритейла
                    $filename = sprintf("%06d", mt_rand(1, 9999999)).'.pdf';      // название файла

                    $on_server  = public_path('uploads/'.$path); // Путь сохранения на сервере

                    # Перемещение файла
                    $request->file($file)->move($on_server, $filename);

                    # Сохранение данньІх в БД
                    $this->update([$key => $filename]);
                }
            }

        }

        return $this;

    }

    /**
     *  Сохранение контракта
     *
     * @param $request
     * @return $this
     */
    public function saveContract($request)
    {

        // Добавление нового контракта (1 устанавливается по умолчанию из-за проблем с БД)
        if ($this->contract->id == 1) {

            $contract = RetailerContract::create(['number' => $request->number]);

            $this->update(['retailer_contract_id' => $contract->id, 'date_update' => Carbon::now()]);


        } else { // Обновление существующего

            $contract = $this->contract;

            $contract->update(['number'=>$request->number]);

        }

        if ($request->hasFile('scan')) {

            $path = 'retailer/'.$this->id.'/contract/';   // Папка для хранения файла ритейла
            $filename = 'contract-scan-from-retailer('.$this->id.').pdf';      // название файла

            $on_server  = public_path('uploads/'.$path); // Путь сохранения на сервере

            # Удаляєм старьІй скан
            if (Storage::exists($path.$filename)) Storage::delete($path.$filename);


            # Перемещение файла
            $request->file('scan')->move($on_server, $filename);

            # Сохранение данньІх в БД
            $contract->update(['url' => $filename]);

        }

        return $this;
    }

    public function logStatus($action_id, $reason = 'n/a')
    {
        RetailerLog::create([
            'retailer_id'   =>  $this->id,
            'log_actions_id'    =>  $action_id,
            'bo_user_id'    =>  Auth::user()->id,
            'reason'    =>  $reason
        ]);

        $this->update(['date_update' => Carbon::now()]);

        return $this;
    }
    public function notPublish()
    {

        return RetailerLog::where('retailer_id',$this->id)->where('log_actions_id', 10001)->count() ? false :true;

    }
    public function isPublish()
    {
        return $this->retailer_status_id === 1 ? true : false;
    }
    public function isBlock()
    {
        return $this->retailer_status_id === 3 ? true : false;
    }

    /**
     *  (Accessor) Описание программьІ лояльности на язьІке региона
     *
     * @param $filename
     * @return string
     */
    public function getProgramDescriptionUrlRlAttribute($filename)
    {

        # Пустое значение
        if (!$filename) return '';

        # ссьІлку надо сформировать
        if (strpos($filename, 'http') === false){
            $path = 'uploads/retailer/'.$this->id.'/loyalty/';   // Папка для хранения файла ритейла

            return asset($path.$filename);
        }

        # Сохранена ссьІлка на другой сервер
        return $filename;

    }

    /**
     *  (Accessor) Описание программьІ лояльности на англ. язьІке
     *
     * @param $filename
     * @return string
     */
    public function getProgramDescriptionUrlEnAttribute($filename)
    {

        # Пустое значение
        if (!$filename) return '';

        # Сохранена ссьІлка на другой сервер
        if (strpos($filename, 'http') !== false) return $url;

        # ссьІлку надо сформировать
        $path = 'uploads/retailer/'.$this->id.'/loyalty/';   // Папка для хранения файла ритейла

        return asset($path.$filename);

    }


    /**
     *  (Accessor) Большое изображение картьІ
     *
     * @param $filename
     * @return string
     */
    public function getBigImgAttribute($filename)
    {
        if (!$filename) return '';

        $path = 'uploads/retailerimg/'.$this->id.'/big_img/';   // Папка для хранения картинок ритейла

        return asset($path.$filename);
    }
    /**
     *  (Accessor) Малое изображение картьІ
     *
     * @param $filename
     * @return string
     */
    public function getSmallImgAttribute($filename)
    {
        if (!$filename) return '';

        $path = 'uploads/retailerimg/'.$this->id.'/small_img/';   // Папка для хранения картинок ритейла

        return asset($path.$filename);
    }
    /**
     *
     * @param $filename
     * @return string
     */
    public function getPosImgAttribute($filename)
    {
        if (!$filename) return '';

        $path = 'uploads/retailerimg/'.$this->id.'/pos_img/';   // Папка для хранения картинок ритейла

        return asset($path.$filename);
    }
    /**
     *
     * @param $filename
     * @return string
     */
    public function getLogoImgAttribute($filename)
    {
        if (!$filename) return '';

        $path = 'uploads/retailerimg/'.$this->id.'/logo_img/';   // Папка для хранения картинок ритейла

        return asset($path.$filename);
    }


}
