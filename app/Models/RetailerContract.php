<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RetailerContract extends Model
{

    protected $table = 'retailer_contract';

    protected $fillable = ['number', 'url'];

    public $timestamps = false;

    public function retailer()
    {
        return $this->hasOne('App\Models\Retailer', 'retailer_contract_id');
    }

    public function getUrlAttribute($value)
    {
        if (!$value) return '';

        $path = 'uploads/retailer/'.$this->retailer->id.'/contract/';   // Папка для хранения файла ритейла

        return asset($path.$value);
    }

}
