<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

# Группа роутов поддерживаюших локализацию
Route::localizedGroup(function(){

    # Группа роутов для админки
    Route::group(['prefix' => 'admin'], function (){

        # Группа роутов которьІе доступньІ только неаторизированьІм пользователям
        Route::group(['middleware' => 'cGuest'], function (){

            Auth::routes();

        });

        # Группа роутов которьІе доступньІ только аторизированьІм пользователям
        Route::group(['middleware' => ['cAuth'], 'namespace' => 'Admin'], function (){

            Route::get('logout', ['as' => 'logout', 'uses' => function(){

                \Illuminate\Support\Facades\Auth::logout();

                return redirect('/admin');

            }]);

            Route::get('/', ['as' => 'admin.index', 'uses' => 'AdminController@index']);

            Route::get('users', ['as' => 'users', 'middleware'=>'only-admin','uses' => 'UserController@index']);
            Route::post('user/add', ['as' => 'user.add', 'middleware'=>'only-admin', 'uses' => 'UserController@create']);
            Route::post('user/edit', ['as' => 'user.edit', 'uses' => 'UserController@store']);
            Route::post('user/valid/{id?}', ['as' => 'user.valid',  'uses' => 'UserController@valid']);

            Route::get('retailers', ['as' => 'retailers', 'uses' => 'RetailerController@index']);
            Route::post('retailer/save/{id?}', ['as' => 'retailer.save', 'uses' => 'RetailerController@save']);
            Route::get('retailer/{id}/edit', ['as' => 'retailer.edit', 'uses' => 'RetailerController@edit']);
            Route::post('retailer/edit', ['as' => 'retailer.edit', 'uses' => 'RetailerController@store']);
            Route::post('retailer/valid/{id?}', ['as' => 'retailer.valid',  'uses' => 'RetailerController@valid']);
            Route::post('retailer/img', ['as' => 'retailer.img',  'uses' => 'RetailerController@image']);

            Route::get('retailer/{retailer_id}/delete-image/{img_type}', [
                'as' => 'retailer.delete_img',
                'middleware' => 'exists.retailer',
                'uses' => 'RetailerController@delete_image'
            ])
                ->where('retailer_id','^[0-9]+$')
                ->where('img_type', 'big_img|small_img|pos_img|logo_img');

            Route::post('retailer/loyalty', ['as' => 'retailer.loyalty',  'uses' => 'RetailerController@loyalty']);
            Route::post('retailer/contract', ['as' => 'retailer.contract',  'uses' => 'RetailerController@contract']);
            Route::post('retailer/{id}/status/{status}', ['as' => 'retailer.status',  'uses' => 'RetailerController@status'])
                ->where('id','^[0-9]+$')
                ->where('status', '1|2|3');

            Route::get('retailer/{id}/check/{status}', ['as' => 'retailer.check',  'uses' => 'RetailerController@check'])
                ->where('id','^[0-9]+$')
                ->where('status', '1|2|3');
            Route::get('retailer/{id}/delete', ['as' => 'retailer.delete',  'uses' => 'RetailerController@delete'])
                ->where('id','^[0-9]+$');
            Route::get('api/table/retailer/{id}/{type}', ['as' => 'retailer.other',  'uses' => 'APIController@fromRetailer'])
                ->where('id','^[0-9]+$')
                ->where('type','pos|log');

            Route::post('retailer/{retailer_id}/pos/save/{id?}', ['as' => 'pos.save', 'middleware' => ['exists.retailer', 'exists.pos'], 'uses' => 'PosController@save'])
                ->where('retailer_id','^[0-9]+$')
                ->where('id','^[0-9]+$');
            Route::post('retailer/{retailer_id}/pos/upload', ['as' => 'pos.upload', 'middleware' => 'exists.retailer', 'uses' => 'PosController@upload'])
                ->where('retailer_id','^[0-9]+$')
                ->where('id','^[0-9]+$');
            Route::get('retailer/{retailer_id}/analytics', ['as' => 'pos.analytics', 'middleware' => 'exists.retailer', 'uses' => 'RetailerController@analytics'])
                ->where('retailer_id','^[0-9]+$');
            Route::post('pos/valid', ['as' => 'pos.valid',  'uses' => 'PosController@valid']);
            Route::post('pos/{action}', ['as' => 'pos.manipulate',  'uses' => 'PosController@manipulate'])
                ->where('action', 'delete|block');

            Route::get('api/table/{type?}', ['uses' => 'APIController@table'])
                ->where('type', 'user|retailer');

            Route::post('retailer/page_status_update', ['uses' => 'RetailerController@page_status_update']);

        });

    });
});

Route::get('/home', 'HomeController@index');
