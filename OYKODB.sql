-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Час створення: Лис 07 2016 р., 22:17
-- Версія сервера: 5.6.33
-- Версія PHP: 7.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `oyko_new`
--

-- --------------------------------------------------------

--
-- Структура таблиці `bo_user`
--

CREATE TABLE `bo_user` (
  `id` int(10) NOT NULL,
  `bo_user_role_id` int(11) NOT NULL DEFAULT '3',
  `bo_user_status_id` int(11) NOT NULL DEFAULT '1',
  `email` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surename` varchar(255) DEFAULT NULL,
  `registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `bo_user`
--

INSERT INTO `bo_user` (`id`, `bo_user_role_id`, `bo_user_status_id`, `email`, `password`, `name`, `surename`, `registration_date`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'plodnik@gmail.com', '$2y$10$U6yQ5yPApvbIrnnghUglc.Uj58nXjCMAF1PVwP2j9HZ7BaLlntBaS', 'David', '', '2016-10-13 12:11:07', 'u0grfKEJ3UWpDuKmf8mpYDfigJyP1Vuj8bGzZQwndbImr6tTArBj0Sqq0flF', '2016-10-13 06:11:07', '2016-11-07 18:13:02'),
(3, 3, 1, 'mary.doe@gmail.com', '$2y$10$wTiKzvd3rPY6IN9gcbX.N.UCPmC7k.YC/H0vHs1vTqVXM8dvfPeka', NULL, NULL, '2016-10-14 01:55:39', NULL, '2016-10-13 19:55:39', '2016-10-14 06:24:35'),
(4, 3, 1, 'john.doe@gmail.com', '$2y$10$gARqmGuaUxvsqwTZStUuo.xA/zzpqiJ8mB6gwe.AkJRiIPKzmE6Im', 'John', 'Doe', '2016-10-14 01:56:37', 'qlki7aVXdhvhJdF1YF3RD57eg6lv0a4UYcSp2BIh6FLA7rkbmsZSAmLsW0jw', '2016-10-13 19:56:37', '2016-10-14 06:26:20'),
(5, 2, 2, 'root', '$2y$10$nISzPEo6jd8dMAZq3r.pEOxclrrFpH6/vx2ED8UVmRHgoqK.8LvJG', NULL, NULL, '2016-10-14 12:34:50', 'jFQJZQFruqsZoKz9ceiNKM1FvOCDHyICC4EVDA6vBcbAtkAr99Tw1TmhevPo', '2016-10-14 06:34:50', '2016-10-14 06:56:40'),
(6, 2, 1, 'superuser', '$2y$10$0g63qpAAEXlwoCPrnjEyJuLvMjFVmoz44o3vVKwqXvxM686G99gem', 'Superuser', 'Administrator', '2016-10-14 12:39:19', 'nQ9aYCxVGcwKa2lt9vxMF0voaiP4jSTb51D9wTxnZ6zInciRlyB7DE2Mz314', '2016-10-14 06:39:19', '2016-10-19 06:16:13'),
(10, 3, 2, 'free', '$2y$10$zIAqvkhHuXa5eMPB7FdF4.tARTe3JedkfpBhyOxvTD9qX.jUM8aMe', NULL, NULL, '2016-10-16 19:34:09', 'o0JFEtbjnk8f13BtyHkUBmOESioWhkWw7Ta6zdRTQUjJc6yJuyJiKtwgTPeI', '2016-10-16 16:34:09', '2016-10-17 11:10:45'),
(12, 2, 2, 'fad@nd.com', '$2y$10$2q0KykJBRujiNWRyxCdinehNFvCpEje5aH214xhvOASccsTKtTBKO', NULL, NULL, '2016-10-19 09:14:16', NULL, '2016-10-19 06:14:16', '2016-10-19 06:15:39'),
(13, 3, 2, 'jsafgd@sd.bf', '$2y$10$W3vH9NTFxJIknRhamrMCXOIGT.oJI/4qpj.9I/GzaLlu6ltlPnvM6', NULL, NULL, '2016-10-21 06:59:57', NULL, '2016-10-21 03:59:57', '2016-10-21 04:00:15'),
(14, 3, 1, 'test@test.com', '$2y$10$gC5tiF2f/nk7PiO6tMkq/.j3uprJu5/mJAIzFKpBR4Gd2bwRBHZyK', NULL, NULL, '2016-10-27 09:54:02', 'z4Plke2GM4huSu44vdNFQah3F5V734kF1xHLf6gv3mHokFfCTVl2CsEFHVgm', '2016-10-27 06:54:02', '2016-11-07 15:33:22'),
(15, 2, 1, 'moder@gmail.com', '$2y$10$KgFkKaJV95Nngpg539DF9OBMD4gfzBtmGYmHN6w2kWKsnIaMnp.u.', NULL, NULL, '2016-11-07 18:32:50', 'lsokurXdHAQJFt1KyzbKP1Enh7VAT8GBEuJMRNbfHxm2rTWPBJize9tCbKrV', '2016-11-07 16:32:50', '2016-11-07 16:33:57'),
(17, 2, 1, 'moder2@gmail.com', '$2y$10$2QI8y81Kxc9PunyvCwnNJuiamLF/RgTHjIMYBcpacXxLaiq2F3BQS', NULL, NULL, '2016-11-07 18:34:40', NULL, '2016-11-07 16:34:40', '2016-11-07 16:34:40'),
(20, 3, 1, 'user@gmail.com', '$2y$10$H8zVcHC/swYKjkvIvIswG.LPO8JP780TuDXZFCr4ztXW7gtEcSvc6', NULL, NULL, '2016-11-07 18:48:58', NULL, '2016-11-07 16:48:58', '2016-11-07 16:48:58'),
(22, 2, 1, 'moder3@gmail.com', '$2y$10$6Ioa.wtF/Z.IXTX8ba5MWu1i1LdFbrUFJa3H76pduePDGNHlKYGwO', NULL, NULL, '2016-11-07 18:49:35', NULL, '2016-11-07 16:49:35', '2016-11-07 16:49:35'),
(26, 2, 1, 'moder6@gmail.com', '$2y$10$dpF36ur.ZFNz27e5oYf1E.EvlNXj/M3HOxKf..T0jL1sAsHzN4RvO', NULL, NULL, '2016-11-07 20:02:12', 'R2wtCf0Xn0ypjt3Kj5WoU3FV78JFPUKVzF4djgn5KQUojEr6uhJINs6likGm', '2016-11-07 18:02:12', '2016-11-07 18:14:50');

-- --------------------------------------------------------

--
-- Структура таблиці `bo_user_password_resets`
--

CREATE TABLE `bo_user_password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `bo_user_permissions`
--

CREATE TABLE `bo_user_permissions` (
  `id` int(5) NOT NULL,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `bo_user_permissions`
--

INSERT INTO `bo_user_permissions` (`id`, `name`, `display_name`, `description`) VALUES
(10001, 'Creating new user', 'Creating new user', 'Creating new OYKO backoffice user'),
(10002, 'Changing retailer status', 'Changing retailer status', 'Changing retailer status in the OYKO backoffice system (published/ moderation/ blocked)'),
(10003, 'Adding new retailer', 'Adding new retailer', 'Adding new retailer in the OYKO backoffice system'),
(10004, 'Editing existing retailer', 'Editing existing retailer', 'Data editing of the already added retailer in the OYKO backoffice system (everything except retailer status)'),
(10005, 'Viewing backoffice statistics', 'Viewing backoffice statistics', 'Viewing OYKO backoffice statistics ("Statistics" button/bookmark is available)'),
(10006, 'Viewing retailer analytics', 'Viewing retailer analytics', 'Viewing retailer analytics ("Analytics" button/bookmark is available)'),
(10007, 'Viewing retailer logs', 'Viewing retailer logs', 'Viewing retailer logs ("Logs" button/bookmark is available)');

-- --------------------------------------------------------

--
-- Структура таблиці `bo_user_permissions_role`
--

CREATE TABLE `bo_user_permissions_role` (
  `bo_user_permissions_id` int(10) NOT NULL,
  `bo_user_role_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `bo_user_permissions_role`
--

INSERT INTO `bo_user_permissions_role` (`bo_user_permissions_id`, `bo_user_role_id`) VALUES
(10001, 1),
(10002, 1),
(10003, 1),
(10004, 1),
(10005, 1),
(10006, 1),
(10007, 1),
(10002, 2),
(10003, 2),
(10004, 2),
(10005, 2),
(10006, 2),
(10003, 3),
(10004, 3);

-- --------------------------------------------------------

--
-- Структура таблиці `bo_user_role`
--

CREATE TABLE `bo_user_role` (
  `id` int(2) NOT NULL,
  `role` varchar(10) NOT NULL,
  `role_name_en` varchar(20) NOT NULL,
  `role_name_ru` varchar(20) NOT NULL,
  `description` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `bo_user_role`
--

INSERT INTO `bo_user_role` (`id`, `role`, `role_name_en`, `role_name_ru`, `description`) VALUES
(1, 'admin', 'Admin', 'Администратор', NULL),
(2, 'moderator', 'Moderator', 'Модератор', NULL),
(3, 'user', 'User', 'Пользователь', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `bo_user_status`
--

CREATE TABLE `bo_user_status` (
  `id` int(2) NOT NULL,
  `status_en` varchar(45) NOT NULL,
  `status_ru` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `bo_user_status`
--

INSERT INTO `bo_user_status` (`id`, `status_en`, `status_ru`) VALUES
(1, 'active', 'Активен'),
(2, 'blocked', 'Заблокирован');

-- --------------------------------------------------------

--
-- Структура таблиці `log_actions`
--

CREATE TABLE `log_actions` (
  `id` int(5) NOT NULL,
  `description_en` varchar(255) NOT NULL,
  `description_ru` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `log_actions`
--

INSERT INTO `log_actions` (`id`, `description_en`, `description_ru`) VALUES
(10001, 'Retailer status change', 'Изменение статуса ретейла'),
(10002, 'Change a large cover', 'Изменение большое обожки'),
(10003, 'Changing the small cover', 'Изменение малой обложка'),
(10004, 'Changing the map icons', 'Изменение иконки карты'),
(10005, 'Change logo', 'Изменение логотипа'),
(10006, 'General data change', 'Изменение общей информации'),
(10007, 'Loyalty program description change', 'Изменение описания программы лояльности'),
(10008, 'Technical data change', 'Изменение технических данных'),
(10009, 'POS addition', 'Добавление POS'),
(10010, 'POS file import', 'Импорт файла с POS'),
(10011, 'POS status change', 'Изменение статуса POS'),
(10012, 'POS deleted', 'Удаление POS');

-- --------------------------------------------------------

--
-- Структура таблиці `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2016_10_16_190310_add_role_column_frole_bo_user_role_table', 1),
(5, '2016_10_17_095433_rename_user_status', 2),
(7, '2016_10_19_200351_add_auto_increment_from_retailer_table', 3);

-- --------------------------------------------------------

--
-- Структура таблиці `office_statistics`
--

CREATE TABLE `office_statistics` (
  `total_retailers_count` int(11) DEFAULT '0',
  `total_retailers_published` int(11) DEFAULT '0' COMMENT 'total retailers with a status of published',
  `total_retailers_approval` int(11) DEFAULT '0' COMMENT 'total retailers with a status of approval',
  `total_retailers_blocked` int(11) DEFAULT '0' COMMENT 'total retailers with a status of blocked\n',
  `total_cards_added` int(11) DEFAULT '0',
  `average_card_one_retail` int(11) DEFAULT '0' COMMENT 'an average of one retail\n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `pos`
--

CREATE TABLE `pos` (
  `id` bigint(20) NOT NULL,
  `retailer_id` bigint(20) UNSIGNED NOT NULL,
  `pos_status_id` int(5) NOT NULL DEFAULT '1' COMMENT 'status of the point of sale\n',
  `bo_user_id` int(10) NOT NULL,
  `name_rl` varchar(255) DEFAULT NULL COMMENT 'name for regional language',
  `name_en` varchar(255) DEFAULT NULL COMMENT 'name for english',
  `address_rl` varchar(255) DEFAULT NULL COMMENT 'name for english',
  `address_en` varchar(255) DEFAULT NULL COMMENT 'address for english',
  `latitude` decimal(10,8) DEFAULT NULL COMMENT 'GPS Latitude ex:40.71727401',
  `longitude` decimal(11,8) DEFAULT NULL COMMENT 'GPS Longitude  ex: -74.00898606',
  `date_add` timestamp NULL DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `pos`
--

INSERT INTO `pos` (`id`, `retailer_id`, `pos_status_id`, `bo_user_id`, `name_rl`, `name_en`, `address_rl`, `address_en`, `latitude`, `longitude`, `date_add`, `date_update`) VALUES
(7, 100016, 1, 26, 'dssadvsdfsd', 'fsdfsdf', 'sdfdsf', 'fsdfsdfsd', '23.66000000', '77.99000000', '2016-11-07 18:14:36', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `pos_status`
--

CREATE TABLE `pos_status` (
  `id` int(5) NOT NULL,
  `name_en` varchar(45) NOT NULL,
  `name_ru` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `pos_status`
--

INSERT INTO `pos_status` (`id`, `name_en`, `name_ru`) VALUES
(1, 'Active', 'Активна'),
(2, 'Blocked', 'Заблокирована');

-- --------------------------------------------------------

--
-- Структура таблиці `registrator_registered`
--

CREATE TABLE `registrator_registered` (
  `registrator_id` int(11) NOT NULL,
  `registered_id` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `retailer`
--

CREATE TABLE `retailer` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'unique identifier begins with 100000',
  `retailer_status_id` int(5) NOT NULL COMMENT 'identifier for the retail status',
  `retailer_barcode_id` int(5) NOT NULL COMMENT 'an identifier for the type of bar code',
  `retailer_regional_id` int(5) NOT NULL COMMENT 'identifier for the region',
  `retailer_contract_id` int(10) NOT NULL COMMENT 'identifier for the contracts',
  `retailer_work_schemes_id` int(5) NOT NULL COMMENT 'identifier for the schema on which operates retail\n',
  `bo_user_id` int(10) NOT NULL COMMENT 'the user ID that created the retail',
  `oyko_id` bigint(20) NOT NULL COMMENT 'random 6-digit unique identifier',
  `name_rl` varchar(150) NOT NULL COMMENT 'name for regional language',
  `name_en` varchar(150) NOT NULL COMMENT 'name for english  language',
  `company_description_rl` varchar(255) NOT NULL COMMENT 'description for regional language',
  `company_description_en` varchar(255) NOT NULL COMMENT 'description for english  language',
  `phone_first` varchar(50) NOT NULL,
  `phone_second` varchar(50) DEFAULT NULL,
  `free_phone` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `color` varchar(10) DEFAULT NULL COMMENT 'Format: a #123ABC',
  `keyword` varchar(1000) NOT NULL COMMENT 'keywords for search indexing, separator - comma',
  `big_img` varchar(255) DEFAULT NULL COMMENT 'the database should be stored only in the MD5 hash, the picture should be moved to a laravel \\ public \\ retail \\ image \\ big \\ and   and renamed MD5 renamed',
  `small_img` varchar(255) DEFAULT NULL COMMENT 'the database should be stored only in the MD5 hash, the picture should be moved to a laravel \\ public \\ retail \\ image \\ small  \\ and   and renamed MD5 renamed',
  `pos_img` varchar(255) DEFAULT NULL COMMENT 'the database should be stored only in the MD5 hash, the picture should be moved to a laravel \\ public \\ retail \\ image \\ pos  \\ and   and renamed MD5 renamed',
  `logo_img` varchar(255) DEFAULT NULL COMMENT 'the database should be stored only in the MD5 hash, the picture should be moved to a laravel \\ public \\ retail \\ image \\ logo  \\ and   and renamed MD5 renamed',
  `program_description_url_rl` varchar(45) DEFAULT NULL COMMENT 'a reference to the program description in the language of region',
  `program_description_url_en` varchar(45) DEFAULT NULL COMMENT 'a reference to the program description in english',
  `date_add` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `retailer`
--

INSERT INTO `retailer` (`id`, `retailer_status_id`, `retailer_barcode_id`, `retailer_regional_id`, `retailer_contract_id`, `retailer_work_schemes_id`, `bo_user_id`, `oyko_id`, `name_rl`, `name_en`, `company_description_rl`, `company_description_en`, `phone_first`, `phone_second`, `free_phone`, `email`, `url`, `color`, `keyword`, `big_img`, `small_img`, `pos_img`, `logo_img`, `program_description_url_rl`, `program_description_url_en`, `date_add`, `date_update`) VALUES
(100015, 2, 6, 1, 1, 1, 1, 100000, '1', '2', '4', '5', '3', '', '', '', '', '#FFFFFF', '', NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-07 20:10:53', '2016-11-07 20:11:44'),
(100016, 2, 6, 1, 1, 1, 26, 100001, 'moder', 'moder', 'dasdsa', 'dsfdf', 'phone', '', '', '', '', '#FFFFFF', 'sdf,fsdgdf', NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-07 20:13:53', '2016-11-07 20:14:36');

-- --------------------------------------------------------

--
-- Структура таблиці `retailer_analytics`
--

CREATE TABLE `retailer_analytics` (
  `id` int(11) NOT NULL,
  `retailer_id` bigint(20) UNSIGNED NOT NULL,
  `total_cards_added` int(11) DEFAULT '0' COMMENT 'total added cards',
  `total_pos_added` int(11) DEFAULT '0' COMMENT 'total pos added ',
  `percentage_cards_favorites` int(11) DEFAULT '0' COMMENT 'percentage of cards in favorites'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `retailer_barcode`
--

CREATE TABLE `retailer_barcode` (
  `id` int(5) NOT NULL,
  `bar_code_standard_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `retailer_barcode`
--

INSERT INTO `retailer_barcode` (`id`, `bar_code_standard_name`) VALUES
(6, 'com.intermec.Code93'),
(11, 'org.ansi.Interleaved2of5'),
(4, 'org.gs1.EAN-13'),
(5, 'org.gs1.EAN-8'),
(12, 'org.gs1.ITF14'),
(1, 'org.gs1.UPC-E'),
(10, 'org.iso.Aztec'),
(7, 'org.iso.Code128'),
(2, 'org.iso.Code39'),
(3, 'org.iso.Code39Mod43'),
(13, 'org.iso.DataMatrix'),
(8, 'org.iso.PDF417'),
(9, 'org.iso.QRCode'),
(14, 'unknown');

-- --------------------------------------------------------

--
-- Структура таблиці `retailer_category`
--

CREATE TABLE `retailer_category` (
  `id` int(5) NOT NULL,
  `name_en` varchar(100) NOT NULL,
  `name_ru` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `retailer_category`
--

INSERT INTO `retailer_category` (`id`, `name_en`, `name_ru`) VALUES
(1, 'Cars', 'Авто'),
(2, 'Home & Garden', 'Дом и сад'),
(3, 'Electronics', 'Электроника'),
(4, 'Clothing, Shoes & Jewerly', 'Одежда, обувь и украшения'),
(5, 'Sports & Fitness', 'Спорт и фитнес'),
(6, 'Health & Beauty', 'Красота и здоровье'),
(7, 'Grocery', 'Бакалея'),
(8, 'Restaurants', 'Рестораны'),
(9, 'Kids', 'Дети'),
(10, 'Entertainment', 'Развлечения'),
(11, 'Travelling & Transportation', 'Путешествия и транспорт'),
(12, 'Malls', 'ТРЦ');

-- --------------------------------------------------------

--
-- Структура таблиці `retailer_category_selected`
--

CREATE TABLE `retailer_category_selected` (
  `retailer_category_id` int(5) NOT NULL,
  `retailer_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `retailer_category_selected`
--

INSERT INTO `retailer_category_selected` (`retailer_category_id`, `retailer_id`) VALUES
(1, 100016),
(9, 100015);

-- --------------------------------------------------------

--
-- Структура таблиці `retailer_contract`
--

CREATE TABLE `retailer_contract` (
  `id` int(10) NOT NULL,
  `number` varchar(200) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `retailer_contract`
--

INSERT INTO `retailer_contract` (`id`, `number`, `url`) VALUES
(1, '', ''),
(11, 'sw62632df', 'contract-scan-from-retailer(3).pdf'),
(12, '1231234242', 'contract-scan-from-retailer(7).pdf'),
(13, '1000001', 'contract-scan-from-retailer(6).pdf'),
(14, '52353452345', NULL),
(15, '23453245', NULL),
(16, '2352345234', NULL),
(17, '235423452', NULL),
(18, '2345235', 'contract-scan-from-retailer(12).pdf'),
(19, '34653465345', NULL),
(20, '34653465345', NULL),
(21, '34653465345', NULL),
(22, '34653465345', 'contract-scan-from-retailer(13).pdf');

-- --------------------------------------------------------

--
-- Структура таблиці `retailer_key_words`
--

CREATE TABLE `retailer_key_words` (
  `retailer_id` int(11) NOT NULL,
  `key_word_1` varchar(45) DEFAULT NULL,
  `key_word_2` varchar(45) DEFAULT NULL,
  `key_word_3` varchar(45) DEFAULT NULL,
  `key_word_4` varchar(45) DEFAULT NULL,
  `key_word_5` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `retailer_logs`
--

CREATE TABLE `retailer_logs` (
  `id` bigint(20) NOT NULL,
  `retailer_id` bigint(20) UNSIGNED NOT NULL,
  `log_actions_id` int(5) NOT NULL,
  `bo_user_id` int(11) NOT NULL,
  `reason` varchar(255) DEFAULT 'Not specified',
  `change_unit` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `retailer_logs`
--

INSERT INTO `retailer_logs` (`id`, `retailer_id`, `log_actions_id`, `bo_user_id`, `reason`, `change_unit`, `created_at`, `updated_at`) VALUES
(184, 100015, 10006, 1, 'n/a', NULL, '2016-11-07 20:10:53', '2016-11-07 18:10:53'),
(185, 100015, 10009, 1, 'n/a', NULL, '2016-11-07 20:11:26', '2016-11-07 18:11:26'),
(186, 100015, 10012, 1, 'n/a', NULL, '2016-11-07 20:11:44', '2016-11-07 18:11:44'),
(187, 100016, 10006, 26, 'n/a', NULL, '2016-11-07 20:13:53', '2016-11-07 18:13:53'),
(188, 100016, 10009, 26, 'n/a', NULL, '2016-11-07 20:14:36', '2016-11-07 18:14:36');

-- --------------------------------------------------------

--
-- Структура таблиці `retailer_regional`
--

CREATE TABLE `retailer_regional` (
  `id` int(5) NOT NULL,
  `name_en` varchar(100) NOT NULL,
  `name_ru` varchar(100) NOT NULL,
  `iso_code` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `retailer_regional`
--

INSERT INTO `retailer_regional` (`id`, `name_en`, `name_ru`, `iso_code`) VALUES
(1, 'Austria', 'Австрия', 'AUT'),
(2, 'France', 'Франция', 'FRA'),
(3, 'Germany', 'Германия', 'DEU'),
(4, 'Switzerland', 'Швейцария', 'CHE'),
(5, 'Ukraine', 'Украина', 'UKR'),
(6, 'Hotels / Airlines', 'Отели / Авиалинии', 'HA');

-- --------------------------------------------------------

--
-- Структура таблиці `retailer_status`
--

CREATE TABLE `retailer_status` (
  `id` int(5) NOT NULL,
  `name_en` varchar(50) NOT NULL,
  `name_ru` varchar(50) NOT NULL,
  `do_name_en` varchar(20) NOT NULL,
  `do_name_ru` varchar(20) NOT NULL,
  `add_name_ru` varchar(100) NOT NULL,
  `add_name_en` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `retailer_status`
--

INSERT INTO `retailer_status` (`id`, `name_en`, `name_ru`, `do_name_en`, `do_name_ru`, `add_name_ru`, `add_name_en`) VALUES
(1, 'Active', 'Активен', 'Publisch', 'Опубликовать', 'Разблокировать', 'Unblock'),
(2, 'Moderation', 'Модерация', 'Moderation', 'Модерация', '', ''),
(3, 'Blocked', 'Заблокирован', 'Block', 'Заблокировать', '', '');

-- --------------------------------------------------------

--
-- Структура таблиці `retailer_work_schemes`
--

CREATE TABLE `retailer_work_schemes` (
  `id` int(5) NOT NULL,
  `name_en` varchar(45) NOT NULL,
  `name_ru` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `retailer_work_schemes`
--

INSERT INTO `retailer_work_schemes` (`id`, `name_en`, `name_ru`) VALUES
(1, 'Cardholder', 'Кардхолдер'),
(2, 'SME', 'МСБ'),
(3, 'Enterprise', 'Энтерпрайз');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `bo_user`
--
ALTER TABLE `bo_user`
  ADD PRIMARY KEY (`id`,`bo_user_status_id`,`bo_user_role_id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `fk_bo_user_bo_user_role1_idx` (`bo_user_role_id`),
  ADD KEY `fk_bo_user_bo_user_status1_idx` (`bo_user_status_id`);

--
-- Індекси таблиці `bo_user_password_resets`
--
ALTER TABLE `bo_user_password_resets`
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Індекси таблиці `bo_user_permissions`
--
ALTER TABLE `bo_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Індекси таблиці `bo_user_permissions_role`
--
ALTER TABLE `bo_user_permissions_role`
  ADD PRIMARY KEY (`bo_user_permissions_id`,`bo_user_role_id`),
  ADD KEY `fk_bo_user_permissions_has_bo_user_role_bo_user_role2_idx` (`bo_user_role_id`),
  ADD KEY `fk_bo_user_permissions_has_bo_user_role_bo_user_permissions_idx` (`bo_user_permissions_id`);

--
-- Індекси таблиці `bo_user_role`
--
ALTER TABLE `bo_user_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_name_en_UNIQUE` (`role_name_en`),
  ADD UNIQUE KEY `role_name_ru_UNIQUE` (`role_name_ru`);

--
-- Індекси таблиці `bo_user_status`
--
ALTER TABLE `bo_user_status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `status_unique` (`status_en`,`status_ru`);

--
-- Індекси таблиці `log_actions`
--
ALTER TABLE `log_actions`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `pos`
--
ALTER TABLE `pos`
  ADD PRIMARY KEY (`id`,`retailer_id`,`pos_status_id`,`bo_user_id`),
  ADD KEY `fk_retailer_pos_pos_status1_idx` (`pos_status_id`),
  ADD KEY `fk_pos_retailer1_idx` (`retailer_id`),
  ADD KEY `fk_pos_bo_user1_idx` (`bo_user_id`);

--
-- Індекси таблиці `pos_status`
--
ALTER TABLE `pos_status`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `registrator_registered`
--
ALTER TABLE `registrator_registered`
  ADD PRIMARY KEY (`registrator_id`);

--
-- Індекси таблиці `retailer`
--
ALTER TABLE `retailer`
  ADD PRIMARY KEY (`id`,`retailer_status_id`,`retailer_barcode_id`,`retailer_regional_id`,`retailer_contract_id`,`retailer_work_schemes_id`,`bo_user_id`,`oyko_id`),
  ADD KEY `fk_retailer_bo_user1_idx` (`bo_user_id`),
  ADD KEY `fk_retailer_retailer_status1_idx` (`retailer_status_id`),
  ADD KEY `fk_retailer_retailer_barcode1_idx` (`retailer_barcode_id`),
  ADD KEY `fk_retailer_retailer_contract1_idx` (`retailer_contract_id`),
  ADD KEY `fk_retailer_retailer_regional1_idx` (`retailer_regional_id`),
  ADD KEY `fk_retailer_retailer_work_schemes1_idx` (`retailer_work_schemes_id`);

--
-- Індекси таблиці `retailer_analytics`
--
ALTER TABLE `retailer_analytics`
  ADD PRIMARY KEY (`id`,`retailer_id`),
  ADD KEY `fk_retailer_analytics_retailer1_idx` (`retailer_id`);

--
-- Індекси таблиці `retailer_barcode`
--
ALTER TABLE `retailer_barcode`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bar_code_standard_name_UNIQUE` (`bar_code_standard_name`);

--
-- Індекси таблиці `retailer_category`
--
ALTER TABLE `retailer_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_en_UNIQUE` (`name_en`),
  ADD UNIQUE KEY `name_ru_UNIQUE` (`name_ru`);

--
-- Індекси таблиці `retailer_category_selected`
--
ALTER TABLE `retailer_category_selected`
  ADD PRIMARY KEY (`retailer_category_id`,`retailer_id`),
  ADD KEY `fk_retailer_category_has_retailer_retailer1_idx` (`retailer_id`),
  ADD KEY `fk_retailer_category_has_retailer_retailer_category1_idx` (`retailer_category_id`);

--
-- Індекси таблиці `retailer_contract`
--
ALTER TABLE `retailer_contract`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `retailer_key_words`
--
ALTER TABLE `retailer_key_words`
  ADD PRIMARY KEY (`retailer_id`);

--
-- Індекси таблиці `retailer_logs`
--
ALTER TABLE `retailer_logs`
  ADD PRIMARY KEY (`id`,`retailer_id`,`log_actions_id`,`bo_user_id`),
  ADD KEY `fk_retailer_logs_log_actions1_idx` (`log_actions_id`),
  ADD KEY `fk_logs_bo_user1_idx` (`bo_user_id`),
  ADD KEY `fk_logs_retailer1_idx` (`retailer_id`);

--
-- Індекси таблиці `retailer_regional`
--
ALTER TABLE `retailer_regional`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `retailer_status`
--
ALTER TABLE `retailer_status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_en_UNIQUE` (`name_en`),
  ADD UNIQUE KEY `name_ru_UNIQUE` (`name_ru`);

--
-- Індекси таблиці `retailer_work_schemes`
--
ALTER TABLE `retailer_work_schemes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_ru_UNIQUE` (`name_ru`),
  ADD UNIQUE KEY `name_en_UNIQUE` (`name_en`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `bo_user`
--
ALTER TABLE `bo_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT для таблиці `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблиці `pos`
--
ALTER TABLE `pos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблиці `retailer`
--
ALTER TABLE `retailer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'unique identifier begins with 100000', AUTO_INCREMENT=100017;
--
-- AUTO_INCREMENT для таблиці `retailer_contract`
--
ALTER TABLE `retailer_contract`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблиці `retailer_logs`
--
ALTER TABLE `retailer_logs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;
--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `bo_user`
--
ALTER TABLE `bo_user`
  ADD CONSTRAINT `fk_bo_user_bo_user_role1` FOREIGN KEY (`bo_user_role_id`) REFERENCES `bo_user_role` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_bo_user_bo_user_status1` FOREIGN KEY (`bo_user_status_id`) REFERENCES `bo_user_status` (`id`) ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `bo_user_permissions_role`
--
ALTER TABLE `bo_user_permissions_role`
  ADD CONSTRAINT `fk_bo_user_permissions_has_bo_user_role_bo_user_permissions2` FOREIGN KEY (`bo_user_permissions_id`) REFERENCES `bo_user_permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_bo_user_permissions_has_bo_user_role_bo_user_role2` FOREIGN KEY (`bo_user_role_id`) REFERENCES `bo_user_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `pos`
--
ALTER TABLE `pos`
  ADD CONSTRAINT `fk_retailer_pos_pos_status1` FOREIGN KEY (`pos_status_id`) REFERENCES `pos_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pos_ibfk_1` FOREIGN KEY (`bo_user_id`) REFERENCES `bo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pos_retailer_id_foreign` FOREIGN KEY (`retailer_id`) REFERENCES `retailer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `retailer`
--
ALTER TABLE `retailer`
  ADD CONSTRAINT `fk_retailer_retailer_barcode1` FOREIGN KEY (`retailer_barcode_id`) REFERENCES `retailer_barcode` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_retailer_retailer_regional1` FOREIGN KEY (`retailer_regional_id`) REFERENCES `retailer_regional` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_retailer_retailer_status1` FOREIGN KEY (`retailer_status_id`) REFERENCES `retailer_status` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_retailer_retailer_work_schemes1` FOREIGN KEY (`retailer_work_schemes_id`) REFERENCES `retailer_work_schemes` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `retailer_ibfk_1` FOREIGN KEY (`bo_user_id`) REFERENCES `bo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `retailer_ibfk_2` FOREIGN KEY (`retailer_contract_id`) REFERENCES `retailer_contract` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `retailer_analytics`
--
ALTER TABLE `retailer_analytics`
  ADD CONSTRAINT `retailer_analytics_retailer_id_foreign` FOREIGN KEY (`retailer_id`) REFERENCES `retailer` (`id`) ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `retailer_category_selected`
--
ALTER TABLE `retailer_category_selected`
  ADD CONSTRAINT `fk_retailer_category_has_retailer_retailer_category1` FOREIGN KEY (`retailer_category_id`) REFERENCES `retailer_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `retailer_category_selected_retailer_id_foreign` FOREIGN KEY (`retailer_id`) REFERENCES `retailer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `retailer_logs`
--
ALTER TABLE `retailer_logs`
  ADD CONSTRAINT `fk_retailer_logs_log_actions1` FOREIGN KEY (`log_actions_id`) REFERENCES `log_actions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `retailer_logs_ibfk_1` FOREIGN KEY (`bo_user_id`) REFERENCES `bo_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `retailer_logs_retailer_id_foreign` FOREIGN KEY (`retailer_id`) REFERENCES `retailer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
