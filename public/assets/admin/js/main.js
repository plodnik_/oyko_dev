/**
 *  Функция-часьІ
 * @param $elem jQuery елемент для обновления
 * @param start
 */
var upTime = function ($elem, start) {

    var date = new Date();

    var h = date.getHours(),
        m = date.getMinutes(),
        s = date.getSeconds();

    if (h < 10) h = '0'+h;
    if (m < 10) m = '0'+m;
    if (s < 10) s = '0'+s;

    $elem.text(h+':'+m+':'+s);

    if (typeof  start !== 'undefined') {

        var timer = window.setInterval(function () {

            upTime($elem);

        }, 1000);

    }
};

Date.prototype.ddmmyyyy = function() {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = this.getDate().toString();
    return (dd[1]?dd:"0"+dd[0]) + '.' + (mm[1]?mm:"0"+mm[0])  + '.' + yyyy ; // padding
};

/**
 *  Запрос на валидацию данньІх пользователя
 *
 * @param fields
 * @param url
 * @param modal
 * @param callback
 */
function validateFields(fields, url, modal,  callback)
{

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url, // ULR обрабатьІвающий данньІе
        method: 'post',
        dataType: 'json',
        data: fields, // валидируемьІе данньІе
        success: function (response) {

            $(modal).find('.form-group').removeClass('has-error');
            $(modal).find('.help-block').hide(200);

            // Если проверка прошла успешно
            if (response.check) {
                modal.find('.success-status').show(300); // показьІваем галочку успешной проверки

                var button = modal.find('.submit')[0];

                // Добавляем отслеживание на кнопку отправки формьІ
                button.onclick = function (e) {

                    // fix
                    button = $(button);

                    // Сохранение
                    writeUser(
                        modal.find('form').serialize(), // ДанньІе формьІ
                        button.attr('data-type'),       // Тип запроса сохранения (создание/обновление пользователя)

                        function(){                     // Callback для обработки ошьІбок валидации

                            // Убираем отслеживание кнопки отправки формьІ
                            button.unbind('click');

                            modal.find('.success-status').hide(300); // скрьІваем галочку успешной проверки

                            // Добавление ошибок к полям
                            $.each(response.message, function (key, text) {
                                modal.find('.'+key).addClass('has-error');
                                modal.find('.'+key+' .help-block').text(text).show(200);
                            });

                            // ОсобьІе callback
                            if (typeof callback === 'function') callback();
                        },

                        function (response) {               // callback после сохранения

                            button.unbind('click'); // Убираем отслеживание кнопки отправки формьІ

                            modal.modal('hide');

                            modal.find('.success-status').hide(); // скрьІваем галочку успешной проверки

                            // Если пришла команда редиректа
                            if (response.redirect) window.location.replace(response.redirect);

                            // Если есть есть уведомления
                            if (response.message) specialAlert(response.message);

                            // Обновление таблицьІ
                            reloadTable();

                            if (button.attr('data-type') == 'create') clearCreateFormFields();

                    })

                };

                return;
            }

            // ------- Поля не прошли проверку ------- //
            modal.find('.success-status').hide(); // скрьІваем галочку успешной проверки

            // Добавление ошибок к полям
            $.each(response.message, function (key, text) {

                modal.find('.'+key).addClass('has-error');
                modal.find('.'+key+' .help-block').text(text).show(200);

            });

        },
        error: function (e) {
            specialAlert(error500);
        }
    });

}

/**
 *  Установить статус для ритейла
 * @param status
 * @param data
 * @param callback
 */
function setStatus(status, data, callback) {

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/'+locale+'/admin/retailer/'+window.retailer_id+'/status/'+status,
        data: data,
        method: 'post',
        success: function (response) {

            if (typeof callback == 'function') callback(response);

            if (typeof response.message !== 'undefined') specialAlert(response.message);

        },
        error: function (e) {
            specialAlert(error500);
        }
    })

}

/**
 *  Запрос валидиции ритейла
 *  
 * @param fields
 * @param url
 * @param modal
 * @param successCallback
 * @param errorCallback
 */
function validateRetailer(fields, url, modal,  successCallback, errorCallback)
{

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url, // ULR обрабатьІвающий данньІе
        method: 'post',
        dataType: 'json',
        data: fields, // валидируемьІе данньІе
        success: function (response) {

            $(modal).find('.form-group').removeClass('has-error');
            $(modal).find('.help-block').hide(200);

            // Если проверка прошла успешно
            if (response.check) {

                if (typeof successCallback == 'function') successCallback(modal);

            } else {

                // ------- Поля не прошли проверку ------- //
                modal.find('.success-status').hide(); // скрьІваем галочку успешной проверки

                // Добавление ошибок к полям
                $.each(response.message, function (key, text) {

                    modal.find('.'+key).addClass('has-error');
                    modal.find('.'+key+' .help-block').text(text).show(200);

                });

                if (typeof errorCallback === 'function') errorCallback(modal);

            }

        },
        error: function (e) {
            specialAlert(error500);
        }
    });

}

/**
 *  Запрос на валидацию данньІх пользователя
 *
 * @param fields
 * @param modal
 * @param callback
 */
function validatePos(fields, modal,  callback, errorCallback)
{

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/'+locale+'/admin/pos/valid', // ULR обрабатьІвающий данньІе
        method: 'post',
        dataType: 'json',
        data: fields, // валидируемьІе данньІе
        success: function (response) {

            $(modal).find('.form-group').removeClass('has-error');
            $(modal).find('.help-block').hide(200);

            // Если проверка прошла успешно
            if (response.check) {
                
                if (typeof callback == 'function') callback(response);
                
                return;
            }
            
            // Добавление ошибок к полям
            $.each(response.message, function (key, text) {

                modal.find('.'+key).addClass('has-error');
                modal.find('.'+key+' .help-block').text(text).show(200);

            });

            if (typeof errorCallback== 'function') errorCallback(response);

        },
        error: function (e) {
            specialAlert(error500);
        }
    });

}

/**
 *  Функция обрабатьІвающая сохранение данньІх пользователя
 *
 * @param fields   поля обновления
 * @param type     тип сохранения (создание/обновление пользователя)
 * @param onError  callback на случай ошибки обработки запроса
 * @param callback callback если сохнанение прошло успешно
 */
function writeUser(fields, type, onError, callback) {

    // Опредиление ULR
    var create = '/'+locale+'/admin/user/add',
        update = '/'+locale+'/admin/user/edit';

    var url = '';

    if (type == 'create') url = create;
    if (type == 'update') url = update;

    // Если URL не предусмотрен показать ошибку и завершить работу
    if (!url) {
        specialAlert('Some error!');
        return;
    }

    $.ajax({

        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        method: 'post',
        dataType: 'json',
        data: fields,
        success: function (response) {

            if (!response.ckeck){

                if (response.trouble == 'valid') {

                    if (typeof onError === 'function') onError();
                    return;

                }

            }

            if (typeof callback === 'function') callback(response);

        },
        error: function (e) {
            specialAlert(error500);
        }
    })
}

/**
 *  Изменить тип кнопки на submit  для отправки формьІ
 * @param modal
 */
function enableSubmitModalButton(modal) {

    modal.find('button.submit').attr('type', 'submit');

}
/**
 *  Изменить тип кнопки на button для невозможности отправки формьІ
 * @param modal
 */
function disableSubmitModalButton(modal) {

    modal.find('button.submit').attr('type', 'button');

}

/**
 *  Очистка формьІ создания пользователя
 */
function clearCreateFormFields()
{
    $("#new_role").val('');
    $("#new_email").val('');
    $("#new_password").val('');
}

/**
 *  Обновление таблицьІ
 */
function reloadTable() {
    $('.dataTable').each(function() {
        dt = $(this).dataTable();
        dt.fnDraw();
    })
}

/**
 *  Показать уведомление
 *
 * @param message текст сообщения
 */
function specialAlert(message) {

    var alert = $('#specialAlert');

    if (longestWord(message) > 20) {
        $('#specialAlert .modal-dialog').css('width', '600px');
    } else {
        $('#specialAlert .modal-dialog').css('width', '400px');
    }

    alert.find('.message').html(message);
    alert.modal('show');


}

/**
 *  Кастомное решение для биндинга нескольких собьІтий для одного селектора
 *
 * @param selector string       Селектор (не єкземпляр jQuery!)
 * @param event [array, string] Массив или строка названия собьІтия(ий)
 * @param handler function      Функция (общая для всех собьІтий)
 * @param tHandler object       Обьект функций (назначение функции для обдельного собьІтия)
 */
function eventHandler(selector, event, handler, tHandler)
{

    if (typeof event === 'string') {

        $(selector).on(event, handler);
    }

    if (typeof event === 'object') {

        $.each(event, function (i, trigger) {

            $(selector).on(trigger, function (e) {

                handler(e);

                if (typeof tHandler !== 'undefined') {
                    if (typeof tHandler[trigger] === 'function') tHandler[trigger](e);
                }
            });

        })

    }
}

/**
 *  Установка слежения с задерждой
 */
var delay = (function(){
    var timer = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();

/**
 *  Поиск самого длинного слова
 * @param string
 * @returns {*}
 */
function longestWord(string) {
    var str = string.split(" ");
    var longest = 0;
    var word = null;
    for (var i = 0; i < str.length; i++) {
        if (longest < str[i].length) {
            longest = str[i].length;
            word = str[i];
        }
    }
    return word.length;
}

/**
 *  Проверка ритейла
 *
 * @param retailer_id
 */
function checkPublish()
{

    // ПервьІй шаг - проверка обьязательньІх полей
    $.ajax({
        url: '/' + locale + '/admin/retailer/' + window.retailer_id + '/check/1',
        method: 'get',
        dataType: 'json',
        success: function (response) {

            if (!response.check) {

                $('#unfinished .errors').html(response.messages);
                $('#unfinished').modal('show');

                return;
            } else {

                if ($('div').is('#notice')){
                    $('#notice').modal('show');
                } else {
                    $('#unblock').modal('show');
                }

                return;
            }
        },
        error: function (e) {
            specialAlert(error500);
        }
    })
}

function redrawPosTableCallback() {


    if ($('input').is('.icheck-pos')) {

        $('.icheck-pos').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '15%' // optional,
        });

        // Кнопки действий
        var buttons = '.actions-btn';

        // ВьІделен только один чекбокс
        $('.check-one').on('ifChanged', function (e) {

            var checkbox = $(e.target);

            // Если чекбокс бьІл отмечен - надо проверить отмечать ли
            // "главньІй" чекбокс для всех
            if (checkbox.is(':checked')) {

                var check = true;

                $('.check-one').each(function (i, ch) {

                    // Если хотя бьІ один чекбокс из таблици не отмечен -
                    // "главньІй" тоже должен бьІть вьІключен
                    if (!$(ch).is(':checked')) {

                        check = false;
                        return false;
                    }
                });

                // Меняем состояние "главного" чекбокса
                $('#pos-all').prop('checked', check);

                $('#pos-all').iCheck('update');


            } else {

                // Если хотя бьІ один чекбокс из таблици не отмечен -
                // "главньІй" тоже должен бьІть вьІключен
                $('#pos-all').prop('checked', false);

                $('#pos-all').iCheck('update');
            }

            updateActionButtons();

        });

        // ВьІделить все
        $('#pos-all').on('ifChanged', function (e) {

            var checkbox = $(e.target);

            if (checkbox.is(':checked')) {

                $('.check-one').iCheck('check');

            } else {

                $('.check-one').iCheck('uncheck');
            }

            updateActionButtons();
        });


        /**
         *  Обновление состояния кнопок управления POS-записями
         */
        function updateActionButtons()
        {
            var enabled = false;

            $('.check-one').each(function (i, ch) {

                // Если хотя бьІ один чекбокс из таблици не отмечен -
                // "главньІй" тоже должен бьІть вьІключен
                if ($(ch).is(':checked')) {

                    enabled = true;
                    return false;
                }
            });

            if (enabled) {
                $(buttons).removeClass('disable');
                $(buttons).addClass('enabled-grey');

            } else {

                $(buttons).removeClass('enabled-grey');
                $(buttons).addClass('disable');
            }
        }
    }
}

function updatePosTable() {

    var select = $('#pos-status'),
        input = $('#pos-added'),
        val = select.val(),
        params = '';

    if (typeof val === 'object' && val !==null && val.length > 0) {

        if (val.length == 1) {

            if (val[0] !== '') {
                params = !params ? params + '?' : params + '&';

                params = params + select.attr('name') + '=' + val[0];
            }
        } else {

            params = !params ? params+'?' : params+'&';

            params = params + select.attr('name') + '=' + val.join(',');
        }
    }

    if (input.val()) {

        var from, to, iVal = input.val().replace(/ /g,'');

        if (iVal.indexOf('-')>=0) {

            var dates = iVal.split('-');

            from = dates[0];
            to = dates[1];

            params = !params ? params+'?' : params+'&';

            params = params + 'from='+from+'&to='+to;
            params = params + 'from='+from+'&to='+to;

        }

    }

    // URL с установленньІми фильтрами
    var url = pos_table_url + params;

    oTablePos.api().ajax.url(url);
    oTablePos.api().ajax.reload();

}

$(function () {

    // Запукс часов
    upTime($('.clock-block__time-text'), true);

    var create_valid =  '/'+locale+'/admin/user/valid'; // URL проверки при создании пользователя


    // ---------- Отслеживание собьІтий ---------- //
    eventHandler('#new_role', 'change', function (e) {

        var fg = $(e.target).closest('.form-group'),
            _class = $(e.target).attr('data-class');

        if (!fg.hasClass(_class)) fg.addClass(_class);

        var fields = $(e.target).parents('form').serialize();

        validateFields(fields, create_valid, $(e.target).parents('.modal'));

    });

    eventHandler('#new_email,#new_password', ['change', 'keyup'],
        function (e) {

            var fg = $(e.target).closest('.form-group'),
                _class = $(e.target).attr('data-class');

            if (!fg.hasClass(_class)) fg.addClass(_class);
        },
        {
            change: function (e) {
                var fields = $(e.target).parents('form').serialize();
                validateFields(fields, create_valid, $(e.target).parents('.modal'));
            },
            keyup: function (e) {
                delay(function () {
                    var fields = $(e.target).parents('form').serialize();
                    validateFields(fields, create_valid, $(e.target).parents('.modal'));
                }, 300)
            }
        }
    );

    $('body').on('click', '.edit-user', function (e) {

        var user = $(e.target).hasClass('edit-user') ? $(e.target) : $(e.target).closest('.edit-user'),
            modal= $('#updateUser');

        $('#edit_email').text(user.attr('data-email'));
        $('#edit_role').find('option[value='+user.attr('data-role')+']').prop('selected', true);
        $('#edit_status').find('option[value='+user.attr('data-status')+']').prop('selected', true);
        $('#edit_id').val(user.attr('data-id'));

        modal.find('.modal-title span').text(user.attr('data-email'));

    });

    eventHandler('#edit_role,#edit_status', 'change', function (e) {

        var fg = $(e.target).closest('.form-group'),
            _class = $(e.target).attr('name');

        if (!fg.hasClass(_class)) fg.addClass(_class);

        var fields = $(e.target).parents('form').serialize();

        validateFields(fields, create_valid+'/'+$('#edit_id').val(), $(e.target).parents('.modal'));
    });

    eventHandler('#edit_password', ['change', 'keyup'],
        function (e) {
            var fg = $(e.target).closest('.form-group'),
                _class = $(e.target).attr('name');

            if (!fg.hasClass(_class)) fg.addClass(_class);
        },
        {
            change: function (e) {
                var fields = $(e.target).parents('form').serialize();
                validateFields(fields, create_valid+'/'+$('#edit_id').val(), $(e.target).parents('.modal'));
            },
            keyup: function (e) {
                delay(function () {
                    var fields = $(e.target).parents('form').serialize();
                    validateFields(fields, create_valid+'/'+$('#edit_id').val(), $(e.target).parents('.modal'));
                }, 300)
            }
        }
    );

    eventHandler('#my_password,#my_name,#my_surname', ['change', 'keyup'],
        function (e) {
            var fg = $(e.target).closest('.form-group'),
                _class = $(e.target).attr('name');

            if (!fg.hasClass(_class)) fg.addClass(_class);
        },
        {
            change: function (e) {
                var fields = $(e.target).parents('form').serialize();
                validateFields(fields, create_valid+'/'+$('#my_id').val(), $(e.target).parents('.modal'));
            },
            keyup: function (e) {
                delay(function () {
                    var fields = $(e.target).parents('form').serialize();
                    validateFields(fields, create_valid+'/'+$('#my_id').val(), $(e.target).parents('.modal'));
                }, 300)
            }
        }
    );

    // Счетчик input-ов
    var inputs = '#new_name_rl,#new_name_en,#new_desc_rl,#new_desc_en,';

    inputs += '#new_pos_name_rl,#new_pos_name_en,#new_pos_address_rl,#new_pos_address_en,';

    inputs += '#edit_pos_name_rl,#edit_pos_name_en,#edit_pos_address_rl,#edit_pos_address_en,';

    inputs += '#reason-block,#reason-unblock';

    eventHandler(inputs, 'keyup', function (e) {

        var input   = $(e.target),
            length  = input.val().length,
            max     = parseInt(input.attr('maxlength')),
            left    = input.siblings('.max-length').find('.left');

        left.text(max-length);

    });

    // Управление категориями
    $('.categories input').on('click', function (e) {

        var checkboxes  = $('.categories [type=checkbox]'),
            checked     = $('.categories [type=checkbox]:checked').length;

        if (window.maxCategories === true) {

            window.maxCategories = false;
            checkboxes.css({visibility: 'visible'});
        }

        if (checked >= 3) {

            window.maxCategories = true;

            checkboxes.each(function (i, checkbox) {
                if ($(checkbox).prop('checked') == false ) $(checkbox).css({visibility: 'hidden'});
            })

        }

    });

    // Плагин для ключевьІх слов
    if ($('ul').is('#tag-it-keywords')) {
        $("#tag-it-keywords").tagit({
            singleField: true,
            fieldName: "keyword",
            allowSpaces: true
        });
    }

    var create_retailer_valid = '/'+locale+'/admin/retailer/valid';

    // Валидация ритейлов
    eventHandler('#new_name_rl,#new_name_en,#new_phone_1,#new_desc_rl,#new_desc_en,#new_corp_color',
        ['change', 'keyup'],
        function (e) {
            var fg = $(e.target).closest('.form-group'),
                _class = $(e.target).attr('name');

            if (!fg.hasClass(_class)) fg.addClass(_class);
        },
        {
            change: function (e) {

                var fields = $(e.target).parents('form').serialize();

                validateRetailer(
                    fields,
                    create_retailer_valid,
                    $(e.target).parents('.modal')
                );
            },
            keyup: function (e) {
                delay(function () {

                    var fields = $(e.target).parents('form').serialize();

                    validateRetailer(
                        fields,
                        create_retailer_valid,
                        $(e.target).parents('.modal')
                    );
                }, 600)
            }
        }
    );

    eventHandler('#new_role,#new_region,#new_barcode', 'change',
        function (e) {
            var fg = $(e.target).closest('.form-group'),
                _class = $(e.target).attr('name');

            if (!fg.hasClass(_class)) fg.addClass(_class);
            var fields = $(e.target).parents('form').serialize();

            validateRetailer(
                fields,
                create_retailer_valid,
                $(e.target).parents('.modal')
            );
        }
    );
    eventHandler('.categories [type=checkbox]', 'click',
        function (e) {
            var fg = $(e.target).parents('.form-group'),
                _class = 'category';

            if (!fg.hasClass(_class)) fg.addClass(_class);
            var fields = $(e.target).parents('form').serialize();

            validateRetailer(
                fields,
                create_retailer_valid,
                $(e.target).parents('.modal')
            );
        }
    );

    $('#create-retailer, #edit-retailer').on('submit', function (e) {

        // отключаем отправку формьІ
        e.preventDefault();

        $(e.target).find('.form-group').each(function (i, div) {

            var _addClass = $(div).find('input, textarea, select').attr('name');

            if (div.querySelector('#tag-it-keywords') !== null) {
                _addClass = 'keyword';
            }


            if (typeof _addClass !== 'undefined' && !$(div).hasClass(_addClass)) {

                if (_addClass == 'category[]') _addClass = 'category';

                $(div).addClass(_addClass);
            }
        });

        validateRetailer(
            $(e.target).serialize(),
            create_retailer_valid,
            $(e.target).parents('.modal'),
            function () {

                $(e.target).unbind('submit');

                $(e.target).submit();
            },
            function () {
                specialAlert($(e.target).attr('data-error'));
            }
        );


    });

    // Обновление статуса
    if ($('select').is('#select-status')) {

        $('#select-status').select2({
            minimumResultsForSearch: -1,
            escapeMarkup: function(m) { return m; },
            templateSelection: function (val) {

                var option  = $(val.element),
                    title   = option.attr('data-title');

                if (title == 'Moderation') {
                    $('.retailer-status .select2-container').css({width: '120px'});
                }
                if (title == 'Модерация') {
                    $('.retailer-status .select2-container').css({width: '115px'});
                }
                if (title == 'Blocked' || title == 'Publish') {
                    $('.retailer-status .select2-container').css({width: '93px'});
                }
                if (title == 'Заблокирован') {
                    $('.retailer-status .select2-container').css({width: '140px'});
                }
                if (title == 'Active') {
                    $('.retailer-status .select2-container').css({width: '80px'});
                }
                if (title == 'Активен') {
                    $('.retailer-status .select2-container').css({width: '95px'});
                }
                var underline = '<span class="select2-underline" style="border-color: '+$(val.element).attr('data-color')+'"></span>';

                return '<span style="color:'+$(val.element).attr('data-color')+'; position: relative">'+title+underline+'</span>';
            },
            templateResult: function (val) {


                var option = $(val.element),
                    show   = option.attr('data-show') ? option.attr('data-show') : ' ',
                    title  = val.text;

                var underline = '<span class="select2-underline"></span>';

                if ((show+'').indexOf(window.retailer_status) <0) return;

                if (window.isPublish && val.id == 1) title = option.attr('data-add');

                return '<span style="color:'+$(val.element).attr('data-color')+'">' + title + underline + '</span>' ;

            }

        });

        $('#select-status').on('select2:select', function (e) {

            var _set = e.target.value;

            if (_set == 1) checkPublish();

            if (_set == 3) $('#block').modal('show');

            if (_set == 'delete') $('#delete').modal('show');

            $('#select-status').val(window.retailer_status).trigger('change');

        });

        $('.categories input').each(function(i, checkbox){

            if ($(checkbox).attr('data-hidden') == 'true') $(checkbox).css({visibility: 'hidden'});

        });

        $('#select-status').on('select2:open', function () {


            var dropdown = $('.select2-container .select2-dropdown');

            // ширина dropdown списка
            var w = parseInt($('.retailer-status .select2-container').css('width'));

            // Установка позиционирования
            dropdown.css('left', ((130-w) * -1)+'px');

            $('.select2-dropdown').css({"min-width": '130px'});
        })

    }

    // ПсевдоссьІлка
    $('body').on('click', 'table.retailer-table tbody>[role=row]', function (e) {

        var tr = $(e.target).is('tr') ? $(e.target) : $(e.target).parents('tr');

        tr.addClass('active');

        window.setTimeout(function () {

            tr.removeClass('active');

            window.open(tr.find('.retailer-link').text());

        }, 100);


    });

    if ($('input').is('#filer_input')) {

        var filer = $("#filer_input").filer({
            appendTo: '#preview',
            limit: 1,
            maxSize: 10,
            changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><span class="field_title">'+field_title+'</span><a class="jFiler-input-choose-btn blue">'+button_title+'</a></div></div>',
            showThumbs: true,
            theme: "dragdropbox",
            extensions: ['png','jpg','jpeg','bmp','svg','gif'],
            templates: {
                box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                item: '<li class="jFiler-item">\
						<div class="jFiler-item-container">\
							<div class="jFiler-item-inner">\
								<div class="jFiler-item-thumb">\
									{{fi-image}}\
								</div>\
							</div>\
						</div>\
					</li>',
                itemAppend: '<li class="jFiler-item">\
							<div class="jFiler-item-container">\
								<div class="jFiler-item-inner">\
									<div class="jFiler-item-thumb">\
										<div class="jFiler-item-status"></div>\
										{{fi-image}}\
									</div>\
								</div>\
							</div>\
						</li>',
                progressBar: '<div class="bar"></div>',
                canvasImage: true,
                _selectors: {
                    list: '.jFiler-items-list',
                    item: '.jFiler-item',
                    progressBar: '.bar'
                }
            },
            allowDuplicates: true,
            clipBoardPaste: true,
            dialogs: {
                alert: function(text) {
                    return alert(text);
                },
                confirm: function (text, callback) {
                    confirm(text) ? callback() : null;
                }
            },
            captions: {
                button: "Choose Files",
                feedback: "Choose files To Upload",
                feedback2: "files were chosen",
                drop: "Drop file here to Upload",
                removeConfirmation: "Are you sure you want to remove this file?",
                errors: {
                    filesLimit: limit_error,
                    filesType: type_error,
                    filesSize: size_error
                }
            }
        });

        // Сохранение изображения ритейла
        $('#save-img').on('click', function(){


            // Если файл не прикрепили
            if (typeof $('#filer_input')[0].files[0] === 'undefined') {

                $('#imageModal').modal('hide');

                return;
            }

            // Формируем форму
            var data = new FormData();

            data.append('retailer_id', $('#retailer_id').val());
            data.append('img_name', $('#filer_input').attr('name'));
            data.append('file', $('#filer_input')[0].files[0]);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/"+locale+'/admin/retailer/img',
                data: data,
                type: 'POST',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function(response){

                    // соответствие поля div-у
                    var relation = {

                        big_img:    '.large',
                        small_img:  '.small',
                        pos_img:    '.pos',
                        logo_img:   '.logo'

                    };

                    // Close modal
                    $('#imageModal').modal('hide');

                    $('#filer_input').val('');

                    if (response.check) { // Если сохранение прошло успешно, обновляем компонентьІ страницьІ

                        var class_name = relation[$('#filer_input').attr('name')];

                        if (class_name == '.pos') {
                            $(".color-box").css({
                                background: "url("+response.file_path+") no-repeat",
                                "background-position": 'center center',
                                "background-size": 'contain'
                            });
                        }

                        var div = $('.images-box '+class_name+' .img-box');

                        div.css({
                            background: "url("+response.file_path+") no-repeat",
                            "background-position": 'center left',
                            "background-size": 'contain'
                        });

                        div.find('.edit-img')[0].dataset.img = response.file_path;
                        div.find('.edit-img').prop('focus', false);
                        div.find('.delete-img').show();

                    } else { // Сообщение об ошибке

                        specialAlert(response.message);

                    }
                },
                error: function (e) {
                    specialAlert(error500);
                }
            })

        });

        // ОткрьІваем модаль с изображением
        $('.edit-img').on('click', function (e) {

            var button  =   $(e.target).is('button') ? $(e.target) : $(e.target).parents('button'),
                modal   =   $('#imageModal');

            modal.find('.modal-title').text(button.attr('data-title'));

            $('#filer_input').attr('name', button.attr('data-field'));

            $('#preview img').attr('src', button.attr('data-img'))

        })
    }


    if ($('input').is('.icheck')) {

        // СтилизацьІя вьІбора загрузки программьІ лояльности
        $('.icheck').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '15%' // optional,
        });

        // Если изменен тип предостявляемьІх данньІх
        $('.icheck[name=type]').on('ifClicked', function (e) {

            var val = e.target.value,
                div_rl = $('.fields-lr'),
                div_en = $('.fields-en');

            if (val == 'link') {    // показьІваем инпутьІ для ссьІлок

                div_rl.find('.link-b').slideDown(300);
                div_rl.find('.pdf-b').slideUp(300);
                div_en.find('.link-b').slideDown(300);
                div_en.find('.pdf-b').slideUp(300);

            } else {                // показьІваем инпутьІ для файлов

                div_rl.find('.pdf-b').slideDown(300);
                div_rl.find('.link-b').slideUp(300);
                div_en.find('.pdf-b').slideDown(300);
                div_en.find('.link-b').slideUp(300);

            }
        });
    }

    if ($('input').is('.up-pdf')) {

        // Стилизация инпутов файлов программьІ лояльности
        $('.up-pdf').filer({
            limit: 1,
            extensions: ["pdf"],
            captions: {
                button: button_pdf_title,
                feedback: "",
                feedback2: "<b>"+file_selected+"</b>",
                errors: {
                    filesLimit: limit_error,
                    filesType: type_error,
                    filesSize: size_error
                }
            }
        });

        // Сохранение данньІх об лояльности
        $('#save-loyalty').on('click', function(){

            var form = new FormData();
            var type = $('[name=type]:checked').val();

            // Составляем форму
            form.append('type', type);
            form.append('retailer_id', $('#retailer_id').val());

            if (type == 'link') {

                form.append('program_description_url_rl', $('[name=program_description_url_rl]').val());
                form.append('program_description_url_en', $('[name=program_description_url_en]').val());

            } else {

                var file_rl = $('[name=file_rl]')[0].files[0],
                    file_en = $('[name=file_en]')[0].files[0];

                form.append('file_rl', file_rl);
                form.append('file_en', file_en);

            }

            // Сохранение
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/'+locale+'/admin/retailer/loyalty',
                method: 'post',
                data: form,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {

                    $('#editLoyalty').modal('hide');

                    if (response.check) { // Если сохранение прошло успешно, обновляем компонентьІ страницьІ

                        var div_rl = $('.program_description_url_rl'),
                            div_en = $('.program_description_url_en');

                        div_rl.find('a').remove();
                        div_rl.append('<a href="'+response.link.rl+'" class="col-md-5 value">'+response.link.rl+'</a>');
                        $('[name=program_description_url_rl]').val(response.link.rl);
                        div_en.find('a').remove();
                        div_en.append('<a href="'+response.link.en+'" class="col-md-5 value">'+response.link.en+'</a>');
                        $('[name=program_description_url_en]').val(response.link.en);

                    } else { // Сообщение об ошибке

                        specialAlert(response.message);

                    }

                },
                error: function (e) {
                    specialAlert(error500);
                }
            })

        });

        // Сохранение данньІх о контракте
        $('#save-contract').on('click', function () {

            var number  =   $('#editContract [name=number]').val(),
                scan    =   $('#editContract [name=file]')[0].files[0] ? $('#editContract [name=file]')[0].files[0] : '',
                retailer_id =   $('#retailer_id').val();

            // Составление формьІ
            var form    =   new FormData();

            form.append('number',number);
            form.append('scan',scan);
            form.append('retailer_id', retailer_id);

            // Сохранение
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/'+locale+'/admin/retailer/contract',
                method: 'post',
                data: form,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {

                    $('#editContract').modal('hide');

                    if (response.check) { // Если сохранение прошло успешно, обновляем компонентьІ страницьІ

                        $('.cont-number').html(response.contract.number);
                        $('.cont-scan').attr('href', response.contract.scan).text(response.contract.scan);

                    } else { // Сообщение об ошибке

                        specialAlert(response.message);

                    }

                },
                error: function (e) {
                    specialAlert(error500);
                }
            })

        })
    }

    // Установить статус опубликовано
    $('#publish-retailer').on('click', function (event) {

       setStatus(1, {send: $('#send_notice').prop('checked')}, function (res) {

           $('#notice').modal('hide');

           // Если сохранение прошло успешно - можно ставить статус "Опубликовано"
           if (res.check) {
               window.setTimeout(function () {
                   $('#notice').remove();
               }, 300);
               $('#select-status').val(1).trigger('change');

               $('.delete-img').css('display', 'none');

               window.isPublish = true;

               window.retailer_status = 1;
           }
       });
    });

    // Блокировать Ритейл
    $('#reason-block').on('keyup', function (e) {

        var button = document.getElementById('retailer-bl');

        var text = $('#reason-block').val();
        if (text) {
            button.className = 'btn btn-custom';
        } else {
            button.className = 'btn btn-custom disable';
        }

        delay(function () {

            button.onclick =  function (e) {

                if (!$('#reason-block').val()) return;

                var reason = $('#reason-block').val();

                setStatus(3, {reason: reason}, function (res) {

                    $('#block').modal('hide');

                    // Если сохранение прошло успешно - можно ставить статус "Заблокирован"
                    if (res.check) {
                        window.retailer_status = 3;
                        $('#select-status').val(3).trigger('change');

                        $('.delete-img').css('display','');

                        $('.decs-blocked').show(300);
                        $('.decs-blocked span').text(reason);

                    }

                    specialAlert(res.message);

                });
            }
        }, 300);
    });

    // Разлокировать Ритейл
    $('#reason-unblock').on('keyup', function (e) {

        var button = document.getElementById('retailer-unbl');

        var text = $('#reason-unblock').val();
        if (text) {
            button.className = 'btn btn-custom';
        } else {
            button.className = 'btn btn-custom disable';
        }

        delay(function () {



            button.onclick =  function () {

                if (!e.target.value.length) return;

                setStatus(1, {reason: $('#reason-unblock').val()}, function (res) {

                    $('#unblock').modal('hide');

                    // Если сохранение прошло успешно - можно ставить статус "Заблокирован"
                    if (res.check) {

                        window.retailer_status = 1;

                        $('#select-status').val(1).trigger('change');

                        $('.decs-blocked span').text('');
                        $('.decs-blocked').slideUp(300);

                        $('#reason-unblock').val('');


                        $('.delete-img').css('display', 'none');
                    }

                });
            }
        }, 300);
    });

    // Удаляем Ритейл
    $('#retailer-del').on('click', function (e) {

        $('#delete').modal('hide');

        $.ajax({
            url: '/'+locale+'/admin/retailer/'+$('#retailer_id').val()+'/delete',
            method: 'get',
            dataType: 'json',
            success: function (response) {

                if (response.check) {

                    $('#specialAlert').on('hide.bs.modal', function () {
                        window.location.assign('/'+locale+'/admin/retailers');
                    })
                }

                specialAlert(response.message);

            },
            error: function (e) {
                specialAlert(error500);
            }
        })
    });

    /* ------- ------- ------- POS ------- ------- ------- */

    // ЧекбоксьІ
    if ($('input').is('.icheck-pos')) {

        $('.icheck-pos').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '15%' // optional,
        });
    }

    // Сохранение/обновление POS
    $('#create-pos-form, #edit-pos-form').on('submit', function (e) {
        
        e.preventDefault();
        
        var formData    =   $(e.target).serialize(),
            modal   =   $(e.target).parents('.modal');

        $(e.target).find('.form-group').each(function (i, div) {

            var _addClass = $(div).find('input, textarea').attr('name');

            if (!$(div).hasClass(_addClass)) $(div).addClass(_addClass);

        });
        
        validatePos(formData, modal, function (response) { // если проверка прошла успешно - сохраняем

            var url = '/'+locale+'/admin/retailer/'+retailer_id+'/pos/save';

            if ($(e.target).find('[name=pos_id]').val()) url = url + '/' + $(e.target).find('[name=pos_id]').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'post',
                url: url,
                data: formData,
                dataType: 'json',
                success: function (res) {

                    if (res.check) {   // успешно сохранено

                        modal.modal('hide');

                        reloadTable();

                    } else {        // возникли ошибки при сохранении

                        modal.modal('hide');

                        specialAlert(res.message);

                    }
                },
                error: function (e) {
                    specialAlert(error500);
                }
            })
        })
    });

    // Поля создания POS
    var inputs_pos_create = '#new_pos_name_rl, #new_pos_name_en, #new_pos_address_rl, #new_pos_address_en, #new_latitude, #new_longitude';

    // Проверка заполненьІх данньІх POS
    eventHandler(inputs_pos_create,
        ['keyup', 'change'],
        function (e) {},
        {
            change: function (e) {

                var input   =   $(e.target),
                    formData =  input.parents('form').serialize(),
                    modal   =   input.parents('.modal'),
                    div     =   input.parents('.form-group');

                if (!div.hasClass(input.attr('name'))) div.addClass(input.attr('name'));

                validatePos(formData, modal);
            },
            keyup: function (e) {

                var input   =   $(e.target),
                    modal   =   input.parents('.modal'),
                    div     =   input.parents('.form-group');

                if (!div.hasClass(input.attr('name'))) div.addClass(input.attr('name'));

                delay(function () {
                    var formData = $(e.target).parents('form').serialize();
                    validatePos(formData, modal);
                }, 300);
            }
        }
    );
    // Поля создания POS
    var inputs_pos_edit = '#edit_pos_name_rl, #edit_pos_name_en, #edit_pos_address_rl, ' +
                         '#edit_pos_address_en, #edit_latitude, #edit_longitude';

    // Проверка заполненьІх данньІх POS
    eventHandler(inputs_pos_edit,
        ['keyup', 'change'],
        function (e) {
        },
        {
            change: function (e) {

                var input   =   $(e.target),
                    formData =  input.parents('form').serialize(),
                    modal   =   input.parents('.modal'),
                    div     =   input.parents('.form-group');

                if (!div.hasClass(input.attr('name'))) div.addClass(input.attr('name'));

                validatePos(formData, modal);
            },
            keyup: function (e) {

                var input   =   $(e.target),
                    modal   =   input.parents('.modal'),
                    div     =   input.parents('.form-group');

                if (!div.hasClass(input.attr('name'))) div.addClass(input.attr('name'));

                delay(function () {
                    var formData = $(e.target).parents('form').serialize();
                    validatePos(formData, modal);
                }, 300);
            }
        }
    );

    // ФильтрьІ таблици ритейлов
    if (typeof $.fn.select2 !== 'undefined') {

        $.fn.select2.amd.require(['select2/selection/search'], function (Search) {
            var oldRemoveChoice = Search.prototype.searchRemoveChoice;

            Search.prototype.searchRemoveChoice = function () {
                oldRemoveChoice.apply(this, arguments);
                this.$search.val('');
            };

            // Инициализация select2 для фильтров таблицьІ
            if ($('select').is('.select2-filter')) {

                var selectors = [

                    '#filter-region',
                    '#filter-category',
                    '#filter-schemes',
                    '#filter-statuses'
                ];

                $.each(selectors, function(i, selector){

                    $(selector).select2({
                        dropdownCssClass: 'filter-table',
                        placeholder: $(selector).attr('data-title')+': '+ $(selector).attr('data-empty'),
                        minimumResultsForSearch: -1,
                        multiple: true,
                        closeOnSelect: false,
                        escapeMarkup: function(m) { return m; },
                        templateResult: function (data) {

                            // Для акуратности создаем отображение с помощью jQuery
                            var label   =   $('<label></label>');

                            label.addClass('setect2__checkbox-label')
                                .html(data.text);

                            // return option.prop('outerHTML')+label.prop('outerHTML');
                            return label.prop('outerHTML');
                        }
                    });

                });

                eventHandler('.select2-filter', ['select2:select', 'select2:unselect'],function (e) {
                    $(e.target).find('option[value=""]').prop('selected', false);

                    $(e.target).trigger('change');

                    var params = '';

                    // Т.к. select2 устанавливает значение массивом использовать .serialize() не получится
                    // по єтому формируем GET параметрьІ "вручную"
                    $('#filters select').each( function (i, field) {

                        var val = $(field).val();

                        if (typeof val === 'object' && val !==null && val.length > 0) {

                            if (val.length == 1) {

                                if (val[0] !== '') {
                                    params = !params ? params + '?' : params + '&';

                                    params = params + $(field).attr('name') + '=' + val[0];
                                }
                            } else {

                                params = !params ? params+'?' : params+'&';

                                params = params + $(field).attr('name') + '=' + val.join(',');
                            }
                        }
                    });


                    // URL с установленньІми фильтрами
                    var url = table_url + params;

                    oTable.api().ajax.url(url);
                    oTable.api().ajax.reload();
                });
            }

            // Фильтр таблици POS
            if ($('select').is('#pos-status')) {

                var selector = '#pos-status';

                $(selector).select2({
                    dropdownCssClass: 'filter-table',
                    placeholder: $(selector).attr('data-title') + ': ' + $(selector).attr('data-empty'),
                    minimumResultsForSearch: -1,
                    multiple: true,
                    escapeMarkup: function (m) {
                        return m;
                    },
                    templateResult: function (data) {

                        // Для аккуратности создаем отображение с помощью jQuery
                        var label = $('<label></label>');

                        label.addClass('setect2__checkbox-label')
                            .html(data.text);

                        // return option.prop('outerHTML')+label.prop('outerHTML');
                        return label.prop('outerHTML');
                    }
                })

            }

            eventHandler('#pos-status', ['select2:select', 'select2:unselect'], function(e){

                $(e.target).find('option[value=""]').prop('selected', false);

                $(e.target).trigger('change');

                updatePosTable();

            });

        });

    }

    // Редактирование POS
    $('body').on('click', '.edit-pos', function(e){

        var fields = ['id','name_rl', 'name_en', 'address_rl', 'address_en', 'latitude', 'longitude'],
            button = $(e.target).hasClass('edit-pos') ? $(e.target) : $(e.target).parents('.edit-pos');

        $.each(fields, function (i, field) {

            var input = $('#editPos [name='+field+']');

            input.val(button.attr('data-'+field));

            // Если есть ограничение по длине
            if (typeof input.attr('maxlength') !== 'undefined') {

                var divMax  = input.siblings('.max-length'),
                    max     = parseInt(input.attr('maxlength'));

                divMax.find('.left').text(max - input.val().length);

            }



        });

        $('#editPos [name=pos_id]').val(button.attr('data-id'));
    });


    if ($('input').is('.up-csv')) {

        // Стилизация инпутов файлов программьІ лояльности
        $('.up-csv').filer({
            limit: 1,
            extensions: ["csv"],
            captions: {
                button: button_csv_title,
                feedback: "",
                feedback2: "<b>"+file_selected+"</b>",
                errors: {
                    filesLimit: limit_error,
                    filesType: type_error,
                    filesSize: size_error
                }
            }
        });

        $('#upload-pos').on('click', function (e) {

            var input = $('#posUpload [type=file]');


            // Проверяем вьІбран ли файл
            if (typeof input[0].files[0] === 'undefined') return;

            // Составляем форму
            var form = new FormData();
            form.append('csv_file',input[0].files[0]);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'post',
                url: '/'+locale+'/admin/retailer/'+window.retailer_id+'/pos/upload',
                data: form,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {

                    $('#posUpload').modal('hide');  // Прячем модаль
                    input.val('');                  // Обнуляем input

                    if (response.check) reloadTable();  // Если сохранение прошло успешно - перезагружаем таблицу

                    specialAlert(response.message); // Показ сообщения об результате

                }
            })
        });

        $('#edit-pos-form').on('submit', function (e) {

            e.preventDefault();

        })
    }

    // Манипуляции с POS-записями
    $('.actions-btn').on('click', function (e) {

        var button = $(e.target),
            action = button.attr('data-action');

        if (button.hasClass('disable')) return;

        var pos_ids = '';

        $('.check-one').each(function(i, checkbox){

            if ($(checkbox).is(':checked')) {

                pos_ids = !pos_ids ? $(checkbox).val() : (pos_ids+','+$(checkbox).val());

            }
        });

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
            },
            method: 'post',
            url: '/'+locale+'/admin/pos/'+action,
            data: { ids: pos_ids },
            dataType: 'json',
            success: function (response) {

                specialAlert(response.message);

                if (response.check) {
                    reloadTable();
                }

            },
            error: function (e) {

                specialAlert(error500);

            }
        })

    });

    // Обновление аналитики
    $('[href=#analytics]').on('click', function (e) {

        $.ajax({
            url: '/'+locale+'/admin/retailer/'+window.retailer_id+'/analytics',
            method: 'get',
            success: function (response) {

                if (response.check) {

                    $('#pos-count').text(response.data.pos_count);

                }
            }
        })

    });
    // Обновление логов
    $('[href=#logs]').on('click', reloadTable);

    var dateRangePicker = {

        init: function (options) {

            var options = typeof options !== 'undefined' ? Object.assign(dateRangePicker.options, options) : dateRangePicker.options;

            $('#pos-added').daterangepicker(dateRangePicker.options, dateRangePicker.callback);

            dateRangePicker.events.apply();
            dateRangePicker.events.show();
            dateRangePicker.events.hide();

        },

        options: {
            locale: {
                format: 'DD.MM.YYYY'
            },
            applyClass: 'btn btn-custom',
            autoApply: true,
            template: '<div class="daterangepicker dropdown-menu">' +
            '<div class="calendar left">' +
            '<div class="daterangepicker_input">' +
            '<input class="input-mini form-control" type="text" name="daterangepicker_start" value="" />' +
            '<div class="calendar-time">' +
            '<div></div>' +
            '</div>' +
            '</div>' +
            '<div class="calendar-table"></div>' +
            '</div>' +
            '<div class="calendar right">' +
            '<div class="daterangepicker_input">' +
            '<input class="input-mini form-control" type="text" name="daterangepicker_end" value="" />' +
            '<div class="calendar-time">' +
            '<div></div>' +
            '</div>' +
            '</div>' +
            '<div class="calendar-table"></div>' +
            '</div>' +
            '<div class="ranges">' +
            '<div class="range_inputs">' +
            '</div>' +
            '</div>' +
            '</div>'
        },
        callback : function (e) {

            window.setTimeout(function () {
                updatePosTable();

                $('#drop-filter-added').show();

            }, 300);

        },
        events: {

            apply: function () {

                $('#pos-added').on('apply.daterangepicker', function (e) {
                    window.emptyPosDate = false;
                });
            },

            show: function () {


                $('#pos-added').on('show.daterangepicker', function (e) {
                    if (!e.target.value) window.emptyPosDate = true;

                    var input = e.target;

                    document.onkeyup = function (event) {
                        if (event.keyCode == 27) {

                            delete dateRangePicker.options.startDate;
                            delete dateRangePicker.options.endDate;

                            $('#pos-added').blur();

                            document.onkeyup = null;
                            $('.daterangepicker').hide();

                            if (input.value) {
                                var date = input.value.split('-');
                                dateRangePicker.init({
                                    startDate: date[0],
                                    endDate: date[1]
                                })
                            } else {
                                dateRangePicker.init();
                                input.value = '';
                            }

                        }
                    };
                });

            },

            hide: function () {

                $('#pos-added').on('hide.daterangepicker', function (e) {

                    var dates = e.target.value;
                    e.target.value = '';

                    window.setTimeout(function (){
                        if (window.emptyPosDate)  {
                            e.target.value = '';
                        } else {
                            e.target.value = dates;
                        }
                    }, 100);
                });
            }
        }
    };

    // Фильтр POS по дате
    if ($('input').is('#pos-added')) {

        var end = new Date().ddmmyyyy(),
            start = new Date(new Date().setMonth((new Date().getMonth() -1))).ddmmyyyy(),
            max = new Date(new Date().setDate(new Date().getDate()+1)).ddmmyyyy();

        $('#pos-added').daterangepicker(dateRangePicker.options, dateRangePicker.callback);

        window.setTimeout(function () {
            $('#pos-added').val('');
        }, 300);

        $('#drop-filter-added').on('click', function (e) {

            $('#pos-added').val('');

            $(e.target).hide(200);
            window.emptyPosDate = true;
            updatePosTable();

        });

        dateRangePicker.init({
            startDate: start,
            endDate: end,
            maxDate: max
        });

    }

    // Удаление изображения ритуйла
    $('.delete-img').on('click', function(){
        var button = $(this);
        $('#delete-image').attr('data-img', button.attr('data-img'));

    });

    $('#delete-image').on('click', function () {

        var button  = $(this),
            img_type= button.attr('data-img');

        // если тип неопределен
        if (!img_type) return;

        button.parents('.modal').modal('hide');

        $.ajax({
            url: '/'+locale+'/admin/retailer/'+window.retailer_id+'/delete-image/'+img_type,
            method: 'get',
            success: function (response) {

                if (response.check) {

                    // соотвутствие типа изб. класу контейнера в котором оно находидлось
                    var equal_class = {

                        big_img:    '.large',
                        small_img:  '.small',
                        pos_img:    '.pos',
                        logo_img:   '.logo'
                    };

                    var container = $('.images-box '+equal_class[img_type]);

                    // Сброс данньІх об изображении
                    container.find('.img-box').css('background','');
                    container.find('.edit-img').attr('data-img', '');
                    container.find('.delete-img').hide(300);

                } else {    // если изображение не удалилось

                    specialAlert(response.message);
                }
            },
            error: function (e) {

                specialAlert(error500);

            }
        });
    });

});

$(document).on('show.bs.modal', '.modal', function () {
    var zIndex = Math.max.apply(null, Array.prototype.map.call(document.querySelectorAll('*'), function(el) {
            return +el.style.zIndex;
        })) + 10;
    $(this).css('z-index', zIndex);
    setTimeout(function() {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
    $(document).on('hidden.bs.modal', '.modal', function () {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });
});


$(document).on('mousedown', '.navbar-brand', function(e) {
    $(this).find('img').attr('src','/assets/admin/img/logo_click.png');
});

$(document).on('mouseover', '.a_bro_user_link', function(e) {
    $(this).find('.a_bro_user_name').css('border-bottom','1px solid');
});

$(document).on('mouseout', '.a_bro_user_link', function(e) {
    $(this).find('.a_bro_user_name').css('border-bottom','none');
});
