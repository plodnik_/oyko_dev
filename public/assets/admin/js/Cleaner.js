/* Обработчик очистки форм модальньІх окон */

var Cleaner = {

    /**
     *  Функция инициализации отслеживания модяльньІх окон
     */
    init: function () {

        $('.modal').on('show.bs.modal', Cleaner.onOpen);
        $('.modal').on('hide.bs.modal', Cleaner.onClose);

    },

    /**
     *  Обработчик собьІтия открьІтия формьІ
     */
    onOpen: function (event) {

        Cleaner.variables.DOM.modal = event.target;

        if (!Cleaner.functions.hasStatus()) return;

        var status = Cleaner.functions.getStatus();

        /*  Если статус save значит надо сохранить состояние формьІ  */
        if (status == 'save') {
            Cleaner.functions.saveState();

        }

        /*  Если статус clear - надо удалить оставшиеся ошибки */
        if (status == 'clear') {

            Cleaner.functions.saveState({buttonsOnly: true});

            // Удаление ошибок
            $(Cleaner.variables.DOM.modal).find('.form-group').removeClass('has-error');
            $(Cleaner.variables.DOM.modal).find('.form-group .help-block').hide();

            // очистка плагина загрузки файлов
            $(Cleaner.variables.DOM.modal).find('.jFiler-input-caption span').html('');
        }
    },

    /**
     *  Обработчик собьІтия закрьІтия формьІ
     */
    onClose: function (event) {

        Cleaner.variables.DOM.modal = event.target;

        if (!Cleaner.functions.hasStatus()) return;

        var status = Cleaner.functions.getStatus();

        /* Статус empty означает что в модальном окне нету полей для сохранения */
        if (status == 'empty') return;

        /* Очистка всех полей (кроме _token) */
        if (status == 'clear') {
            Cleaner.functions.clearAll();
            Cleaner.functions.restoreButtons()
        }

        /* Возврат полей к изначальному состоянию */
        if (status == 'save') {
            Cleaner.functions.backToState();
        }

    },

    functions: {
        hasStatus: function() {
            var data = Cleaner.variables.DOM.modal.dataset;
            return (typeof data.clear !== 'undefined') ? true : false;
        },
        getStatus: function () {
            var data = Cleaner.variables.DOM.modal.dataset;
            return data.clear;
        },
        addUniqID: function () {

            var uniqId = parseInt((Math.random()*1000000) - (Math.random()*1000) + (Math.random()*10000));

            Cleaner.variables.DOM.modal.dataset.cleanerId = uniqId;

            return uniqId;
        },

        getFieldsState: function () {

            var fields = Cleaner.variables.DOM.modal.querySelectorAll('input, textarea, select'),
                state = [];

            fields.forEach(function (field) {

                var data = {};

                data.id = field.id;
                data.name = field.name;

                var group = $(field).parents('.form-group');
                data.groupClass = group.attr('class');

                switch (field.nodeName.toLowerCase()) {

                    case 'input':
                        // пропускать скрьІтьІе поля
                        if (field.type == 'hidden') {
                            if (field.name == 'keyword') {
                                data.value = field.value;
                                data.node = 'hidden';
                            } else {
                                data.node = 'empty'
                            }
                            break;
                        }

                        if (field.type == 'text' || field.type == 'number') {
                            data.node = 'input';
                            data.value = field.value;
                        } else if (field.type == 'checkbox') {
                            data.node = 'checkbox';
                            data.value = field.checked;
                        }

                        break;

                    case 'textarea':

                        data.node = 'textarea';
                        data.value = field.value;

                        break;

                    case 'select':

                        data.node = 'select';
                        data.value = field.selectedIndex;
                        break;
                }

                // Если data не является порожним обьектом - сохраняем
                if (Object.keys(data).length) state.push(data);
            });

            return state;

        },

        getButtonsState: function () {

            var buttons = Cleaner.variables.DOM.modal.querySelectorAll('button');
            var state = [];

            if (buttons) {
                buttons.forEach(function (button) {

                    var one = {};

                    if (!button.id) {
                        button.id = 'btn-' + (parseInt(Math.random() * 10000000));
                    }

                    one.id = button.id;
                    one.className = button.className;
                    one.innerText = button.innerText;

                    state.push(one);

                });
            }

            return state.length > 0 ? state : null;

        },

        saveState: function (options) {

            var cleanerID = Cleaner.functions.addUniqID();

            if (window.cleaner == undefined) window.cleaner = {};
            window.cleaner[cleanerID] = {};

            if (typeof options !== 'undefined' && options.buttonsOnly) {
                window.cleaner[cleanerID].buttons = Cleaner.functions.getButtonsState();
            } else {
                window.cleaner[cleanerID].fields = Cleaner.functions.getFieldsState();
                window.cleaner[cleanerID].buttons = Cleaner.functions.getButtonsState();
            }

            Cleaner.variables.DOM.modal.dataset.cleanerId = cleanerID;
        },

        clearAll: function () {

            var fields = Cleaner.variables.DOM.modal.querySelectorAll('input, textarea, select');

            // Дополнительная проверка
            if (Object.keys(fields).length > 0) {
                fields.forEach(function (field) {

                    var doKeyup = false;

                    switch (field.nodeName.toLowerCase()) {

                        case 'input' :

                            if (field.name != '_token') {

                                if (field.type == 'text' || field.type == 'number' || field.type == 'password') {
                                    field.value = '';
                                    doKeyup = true;
                                } else if (field.type == 'checkbox' || field.type == 'radio') {
                                    field.checked = false;
                                }
                            }

                            break;

                        case 'textarea' :
                            field.value = '';
                            doKeyup = true;
                            break;

                        case 'select' :
                            field.selectedIndex = 0;
                            break;
                    }

                    if (field.name !== '_token') {

                        if (field.type == 'checkbox') {
                            field.style.visibility = 'visible';
                        }
                        if (field.name == 'keyword') {
                            $('#tag-it-keywords').tagit('removeAll');
                        }

                        var formGroup = $(field).parents('.form-group');

                        formGroup.removeClass('has-error');
                        formGroup.find('.help-block').hide();
                    }

                    // $(field).trigger('change');
                    $(field).trigger('keyup');
                });
            }

        },

        backToState: function () {

            var cleanerID = Cleaner.variables.DOM.modal.dataset.cleanerId;

            if (typeof cleanerID === 'undefined') {
                console.warn('State ID not save to modal');
                return false;
            }
            if (typeof window.cleaner === 'undefined' || typeof window.cleaner[cleanerID] === 'undefined') {
                console.warn('Modal state not saved');
                return false;
            }

            var fields = window.cleaner[cleanerID].fields,
                buttons = window.cleaner[cleanerID].buttons;

            delete window.cleaner[cleanerID];

            if (Object.keys(fields).length <= 0) {
                console.warn('Empty modal state');
            }

            fields.forEach(function (data) {

                var field = null;

                if (data.id) {
                    field = document.getElementById(data.id);
                } else {
                    field = document.getElementsByName(data.name);
                    field = field.length > 0 ? field[0] : null;
                }

                if (field) {

                    var doKeyup = false;

                    switch (data.node) {
                        case 'input':
                            if (field.name !== '_token') {
                                field.value = data.value;
                                doKeyup = true;
                            }
                            break;

                        case 'checkbox' :
                            field.checked = data.value;
                            break;

                        case 'select':
                            field.selectedIndex = data.value;
                            break;

                        case 'textarea':
                            field.value = data.value;
                            doKeyup = true;
                            break;
                    }

                    if (field.name !== '_token') {

                        if (field.name == 'keyword') {

                            var keywords = document.getElementById('tag-it-keywords');
                            $(keywords).tagit('removeAll');

                            if (data.value.indexOf(',') >0) {

                                data.value.split(',').forEach(function (tag) {
                                    if (tag.replace(/ /g, "").length > 0) {
                                        $(keywords).tagit('createTag', tag.replace(/ /g, ""));
                                    }
                                })
                            } else {
                                if (data.value.replace(/ /g, "").length > 0) {
                                    $(keywords).tagit('createTag', field.value.replace(/ /g, ""));
                                }
                            }

                        } else {
                            $(field).parents('.form-group').attr('class', data.groupClass);

                            if(field.type == 'checkbox') {
                                field.dispatchEvent(new Event('click'));
                            }

                            if (doKeyup) field.dispatchEvent(new Event('keyup'));

                            var formGroup = $(field).parents('.form-group');

                            formGroup.removeClass('has-error');
                            formGroup.find('.help-block').hide();
                        }
                    }

                }

            });

            if (buttons) {
                buttons.forEach(function (state) {

                    var btn = document.getElementById(state.id);

                    btn.className = state.className;

                });
            }

        },

        restoreButtons: function () {
            var cleanerID = Cleaner.variables.DOM.modal.dataset.cleanerId;

            if (typeof cleanerID === 'undefined') {
                console.warn('State ID not save to modal');
                return false;
            }
            if (typeof window.cleaner === 'undefined' || typeof window.cleaner[cleanerID] === 'undefined') {
                console.warn('Modal state not saved');
                return false;
            }

            var data = window.cleaner[cleanerID].buttons;

            delete window.cleaner[cleanerID];

            if (Object.keys(data).length <= 0) {
                console.warn('Empty modal state from buttons');
            }

            if (data) {
                data.forEach(function (state) {

                    var btn = document.getElementById(state.id);

                    btn.className = state.className;

                });
            }
        }
    },

    variables: {
        DOM: {},
        storage: {}
    }

};

// Запуст отслеживания
(function () {
    Cleaner.init();
}(Cleaner));

