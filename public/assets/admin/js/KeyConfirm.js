/**
 *
 * @constructor
 */
var KeyConfirm = function () {};

/**
 *  Инициализация
 */
KeyConfirm.prototype.init = function () {

    console.info('KeyConfirm initialisation is started');

    this.listen.modals({confirmOnTextarea: true});
};

KeyConfirm.prototype.listen = {

    modals: function (options) {

        KeyConfirm.options = typeof options === 'undefined' ? KeyConfirm.defaults.modalOptions : Object.assign(KeyConfirm.defaults.modalOptions, options);

        console.info('Start listen modal');

        $(KeyConfirm.options.listenSelector).on('show.bs.modal', KeyConfirm.functions.modal.onOpen);
        $(KeyConfirm.options.listenSelector).on('hide.bs.modal', KeyConfirm.functions.modal.onClose);
    }

};

KeyConfirm.defaults = {
    modalOptions: {
        listenSelector: '.modal',
        closeOnECS : true,
        confirmOnTextarea: true
    }
};
KeyConfirm.options = {};
KeyConfirm.functions = {

    modal: {
        onOpen: function (event) {

            document.onkeyup = function (keyUp) {

                if (keyUp.keyCode == 13) {
                    $(event.target).find('[role=confirm]').trigger('click');
                    document.onkeyup = null;
                }
            };

        },
        onClose: function (event) {
            document.onkeyup = null;
        }
    },

};

(function(KeyConfirm){
    var kc = new KeyConfirm();

    kc.init();

}(KeyConfirm));