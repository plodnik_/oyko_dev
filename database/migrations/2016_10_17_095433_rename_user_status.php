<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class RenameUserStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('bo_user_status')
            ->where('id', 1)
            ->update([
                'status_en' => 'active',
                'status_ru' => 'Активен'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('bo_user_status')
            ->where('id', 1)
            ->update([
                'status_en' => 'unblocked',
                'status_ru' => 'Разблокирован'
            ]);
    }
}
