<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class AddAutoIncrementFromRetailerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Удаление внешних ключей
        Schema::table('pos', function (Blueprint $table) {
            $table->dropForeign('fk_pos_retailer1');
        });
        Schema::table('retailer_analytics', function (Blueprint $table) {
            $table->dropForeign('fk_retailer_analytics_retailer1');
        });
        Schema::table('retailer_category_selected', function (Blueprint $table) {
            $table->dropForeign('fk_retailer_category_has_retailer_retailer1');
        });
        Schema::table('retailer_logs', function (Blueprint $table) {
            $table->dropForeign('fk_logs_retailer1');
        });

        // Добавление auto increment
        Schema::table('retailer', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });

        // Возврат внешних ключей
        Schema::table('pos', function (Blueprint $table) {
            $table->unsignedBigInteger('retailer_id')->change();
            $table->foreign('retailer_id')
                ->references('id')
                ->on('retailer')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
        Schema::table('retailer_analytics', function (Blueprint $table) {
            $table->unsignedBigInteger('retailer_id')->change();
            $table->foreign('retailer_id')
                ->references('id')
                ->on('retailer')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
        Schema::table('retailer_category_selected', function (Blueprint $table) {
            $table->unsignedBigInteger('retailer_id')->change();
            $table->foreign('retailer_id')
                ->references('id')
                ->on('retailer')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('retailer_logs', function (Blueprint $table) {
            $table->unsignedBigInteger('retailer_id')->change();
            $table->foreign('retailer_id')
                ->references('id')
                ->on('retailer')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Удаление внешних ключей
        Schema::table('pos', function (Blueprint $table) {
            $table->dropForeign(['retailer_id']);
        });
        Schema::table('retailer_analytics', function (Blueprint $table) {
            $table->dropForeign(['retailer_id']);
        });
        Schema::table('retailer_category_selected', function (Blueprint $table) {
            $table->dropForeign(['retailer_id']);
        });
        Schema::table('retailer_logs', function (Blueprint $table) {
            $table->dropForeign(['retailer_id']);
        });

        // Удаление auto increment
        Schema::table('retailer', function (Blueprint $table) {
            $table->bigInteger('id')->change();
        });

        // Возврат внешних ключей
        Schema::table('pos', function (Blueprint $table) {
            $table->bigInteger('retailer_id')->change();
        });
        Schema::table('retailer_analytics', function (Blueprint $table) {
            $table->bigInteger('retailer_id')->change();
        });
        Schema::table('retailer_category_selected', function (Blueprint $table) {
            $table->bigInteger('retailer_id')->change();
        });
        Schema::table('retailer_logs', function (Blueprint $table) {
            $table->bigInteger('retailer_id')->change();
        });
        DB::select(DB::raw('
                    ALTER TABLE `pos` 
                    ADD CONSTRAINT `fk_pos_retailer1` 
                    FOREIGN KEY (`retailer_id`) 
                    REFERENCES `retailer` (`id`)
                    ON DELETE NO ACTION 
                    ON UPDATE NO ACTION'
        ));
        DB::select(DB::raw('
                    ALTER TABLE `retailer_analytics` 
                    ADD CONSTRAINT `fk_retailer_analytics_retailer1` 
                    FOREIGN KEY (`retailer_id`) 
                    REFERENCES `retailer` (`id`)
                    ON DELETE RESTRICT 
                    ON UPDATE CASCADE '
        ));
        DB::select(DB::raw('
                    ALTER TABLE `retailer_category_selected` 
                    ADD CONSTRAINT `fk_retailer_category_has_retailer_retailer1` 
                    FOREIGN KEY (`retailer_id`) 
                    REFERENCES `retailer` (`id`)
                    ON DELETE CASCADE 
                    ON UPDATE CASCADE '
        ));
        DB::select(DB::raw('
                    ALTER TABLE `retailer_logs` 
                    ADD CONSTRAINT `fk_logs_retailer1` 
                    FOREIGN KEY (`retailer_id`) 
                    REFERENCES `retailer` (`id`)
                    ON DELETE RESTRICT 
                    ON UPDATE CASCADE '
        ));

    }
}
