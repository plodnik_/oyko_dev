<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddRolesIndexes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Администратор
        DB::table('bo_user_role')
            ->where('id', 1)
            ->update(['role' => 'admin']);

        // Модератор
        DB::table('bo_user_role')
            ->where('id', '2')
            ->update(['role' => 'moderator']);

        // Пользователь
        DB::table('bo_user_role')
            ->where('id', 3)
            ->update(['role' => 'user']);
    }
}
